<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Orden de compra | Purchase order</title>
</head>
<body>
	<table style="margin: 0px auto;" cellspacing="0" cellpadding="0" border="0">
	  <tr>
	  	<p style="font-size: 14pt; text-align: center">
	  		@if($data->Provider->country != 'ES')
	  			Hello <b>{{$data->Provider->manager}}</b>, responsible of <span><b>{{$data->Provider->name}}</b></span>
	  			@else
	  		 		Hola <b>{{$data->Provider->manager}}</b>, responsable de <span><b>{{$data->Provider->name}}</b></span>
	  		@endif
	  	</p>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center;">
	  		@if($data->Provider->country != 'ES')
	  			We write to you from Agri Technology Solutions, the following email is to attach the purchase order:
	  			@else
	  				Le escribimos de Agri Technology Solutions, el siguiente correo es para adjuntarle la orden de compra:
	  		@endif
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 18pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  			#{{$data['reference']}}
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; text-align: center;">
	  		@if($data->Provider->country != 'ES')
	  			This email has been sent from our automatic quotation system, waiting for a prompt response from you, the ATS team says goodbye.
	  			@else
	  		 		Este correo ha sido enviado desde nuestro sistema automático de contizaciones, esperando una pronta respuesta de ustedes se despide el equipo de ATS.
	  		@endif
	  		</p>
	  	</td>
	  </tr>
	</table>
	<br><br>
</body>
</html>