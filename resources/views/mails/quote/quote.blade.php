<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Quote</title>
</head>
<body>
	<table style="margin: 0px auto; width: 800px !important" cellspacing="0" cellpadding="0" border="0">
	  <!--<tr>
	    <td width="500px">
	    	<center>
	    		<img src="https://hellenross.com/app/agriquote/images/logo.png" width="300px">
	    	</center>
	    </td>
	  </tr>-->
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
	  		@if(!$data['first_email'])
	  			Estimado <span style="text-transform: uppercase !important;">{{$data->Client->fullname}}</span>, Nuestro mas cordial saludo, su solicitud de cotización ha sido procesada por nuestro departamento correspondiente, este correo se envía de forma automática a través de nuestro sistema de cotizaciones.<br><br>
	  			A través del siguiente enlace (link o botón) podrá acceder a la cotización, planos en caso de aplicar, así como un pequeño formulario donde nos hará conocer sus impresiones sobre la cotización recibida, es importante rellenar todos los campos, en caso de ser prematuro obtener una respuesta de su parte, solo debe seleccionar la opción de “Proyecto en Estudio, aun sin respuesta”
	  			@else
	  			Estimado <span style="text-transform: uppercase !important;">{{$data->Client->fullname}}</span>, Nuestro más cordial saludo, queremos realizar un seguimiento a la cotización No. <b>{{$data['code']}}</b> que ha recibido de parte de nuestro sistema de cotización.<br><br>
	  			Nuestros registros indican que desde <b>{{$data->getLastEmailDate()}}</b> no hemos recibido una respuesta de su parte y es por ello por lo que está recibiendo este correo de seguimiento, con la finalidad de entender un poco el estatus del proyecto y ayudar con lo que este de nuestra parte a lograr materializar el proyecto juntos.
	  		@endif
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 18pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  			{{$data['code']}}
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; text-align: center; color: black;">
	  			@if(!$data['first_email'])
	  				Dar click al siguiente botón:
	  				@else
	  				Dar click al siguiente botón, para revisar la cotización enviada:
	  			@endif
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			@if(ENV('APP_ENV') == 'local')
		  			<a href="{{url(ENV('LOCAL_FROM') . '/#/quotes/client/response/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">
		  				Acceder a la Información
		  			</a>
		  			@else
		  			<a href="{{url(ENV('PROD_FROM') . '/#/quotes/client/response/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">
		  				Acceder a la Información
		  			</a>
	  			@endif
	  		</center>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<br>
	  		<p style="font-size: 14pt; text-align: center; color: black;">
				@if(!$data['first_email'])
		  			Agradecemos de antemano, la oportunidad brindada y confiar en nuestras soluciones, por favor no dude en ponerse en contacto con nosotros si existiese alguna duda, si requiere alguna información adicional o si tiene algún comentario, estaremos muy atentos de poderle servir.<br><br>
		  			Tenga presente que desde Agri Technology Solutions haremos todo lo que este de nuestro lado para ofrecerle la mejor solución al mejor precio, con la mejor tecnología y calidad, daremos nuestro mayor esfuerzo por lograr cerrar la negociación con vosotros.
		  			@else
		  			Agradecemos de antemano, la oportunidad brindada y confiar en nuestras soluciones, por favor no dude en ponerse en contacto con nosotros si existiese alguna duda, si requiere alguna información adicional o si tiene algún comentario, estaremos muy atentos de poderle servir.<br><br>
		  			Tenga presente que desde Agri Technology Solutions haremos todo lo que este de nuestro lado para ofrecerle la mejor solución al mejor precio, con la mejor tecnología y calidad, daremos nuestro mayor esfuerzo por lograr cerrar la negociación con vosotros.
		  		@endif
		  	</p>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			<p style="font-size: 14pt; text-align: center; color: black;">
	  				<b>
	  					Quotation/Sales Department<br>
						AGRI TECHNOLOGY SOLUTIONS
	  				</b>
	  			</p>
	  		</center>
	  	</td>
	  </tr>
	</table>
	<br><br>
</body>
</html>