<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Quote Validation</title>
</head>
<body>
	<table style="margin: 0px auto;" cellspacing="0" cellpadding="0" border="0">
	  <!--<tr>
	    <td width="500px">
	    	<center>
	    		<img src="https://hellenross.com/app/agriquote/images/logo.png" width="300px">
	    	</center>
	    </td>
	  </tr>-->
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
	  		Hola <span style="text-transform: uppercase !important;">{{$data->admin}}</span>, el siguiente correo es para notificarte que el usuario: <b style="text-transform: uppercase;">{{$data->User->first_name}} {{$data->User->last_name}}</b> ha solicitado la validación de la cotización:
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  			{{$data['code']}}
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
			Por favor has click en el siguiente boton para proceder a validar la cotización.
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			@if(ENV('APP_ENV') == 'local')
		  			<a href="{{url(ENV('LOCAL_FROM') . '/#/dashboard/quotes/edit/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">Validar
		  			</a>
		  			@else
		  			<a href="{{url(ENV('PROD_FROM') . '/#/dashboard/quotes/edit/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">Validar
		  			</a>
	  			@endif
	  		</center>
	  		<br>
	  	</td>
	  </tr>
	</table>
	<br><br>
</body>
</html>