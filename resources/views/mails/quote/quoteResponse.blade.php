<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Respuesta cliente</title>
</head>
<body>
	<table style="margin: 0px auto;" cellspacing="0" cellpadding="0" border="0">
	  <!--<tr>
	    <td width="500px">
	    	<center>
	    		<img src="https://hellenross.com/app/agriquote/images/logo.png" width="300px">
	    	</center>
	    </td>
	  </tr>-->
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
	  		Hola, el cliente <b style="text-transform: uppercase;">{{$data->Client->name}} {{$data->Client->last_name}}</b> ha cambiado el estado de la oferta <b>{{$data['code']}}</b> por:
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  			{{$data->getUserStatus()}}
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
			<b>{{$data['comment']}}</b>
	  	</p>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
			Ha continuación le adjustamos el correo de teléfono del cliente:
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			<p style="font-size: 14pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  				Teléfono: {{$data->Client->cellphone}} <br>
	  				Correo: {{$data->Client->email}}
	  			</p>
	  		</center>
	  		<br>
	  	</td>
	  </tr>
	</table>
	<br><br>
</body>
</html>