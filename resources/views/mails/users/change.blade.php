<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Change password</title>
</head>
<body>
	<table style="margin: 0px auto;" cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <td width="500px">
	    	<center>
	    		<img src="https://hellenross.com/app/agriquote/images/logo.png" width="300px">
	    	</center>
	    </td>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center">
	  		Le adjuntamos el siguiente link para realizar el proceso de cambio de contraseña para el usuario:
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  			Usario: {{$data['first_name']}} {{$data['last_name']}}
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center">
	  		Presiona el siguiente link para continuar
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			@if(ENV('APP_ENV') == 'local')
		  			<a href="{{url(ENV('LOCAL_FROM') . '/#/confirm/account/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">Confirmar
		  			</a>
		  			@else
		  			<a href="{{url(ENV('PROD_FROM') . '/#/confirm/account/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">Confirmar
		  			</a>
		  		@endif
	  		</center>
	  		<br>
	  	</td>
	  </tr>
	</table>
	<br><br>
</body>
</html>