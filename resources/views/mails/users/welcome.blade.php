<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Welcome to Agri Technologi Solutions</title>
</head>
<body>
	<table style="margin: 0px auto;" cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <td width="500px">
	    	<center>
	    		<img src="https://hellenross.com/app/agriquote/images/logo.png" width="300px">
	    	</center>
	    </td>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center">
	  		Hola, el siguiente correo es para notificar que se ha registrado una nueva cuenta en agri -techsolutions para este email.
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; background: #eeeeee; color: #033a78; padding: 1em; width: 100%; text-align: center; border-radius: 5px; text-align: center">
	  			Correo: <span style="color: #033a78!important">{{$data['email']}}</span>
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<p style="font-size: 14pt; text-align: center">
	  		Por favor ingresa al siguiente link para poder configurar tu contraseña en AGRIQUOTE APP.
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			@if(ENV('APP_ENV') == 'local')
		  			<a href="{{url(ENV('LOCAL_FROM') . '/#/confirm/account/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">Confirmar
		  			</a>
		  			@else
		  			<a href="{{url(ENV('PROD_FROM') . '/#/confirm/account/' . $data['uuid'])}}" style="background: #033a78; padding: .5em 1em; border-radius: 5px; color: white; text-decoration: none; font-size: 14pt;">Confirmar
		  			</a>
		  		@endif
	  		</center>
	  	</td>
	  </tr>
	</table>
	<br><br><br>
</body>
</html>