<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Quote</title>
</head>
<body>
	<table style="margin: 0px auto; width: 800px !important" cellspacing="0" cellpadding="0" border="0">
	  <!--<tr>
	    <td width="500px">
	    	<center>
	    		<img src="https://hellenross.com/app/agriquote/images/logo.png" width="300px">
	    	</center>
	    </td>
	  </tr>-->
	  <tr>
	  	<p style="font-size: 14pt; text-align: center; color: black;">
	  		@if($data['Provider']['country'] == 'ES')
	  			Hola <b>{{$data['Provider']['name']}}</b>, el siguiente correo es para notificarles que se ha generado la cotización
	  			@else
	  			Hello <b>{{$data['Provider']['name']}}</b>, the following email is to notify you that the quote has been generated
	  		@endif
	  	</p>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 18pt; background: #033a78; color: white; padding: 1em; width: 100%; text-align: center; border-radius: 5px">
	  			{{$data['Requisition']['reference']}}
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<p style="font-size: 14pt; text-align: center; color: black;">
	  			@if($data['Provider']['country'] == 'ES')
		  			A continuación adjuntamos el pdf de la cotización solicitada y su respectivo croquis. En estos se especifica detalladamente los productos solicitados. Esperando una pronta respuesta nos despedimos.
		  			@else
		  			Below we attach the pdf of the requested quote and its respective sketch. These specify in detail the requested products. Waiting for a prompt response, we say goodbye.
		  		@endif
	  		</p>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			<p style="font-size: 14pt; text-align: center; color: black;">
	  				<b>
	  					Quotation/Sales Department<br>
						AGRI TECHNOLOGY SOLUTIONS
	  				</b>
	  			</p>
	  		</center>
	  	</td>
	  </tr>
	  <tr>
	  	<td>
	  		<center>
	  			<p style="font-size: 12pt; text-align: center; color: grey;">This email was sent automatically by our SQS (Smart Quoting System - Sistema de cotización Inteligente).</p>
	  		</center>
	  	</td>
	  </tr>
	</table>
	<br><br>
</body>
</html>