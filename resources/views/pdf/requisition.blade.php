  <!DOCTYPE html>
  <html lang="es">
      <head>
          <meta charset="UTF-8">
          <title>Requisition {{$requisition['code']}}-{{$requisition['provider']['name']}}</title>
          <style>
              /*!
   * Bootstrap v3.3.6 (http://getbootstrap.com)
   * Copyright 2011-2015 Twitter, Inc.
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   */
  /*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */
  html {
    font-family: sans-serif;
    -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
  }
  body {
    margin: 0;
  }

  table {
    border-spacing: 0;
    border-collapse: collapse;
  }
  td,
  th {
    padding: 0;
  }
  /*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
  @media print {
    *,
    *:before,
    *:after {
      color: #000 !important;
      text-shadow: none !important;
      background: transparent !important;
      -webkit-box-shadow: none !important;
              box-shadow: none !important;
    }
    a,
    a:visited {
      text-decoration: underline;
    }
    a[href]:after {
      content: " (" attr(href) ")";
    }
    abbr[title]:after {
      content: " (" attr(title) ")";
    }
    a[href^="#"]:after,
    a[href^="javascript:"]:after {
      content: "";
    }
    pre,
    blockquote {
      border: 1px solid #999;

      page-break-inside: avoid;
    }
    thead {
      display: table-header-group;
    }
    tr,
    img {
      page-break-inside: avoid;
    }
    img {
      max-width: 100% !important;
    }
    p,
    h2,
    h3 {
      orphans: 3;
      widows: 3;
    }
    h2,
    h3 {
      page-break-after: avoid;
    }
    .navbar {
      display: none;
    }
    .btn > .caret,
    .dropup > .btn > .caret {
      border-top-color: #000 !important;
    }
    .label {
      border: 1px solid #000;
    }
    .table {
      border-collapse: collapse !important;
    }
    .table td,
    .table th {
      background-color: #fff !important;
    }
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #ddd !important;
    }
  }

  * {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  *:before,
  *:after {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  html {
    font-size: 10px;

    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }
  body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    background-color: #fff;
  }
  hr {
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eee;
  }
  p {
    margin: 0 0 10px;
  }
  .lead {
    margin-bottom: 20px;
    font-size: 16px;
    font-weight: 300;
    line-height: 1.4;
  }
  @media (min-width: 768px) {
    .lead {
      font-size: 21px;
    }
  }
  small,
  .small {
    font-size: 85%;
  }
  .text-left {
    text-align: left;
  }
  .text-right {
    text-align: right;
  }
  .text-center {
    text-align: center;
  }
  .text-justify {
    text-align: justify;
  }
  .text-nowrap {
    white-space: nowrap;
  }
  .text-lowercase {
    text-transform: lowercase;
  }
  .text-uppercase {
    text-transform: uppercase;
  }
  .text-capitalize {
    text-transform: capitalize;
  }
  .text-muted {
    color: #777;
  }
  .text-primary {
    color: #337ab7;
  }
  a.text-primary:hover,
  a.text-primary:focus {
    color: #286090;
  }
  .text-success {
    color: #3c763d;
  }
  a.text-success:hover,
  a.text-success:focus {
    color: #2b542c;
  }
  .text-info {
    color: #31708f;
  }
  a.text-info:hover,
  a.text-info:focus {
    color: #245269;
  }
  .text-warning {
    color: #8a6d3b;
  }
  a.text-warning:hover,
  a.text-warning:focus {
    color: #66512c;
  }
  .text-danger {
    color: #a94442;
  }
  a.text-danger:hover,
  a.text-danger:focus {
    color: #843534;
  }
  .bg-primary {
    color: #fff;
    background-color: #033a78;
  }
  a.bg-primary:hover,
  a.bg-primary:focus {
    background-color: #286090;
  }
  .bg-success {
    background-color: #dff0d8;
  }
  a.bg-success:hover,
  a.bg-success:focus {
    background-color: #c1e2b3;
  }
  .bg-info {
    background-color: #d9edf7;
  }
  a.bg-info:hover,
  a.bg-info:focus {
    background-color: #afd9ee;
  }
  .bg-warning {
    background-color: #fcf8e3;
  }
  a.bg-warning:hover,
  a.bg-warning:focus {
    background-color: #f7ecb5;
  }
  .bg-danger {
    background-color: #f2dede;
  }
  a.bg-danger:hover,
  a.bg-danger:focus {
    background-color: #e4b9b9;
  }
  .page-header {
    padding-bottom: 9px;
    margin: 40px 0 20px;
    border-bottom: 1px solid #eee;
  }
  ul,
  ol {
    margin-top: 0;
    margin-bottom: 10px;
  }
  ul ul,
  ol ul,
  ul ol,
  ol ol {
    margin-bottom: 0;
  }
  .list-unstyled {
    padding-left: 0;
    list-style: none;
  }
  .list-inline {
    padding-left: 0;
    margin-left: -5px;
    list-style: none;
  }
  .list-inline > li {
    display: inline-block;
    padding-right: 5px;
    padding-left: 5px;
  }
  dl {
    margin-top: 0;
    margin-bottom: 20px;
  }
  dt,
  dd {
    line-height: 1.42857143;
  }
  dt {
    font-weight: bold;
  }
  dd {
    margin-left: 0;
  }
  @media (min-width: 768px) {
    .dl-horizontal dt {
      float: left;
      width: 160px;
      overflow: hidden;
      clear: left;
      text-align: right;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .dl-horizontal dd {
      margin-left: 180px;
    }
  }
  abbr[title],
  abbr[data-original-title] {
    cursor: help;
    border-bottom: 1px dotted #777;
  }
  .initialism {
    font-size: 90%;
    text-transform: uppercase;
  }
  blockquote {
    padding: 10px 20px;
    margin: 0 0 20px;
    font-size: 17.5px;
    border-left: 5px solid #eee;
  }
  blockquote p:last-child,
  blockquote ul:last-child,
  blockquote ol:last-child {
    margin-bottom: 0;
  }
  blockquote footer,
  blockquote small,
  blockquote .small {
    display: block;
    font-size: 80%;
    line-height: 1.42857143;
    color: #777;
  }
  blockquote footer:before,
  blockquote small:before,
  blockquote .small:before {
    content: '\2014 \00A0';
  }
  .blockquote-reverse,
  blockquote.pull-right {
    padding-right: 15px;
    padding-left: 0;
    text-align: right;
    border-right: 5px solid #eee;
    border-left: 0;
  }
  .blockquote-reverse footer:before,
  blockquote.pull-right footer:before,
  .blockquote-reverse small:before,
  blockquote.pull-right small:before,
  .blockquote-reverse .small:before,
  blockquote.pull-right .small:before {
    content: '';
  }
  .blockquote-reverse footer:after,
  blockquote.pull-right footer:after,
  .blockquote-reverse small:after,
  blockquote.pull-right small:after,
  .blockquote-reverse .small:after,
  blockquote.pull-right .small:after {
    content: '\00A0 \2014';
  }
  address {
    margin-bottom: 20px;
    font-style: normal;
    line-height: 1.42857143;
  }
  code,
  kbd,
  pre,
  samp {
    font-family: Menlo, Monaco, Consolas, "Courier New", monospace;
  }
  code {
    padding: 2px 4px;
    font-size: 90%;
    color: #c7254e;
    background-color: #f9f2f4;
    border-radius: 4px;
  }
  kbd {
    padding: 2px 4px;
    font-size: 90%;
    color: #fff;
    background-color: #333;
    border-radius: 3px;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
  }
  kbd kbd {
    padding: 0;
    font-size: 100%;
    font-weight: bold;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  pre {
    display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  pre code {
    padding: 0;
    font-size: inherit;
    color: inherit;
    white-space: pre-wrap;
    background-color: transparent;
    border-radius: 0;
  }
  .pre-scrollable {
    max-height: 340px;
    overflow-y: scroll;
  }
  .container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  @media (min-width: 768px) {
    .container {
      width: 750px;
    }
  }
  @media (min-width: 992px) {
    .container {
      width: 970px;
    }
  }
  @media (min-width: 1200px) {
    .container {
      width: 1170px;
    }
  }
  .container-fluid {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  .row {
    margin-right: -15px;
    margin-left: -15px;
  }
  .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    requisitionsition: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
  }
  .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
    float: left;
  }
  .col-xs-12 {
    width: 100%;
  }
  .col-xs-11 {
    width: 91.66666667%;
  }
  .col-xs-10 {
    width: 83.33333333%;
  }
  .col-xs-9 {
    width: 75%;
  }
  .col-xs-8 {
    width: 66.66666667%;
  }
  .col-xs-7 {
    width: 58.33333333%;
  }
  .col-xs-6 {
    width: 50%;
  }
  .col-xs-5 {
    width: 41.66666667%;
  }
  .col-xs-4 {
    width: 33.33333333%;
  }
  .col-xs-3 {
    width: 25%;
  }
  .col-xs-2 {
    width: 16.66666667%;
  }
  .col-xs-1 {
    width: 8.33333333%;
  }
  .col-xs-pull-12 {
    right: 100%;
  }
  .col-xs-pull-11 {
    right: 91.66666667%;
  }
  .col-xs-pull-10 {
    right: 83.33333333%;
  }
  .col-xs-pull-9 {
    right: 75%;
  }
  .col-xs-pull-8 {
    right: 66.66666667%;
  }
  .col-xs-pull-7 {
    right: 58.33333333%;
  }
  .col-xs-pull-6 {
    right: 50%;
  }
  .col-xs-pull-5 {
    right: 41.66666667%;
  }
  .col-xs-pull-4 {
    right: 33.33333333%;
  }
  .col-xs-pull-3 {
    right: 25%;
  }
  .col-xs-pull-2 {
    right: 16.66666667%;
  }
  .col-xs-pull-1 {
    right: 8.33333333%;
  }
  .col-xs-pull-0 {
    right: auto;
  }
  .col-xs-push-12 {
    left: 100%;
  }
  .col-xs-push-11 {
    left: 91.66666667%;
  }
  .col-xs-push-10 {
    left: 83.33333333%;
  }
  .col-xs-push-9 {
    left: 75%;
  }
  .col-xs-push-8 {
    left: 66.66666667%;
  }
  .col-xs-push-7 {
    left: 58.33333333%;
  }
  .col-xs-push-6 {
    left: 50%;
  }
  .col-xs-push-5 {
    left: 41.66666667%;
  }
  .col-xs-push-4 {
    left: 33.33333333%;
  }
  .col-xs-push-3 {
    left: 25%;
  }
  .col-xs-push-2 {
    left: 16.66666667%;
  }
  .col-xs-push-1 {
    left: 8.33333333%;
  }
  .col-xs-push-0 {
    left: auto;
  }
  .col-xs-offset-12 {
    margin-left: 100%;
  }
  .col-xs-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-xs-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-xs-offset-9 {
    margin-left: 75%;
  }
  .col-xs-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-xs-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-xs-offset-6 {
    margin-left: 50%;
  }
  .col-xs-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-xs-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-xs-offset-3 {
    margin-left: 25%;
  }
  .col-xs-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-xs-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-xs-offset-0 {
    margin-left: 0;
  }
  @media (min-width: 768px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }
    .col-sm-12 {
      width: 100%;
    }
    .col-sm-11 {
      width: 91.66666667%;
    }
    .col-sm-10 {
      width: 83.33333333%;
    }
    .col-sm-9 {
      width: 75%;
    }
    .col-sm-8 {
      width: 66.66666667%;
    }
    .col-sm-7 {
      width: 58.33333333%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-5 {
      width: 41.66666667%;
    }
    .col-sm-4 {
      width: 33.33333333%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-2 {
      width: 16.66666667%;
    }
    .col-sm-1 {
      width: 8.33333333%;
    }
    .col-sm-pull-12 {
      right: 100%;
    }
    .col-sm-pull-11 {
      right: 91.66666667%;
    }
    .col-sm-pull-10 {
      right: 83.33333333%;
    }
    .col-sm-pull-9 {
      right: 75%;
    }
    .col-sm-pull-8 {
      right: 66.66666667%;
    }
    .col-sm-pull-7 {
      right: 58.33333333%;
    }
    .col-sm-pull-6 {
      right: 50%;
    }
    .col-sm-pull-5 {
      right: 41.66666667%;
    }
    .col-sm-pull-4 {
      right: 33.33333333%;
    }
    .col-sm-pull-3 {
      right: 25%;
    }
    .col-sm-pull-2 {
      right: 16.66666667%;
    }
    .col-sm-pull-1 {
      right: 8.33333333%;
    }
    .col-sm-pull-0 {
      right: auto;
    }
    .col-sm-push-12 {
      left: 100%;
    }
    .col-sm-push-11 {
      left: 91.66666667%;
    }
    .col-sm-push-10 {
      left: 83.33333333%;
    }
    .col-sm-push-9 {
      left: 75%;
    }
    .col-sm-push-8 {
      left: 66.66666667%;
    }
    .col-sm-push-7 {
      left: 58.33333333%;
    }
    .col-sm-push-6 {
      left: 50%;
    }
    .col-sm-push-5 {
      left: 41.66666667%;
    }
    .col-sm-push-4 {
      left: 33.33333333%;
    }
    .col-sm-push-3 {
      left: 25%;
    }
    .col-sm-push-2 {
      left: 16.66666667%;
    }
    .col-sm-push-1 {
      left: 8.33333333%;
    }
    .col-sm-push-0 {
      left: auto;
    }
    .col-sm-offset-12 {
      margin-left: 100%;
    }
    .col-sm-offset-11 {
      margin-left: 91.66666667%;
    }
    .col-sm-offset-10 {
      margin-left: 83.33333333%;
    }
    .col-sm-offset-9 {
      margin-left: 75%;
    }
    .col-sm-offset-8 {
      margin-left: 66.66666667%;
    }
    .col-sm-offset-7 {
      margin-left: 58.33333333%;
    }
    .col-sm-offset-6 {
      margin-left: 50%;
    }
    .col-sm-offset-5 {
      margin-left: 41.66666667%;
    }
    .col-sm-offset-4 {
      margin-left: 33.33333333%;
    }
    .col-sm-offset-3 {
      margin-left: 25%;
    }
    .col-sm-offset-2 {
      margin-left: 16.66666667%;
    }
    .col-sm-offset-1 {
      margin-left: 8.33333333%;
    }
    .col-sm-offset-0 {
      margin-left: 0;
    }
  }
  @media (min-width: 992px) {
    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }
    .col-md-12 {
      width: 100%;
    }
    .col-md-11 {
      width: 91.66666667%;
    }
    .col-md-10 {
      width: 83.33333333%;
    }
    .col-md-9 {
      width: 75%;
    }
    .col-md-8 {
      width: 66.66666667%;
    }
    .col-md-7 {
      width: 58.33333333%;
    }
    .col-md-6 {
      width: 50%;
    }
    .col-md-5 {
      width: 41.66666667%;
    }
    .col-md-4 {
      width: 33.33333333%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-2 {
      width: 16.66666667%;
    }
    .col-md-1 {
      width: 8.33333333%;
    }
    .col-md-pull-12 {
      right: 100%;
    }
    .col-md-pull-11 {
      right: 91.66666667%;
    }
    .col-md-pull-10 {
      right: 83.33333333%;
    }
    .col-md-pull-9 {
      right: 75%;
    }
    .col-md-pull-8 {
      right: 66.66666667%;
    }
    .col-md-pull-7 {
      right: 58.33333333%;
    }
    .col-md-pull-6 {
      right: 50%;
    }
    .col-md-pull-5 {
      right: 41.66666667%;
    }
    .col-md-pull-4 {
      right: 33.33333333%;
    }
    .col-md-pull-3 {
      right: 25%;
    }
    .col-md-pull-2 {
      right: 16.66666667%;
    }
    .col-md-pull-1 {
      right: 8.33333333%;
    }
    .col-md-pull-0 {
      right: auto;
    }
    .col-md-push-12 {
      left: 100%;
    }
    .col-md-push-11 {
      left: 91.66666667%;
    }
    .col-md-push-10 {
      left: 83.33333333%;
    }
    .col-md-push-9 {
      left: 75%;
    }
    .col-md-push-8 {
      left: 66.66666667%;
    }
    .col-md-push-7 {
      left: 58.33333333%;
    }
    .col-md-push-6 {
      left: 50%;
    }
    .col-md-push-5 {
      left: 41.66666667%;
    }
    .col-md-push-4 {
      left: 33.33333333%;
    }
    .col-md-push-3 {
      left: 25%;
    }
    .col-md-push-2 {
      left: 16.66666667%;
    }
    .col-md-push-1 {
      left: 8.33333333%;
    }
    .col-md-push-0 {
      left: auto;
    }
    .col-md-offset-12 {
      margin-left: 100%;
    }
    .col-md-offset-11 {
      margin-left: 91.66666667%;
    }
    .col-md-offset-10 {
      margin-left: 83.33333333%;
    }
    .col-md-offset-9 {
      margin-left: 75%;
    }
    .col-md-offset-8 {
      margin-left: 66.66666667%;
    }
    .col-md-offset-7 {
      margin-left: 58.33333333%;
    }
    .col-md-offset-6 {
      margin-left: 50%;
    }
    .col-md-offset-5 {
      margin-left: 41.66666667%;
    }
    .col-md-offset-4 {
      margin-left: 33.33333333%;
    }
    .col-md-offset-3 {
      margin-left: 25%;
    }
    .col-md-offset-2 {
      margin-left: 16.66666667%;
    }
    .col-md-offset-1 {
      margin-left: 8.33333333%;
    }
    .col-md-offset-0 {
      margin-left: 0;
    }
  }
  @media (min-width: 1200px) {
    .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
      float: left;
    }
    .col-lg-12 {
      width: 100%;
    }
    .col-lg-11 {
      width: 91.66666667%;
    }
    .col-lg-10 {
      width: 83.33333333%;
    }
    .col-lg-9 {
      width: 75%;
    }
    .col-lg-8 {
      width: 66.66666667%;
    }
    .col-lg-7 {
      width: 58.33333333%;
    }
    .col-lg-6 {
      width: 50%;
    }
    .col-lg-5 {
      width: 41.66666667%;
    }
    .col-lg-4 {
      width: 33.33333333%;
    }
    .col-lg-3 {
      width: 25%;
    }
    .col-lg-2 {
      width: 16.66666667%;
    }
    .col-lg-1 {
      width: 8.33333333%;
    }
    .col-lg-pull-12 {
      right: 100%;
    }
    .col-lg-pull-11 {
      right: 91.66666667%;
    }
    .col-lg-pull-10 {
      right: 83.33333333%;
    }
    .col-lg-pull-9 {
      right: 75%;
    }
    .col-lg-pull-8 {
      right: 66.66666667%;
    }
    .col-lg-pull-7 {
      right: 58.33333333%;
    }
    .col-lg-pull-6 {
      right: 50%;
    }
    .col-lg-pull-5 {
      right: 41.66666667%;
    }
    .col-lg-pull-4 {
      right: 33.33333333%;
    }
    .col-lg-pull-3 {
      right: 25%;
    }
    .col-lg-pull-2 {
      right: 16.66666667%;
    }
    .col-lg-pull-1 {
      right: 8.33333333%;
    }
    .col-lg-pull-0 {
      right: auto;
    }
    .col-lg-push-12 {
      left: 100%;
    }
    .col-lg-push-11 {
      left: 91.66666667%;
    }
    .col-lg-push-10 {
      left: 83.33333333%;
    }
    .col-lg-push-9 {
      left: 75%;
    }
    .col-lg-push-8 {
      left: 66.66666667%;
    }
    .col-lg-push-7 {
      left: 58.33333333%;
    }
    .col-lg-push-6 {
      left: 50%;
    }
    .col-lg-push-5 {
      left: 41.66666667%;
    }
    .col-lg-push-4 {
      left: 33.33333333%;
    }
    .col-lg-push-3 {
      left: 25%;
    }
    .col-lg-push-2 {
      left: 16.66666667%;
    }
    .col-lg-push-1 {
      left: 8.33333333%;
    }
    .col-lg-push-0 {
      left: auto;
    }
    .col-lg-offset-12 {
      margin-left: 100%;
    }
    .col-lg-offset-11 {
      margin-left: 91.66666667%;
    }
    .col-lg-offset-10 {
      margin-left: 83.33333333%;
    }
    .col-lg-offset-9 {
      margin-left: 75%;
    }
    .col-lg-offset-8 {
      margin-left: 66.66666667%;
    }
    .col-lg-offset-7 {
      margin-left: 58.33333333%;
    }
    .col-lg-offset-6 {
      margin-left: 50%;
    }
    .col-lg-offset-5 {
      margin-left: 41.66666667%;
    }
    .col-lg-offset-4 {
      margin-left: 33.33333333%;
    }
    .col-lg-offset-3 {
      margin-left: 25%;
    }
    .col-lg-offset-2 {
      margin-left: 16.66666667%;
    }
    .col-lg-offset-1 {
      margin-left: 8.33333333%;
    }
    .col-lg-offset-0 {
      margin-left: 0;
    }
  }
  table {
    background-color: transparent;
  }
  caption {
    padding-top: 8px;
    padding-bottom: 8px;
    color: #777;
    text-align: left;
  }
  th {
    text-align: left;
  }
  .table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
  }
  .table > thead > tr > th,
  .table > tbody > tr > th,
  .table > tfoot > tr > th,
  .table > thead > tr > td,
  .table > tbody > tr > td,
  .table > tfoot > tr > td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
  }
  .table > thead > tr > th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
  }
  .table > caption + thead > tr:first-child > th,
  .table > colgroup + thead > tr:first-child > th,
  .table > thead:first-child > tr:first-child > th,
  .table > caption + thead > tr:first-child > td,
  .table > colgroup + thead > tr:first-child > td,
  .table > thead:first-child > tr:first-child > td {
    border-top: 0;
  }
  .table > tbody + tbody {
    border-top: 2px solid #ddd;
  }
  .table .table {
    background-color: #fff;
  }
  .table-condensed > thead > tr > th,
  .table-condensed > tbody > tr > th,
  .table-condensed > tfoot > tr > th,
  .table-condensed > thead > tr > td,
  .table-condensed > tbody > tr > td,
  .table-condensed > tfoot > tr > td {
    padding: 5px;
  }
  .table-bordered {
    border: 1px solid #ddd;
  }
  .table-bordered > thead > tr > th,
  .table-bordered > tbody > tr > th,
  .table-bordered > tfoot > tr > th,
  .table-bordered > thead > tr > td,
  .table-bordered > tbody > tr > td,
  .table-bordered > tfoot > tr > td {
    border: 1px solid #ddd;
  }
  .table-bordered > thead > tr > th,
  .table-bordered > thead > tr > td {
    border-bottom-width: 2px;
  }
  .table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #f9f9f9;
  }
  .table-hover > tbody > tr:hover {
    background-color: #f5f5f5;
  }
  table col[class*="col-"] {
    requisitionsition: static;
    display: table-column;
    float: none;
  }
  table td[class*="col-"],
  table th[class*="col-"] {
    requisitionsition: static;
    display: table-cell;
    float: none;
  }
  .table > thead > tr > td.active,
  .table > tbody > tr > td.active,
  .table > tfoot > tr > td.active,
  .table > thead > tr > th.active,
  .table > tbody > tr > th.active,
  .table > tfoot > tr > th.active,
  .table > thead > tr.active > td,
  .table > tbody > tr.active > td,
  .table > tfoot > tr.active > td,
  .table > thead > tr.active > th,
  .table > tbody > tr.active > th,
  .table > tfoot > tr.active > th {
    background-color: #f5f5f5;
  }
  .table-hover > tbody > tr > td.active:hover,
  .table-hover > tbody > tr > th.active:hover,
  .table-hover > tbody > tr.active:hover > td,
  .table-hover > tbody > tr:hover > .active,
  .table-hover > tbody > tr.active:hover > th {
    background-color: #e8e8e8;
  }
  .table > thead > tr > td.success,
  .table > tbody > tr > td.success,
  .table > tfoot > tr > td.success,
  .table > thead > tr > th.success,
  .table > tbody > tr > th.success,
  .table > tfoot > tr > th.success,
  .table > thead > tr.success > td,
  .table > tbody > tr.success > td,
  .table > tfoot > tr.success > td,
  .table > thead > tr.success > th,
  .table > tbody > tr.success > th,
  .table > tfoot > tr.success > th {
    background-color: #dff0d8;
  }
  .table-hover > tbody > tr > td.success:hover,
  .table-hover > tbody > tr > th.success:hover,
  .table-hover > tbody > tr.success:hover > td,
  .table-hover > tbody > tr:hover > .success,
  .table-hover > tbody > tr.success:hover > th {
    background-color: #d0e9c6;
  }
  .table > thead > tr > td.info,
  .table > tbody > tr > td.info,
  .table > tfoot > tr > td.info,
  .table > thead > tr > th.info,
  .table > tbody > tr > th.info,
  .table > tfoot > tr > th.info,
  .table > thead > tr.info > td,
  .table > tbody > tr.info > td,
  .table > tfoot > tr.info > td,
  .table > thead > tr.info > th,
  .table > tbody > tr.info > th,
  .table > tfoot > tr.info > th {
    background-color: #d9edf7;
  }
  .table-hover > tbody > tr > td.info:hover,
  .table-hover > tbody > tr > th.info:hover,
  .table-hover > tbody > tr.info:hover > td,
  .table-hover > tbody > tr:hover > .info,
  .table-hover > tbody > tr.info:hover > th {
    background-color: #c4e3f3;
  }
  .table > thead > tr > td.warning,
  .table > tbody > tr > td.warning,
  .table > tfoot > tr > td.warning,
  .table > thead > tr > th.warning,
  .table > tbody > tr > th.warning,
  .table > tfoot > tr > th.warning,
  .table > thead > tr.warning > td,
  .table > tbody > tr.warning > td,
  .table > tfoot > tr.warning > td,
  .table > thead > tr.warning > th,
  .table > tbody > tr.warning > th,
  .table > tfoot > tr.warning > th {
    background-color: #fcf8e3;
  }
  .table-hover > tbody > tr > td.warning:hover,
  .table-hover > tbody > tr > th.warning:hover,
  .table-hover > tbody > tr.warning:hover > td,
  .table-hover > tbody > tr:hover > .warning,
  .table-hover > tbody > tr.warning:hover > th {
    background-color: #faf2cc;
  }
  .table > thead > tr > td.danger,
  .table > tbody > tr > td.danger,
  .table > tfoot > tr > td.danger,
  .table > thead > tr > th.danger,
  .table > tbody > tr > th.danger,
  .table > tfoot > tr > th.danger,
  .table > thead > tr.danger > td,
  .table > tbody > tr.danger > td,
  .table > tfoot > tr.danger > td,
  .table > thead > tr.danger > th,
  .table > tbody > tr.danger > th,
  .table > tfoot > tr.danger > th {
    background-color: #f2dede;
  }
  .table-hover > tbody > tr > td.danger:hover,
  .table-hover > tbody > tr > th.danger:hover,
  .table-hover > tbody > tr.danger:hover > td,
  .table-hover > tbody > tr:hover > .danger,
  .table-hover > tbody > tr.danger:hover > th {
    background-color: #ebcccc;
  }
  .table-resrequisitionnsive {
    min-height: .01%;
    overflow-x: auto;
  }
  @media screen and (max-width: 767px) {
    .table-resrequisitionnsive {
      width: 100%;
      margin-bottom: 15px;
      overflow-y: hidden;
      -ms-overflow-style: -ms-autohiding-scrollbar;
      border: 1px solid #ddd;
    }
    .table-resrequisitionnsive > .table {
      margin-bottom: 0;
    }
    .table-resrequisitionnsive > .table > thead > tr > th,
    .table-resrequisitionnsive > .table > tbody > tr > th,
    .table-resrequisitionnsive > .table > tfoot > tr > th,
    .table-resrequisitionnsive > .table > thead > tr > td,
    .table-resrequisitionnsive > .table > tbody > tr > td,
    .table-resrequisitionnsive > .table > tfoot > tr > td {
      white-space: nowrap;
    }
    .table-resrequisitionnsive > .table-bordered {
      border: 0;
    }
    .table-resrequisitionnsive > .table-bordered > thead > tr > th:first-child,
    .table-resrequisitionnsive > .table-bordered > tbody > tr > th:first-child,
    .table-resrequisitionnsive > .table-bordered > tfoot > tr > th:first-child,
    .table-resrequisitionnsive > .table-bordered > thead > tr > td:first-child,
    .table-resrequisitionnsive > .table-bordered > tbody > tr > td:first-child,
    .table-resrequisitionnsive > .table-bordered > tfoot > tr > td:first-child {
      border-left: 0;
    }
    .table-resrequisitionnsive > .table-bordered > thead > tr > th:last-child,
    .table-resrequisitionnsive > .table-bordered > tbody > tr > th:last-child,
    .table-resrequisitionnsive > .table-bordered > tfoot > tr > th:last-child,
    .table-resrequisitionnsive > .table-bordered > thead > tr > td:last-child,
    .table-resrequisitionnsive > .table-bordered > tbody > tr > td:last-child,
    .table-resrequisitionnsive > .table-bordered > tfoot > tr > td:last-child {
      border-right: 0;
    }
    .table-resrequisitionnsive > .table-bordered > tbody > tr:last-child > th,
    .table-resrequisitionnsive > .table-bordered > tfoot > tr:last-child > th,
    .table-resrequisitionnsive > .table-bordered > tbody > tr:last-child > td,
    .table-resrequisitionnsive > .table-bordered > tfoot > tr:last-child > td {
      border-bottom: 0;
    }
  }
  fieldset {
    min-width: 0;
    padding: 0;
    margin: 0;
    border: 0;
  }
  legend {
    display: block;
    width: 100%;
    padding: 0;
    margin-bottom: 20px;
    font-size: 21px;
    line-height: inherit;
    color: #333;
    border: 0;
    border-bottom: 1px solid #e5e5e5;
  }
  label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
  }
  .collapse {
    display: none;
  }
  .collapse.in {
    display: block;
  }
  tr.collapse.in {
    display: table-row;
  }
  tbody.collapse.in {
    display: table-row-group;
  }
  .carousel {
    requisitionsition: relative;
  }
  .carousel-inner {
    requisitionsition: relative;
    width: 100%;
    overflow: hidden;
  }
  .carousel-inner > .item {
    requisitionsition: relative;
    display: none;
    -webkit-transition: .6s ease-in-out left;
         -o-transition: .6s ease-in-out left;
            transition: .6s ease-in-out left;
  }
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    line-height: 1;
  }
  @media all and (transform-3d), (-webkit-transform-3d) {
    .carousel-inner > .item {
      -webkit-transition: -webkit-transform .6s ease-in-out;
           -o-transition:      -o-transform .6s ease-in-out;
              transition:         transform .6s ease-in-out;

      -webkit-backface-visibility: hidden;
              backface-visibility: hidden;
      -webkit-perspective: 1000px;
              perspective: 1000px;
    }
    .carousel-inner > .item.next,
    .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(100%, 0, 0);
              transform: translate3d(100%, 0, 0);
    }
    .carousel-inner > .item.prev,
    .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-100%, 0, 0);
              transform: translate3d(-100%, 0, 0);
    }
    .carousel-inner > .item.next.left,
    .carousel-inner > .item.prev.right,
    .carousel-inner > .item.active {
      left: 0;
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
  }
  .carousel-inner > .active,
  .carousel-inner > .next,
  .carousel-inner > .prev {
    display: block;
  }
  .carousel-inner > .active {
    left: 0;
  }
  .carousel-inner > .next,
  .carousel-inner > .prev {
    requisitionsition: absolute;
    top: 0;
    width: 100%;
  }
  .carousel-inner > .next {
    left: 100%;
  }
  .carousel-inner > .prev {
    left: -100%;
  }
  .carousel-inner > .next.left,
  .carousel-inner > .prev.right {
    left: 0;
  }
  .carousel-inner > .active.left {
    left: -100%;
  }
  .carousel-inner > .active.right {
    left: 100%;
  }
  .carousel-control {
    requisitionsition: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 15%;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
    background-color: rgba(0, 0, 0, 0);
    filter: alpha(opacity=50);
    opacity: .5;
  }
  .carousel-control.left {
    background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, .0001)));
    background-image:         linear-gradient(to right, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
    background-repeat: repeat-x;
  }
  .carousel-control.right {
    right: 0;
    left: auto;
    background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .0001)), to(rgba(0, 0, 0, .5)));
    background-image:         linear-gradient(to right, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);
    background-repeat: repeat-x;
  }
  .carousel-control:hover,
  .carousel-control:focus {
    color: #fff;
    text-decoration: none;
    filter: alpha(opacity=90);
    outline: 0;
    opacity: .9;
  }
  .carousel-control .icon-prev,
  .carousel-control .icon-next,
  .carousel-control .glyphicon-chevron-left,
  .carousel-control .glyphicon-chevron-right {
    requisitionsition: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
    margin-top: -10px;
  }
  .carousel-control .icon-prev,
  .carousel-control .glyphicon-chevron-left {
    left: 50%;
    margin-left: -10px;
  }
  .carousel-control .icon-next,
  .carousel-control .glyphicon-chevron-right {
    right: 50%;
    margin-right: -10px;
  }
  .carousel-control .icon-prev,
  .carousel-control .icon-next {
    width: 20px;
    height: 20px;
    font-family: serif;
    line-height: 1;
  }
  .carousel-control .icon-prev:before {
    content: '\2039';
  }
  .carousel-control .icon-next:before {
    content: '\203a';
  }
  .carousel-indicators {
    requisitionsition: absolute;
    bottom: 10px;
    left: 50%;
    z-index: 15;
    width: 60%;
    padding-left: 0;
    margin-left: -30%;
    text-align: center;
    list-style: none;
  }
  .carousel-indicators li {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 1px;
    text-indent: -999px;
    cursor: requisitioninter;
    background-color: #000 \9;
    background-color: rgba(0, 0, 0, 0);
    border: 1px solid #fff;
    border-radius: 10px;
  }
  .carousel-indicators .active {
    width: 12px;
    height: 12px;
    margin: 0;
    background-color: #fff;
  }
  .carousel-caption {
    requisitionsition: absolute;
    right: 15%;
    bottom: 20px;
    left: 15%;
    z-index: 10;
    padding-top: 20px;
    padding-bottom: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
  }
  .carousel-caption .btn {
    text-shadow: none;
  }
  @media screen and (min-width: 768px) {
    .carousel-control .glyphicon-chevron-left,
    .carousel-control .glyphicon-chevron-right,
    .carousel-control .icon-prev,
    .carousel-control .icon-next {
      width: 30px;
      height: 30px;
      margin-top: -10px;
      font-size: 30px;
    }
    .carousel-control .glyphicon-chevron-left,
    .carousel-control .icon-prev {
      margin-left: -10px;
    }
    .carousel-control .glyphicon-chevron-right,
    .carousel-control .icon-next {
      margin-right: -10px;
    }
    .carousel-caption {
      right: 20%;
      left: 20%;
      padding-bottom: 30px;
    }
    .carousel-indicators {
      bottom: 20px;
    }
  }
  .clearfix:before,
  .clearfix:after,
  .dl-horizontal dd:before,
  .dl-horizontal dd:after,
  .container:before,
  .container:after,
  .container-fluid:before,
  .container-fluid:after,
  .row:before,
  .row:after,
  .form-horizontal .form-group:before,
  .form-horizontal .form-group:after,
  .btn-toolbar:before,
  .btn-toolbar:after,
  .btn-group-vertical > .btn-group:before,
  .btn-group-vertical > .btn-group:after,
  .nav:before,
  .nav:after,
  .navbar:before,
  .navbar:after,
  .navbar-header:before,
  .navbar-header:after,
  .navbar-collapse:before,
  .navbar-collapse:after,
  .pager:before,
  .pager:after,
  .panel-body:before,
  .panel-body:after,
  .modal-header:before,
  .modal-header:after,
  .modal-footer:before,
  .modal-footer:after {
    display: table;
    content: " ";
  }
  .clearfix:after,
  .dl-horizontal dd:after,
  .container:after,
  .container-fluid:after,
  .row:after,
  .form-horizontal .form-group:after,
  .btn-toolbar:after,
  .btn-group-vertical > .btn-group:after,
  .nav:after,
  .navbar:after,
  .navbar-header:after,
  .navbar-collapse:after,
  .pager:after,
  .panel-body:after,
  .modal-header:after,
  .modal-footer:after {
    clear: both;
  }
  .center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
  }
  .pull-right {
    float: right !important;
  }
  .pull-left {
    float: left !important;
  }
  .hide {
    display: none !important;
  }
  .show {
    display: block !important;
  }
  .invisible {
    visibility: hidden;
  }
  .text-hide {
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }
  .hidden {
    display: none !important;
  }
  .affix {
    requisitionsition: fixed;
  }
  @-ms-viewrequisitionrt {
    width: device-width;
  }
  .visible-xs,
  .visible-sm,
  .visible-md,
  .visible-lg {
    display: none !important;
  }
  .visible-xs-block,
  .visible-xs-inline,
  .visible-xs-inline-block,
  .visible-sm-block,
  .visible-sm-inline,
  .visible-sm-inline-block,
  .visible-md-block,
  .visible-md-inline,
  .visible-md-inline-block,
  .visible-lg-block,
  .visible-lg-inline,
  .visible-lg-inline-block {
    display: none !important;
  }
  @media (max-width: 767px) {
    .visible-xs {
      display: block !important;
    }
    table.visible-xs {
      display: table !important;
    }
    tr.visible-xs {
      display: table-row !important;
    }
    th.visible-xs,
    td.visible-xs {
      display: table-cell !important;
    }
  }
  @media (max-width: 767px) {
    .visible-xs-block {
      display: block !important;
    }
  }
  @media (max-width: 767px) {
    .visible-xs-inline {
      display: inline !important;
    }
  }
  @media (max-width: 767px) {
    .visible-xs-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm {
      display: block !important;
    }
    table.visible-sm {
      display: table !important;
    }
    tr.visible-sm {
      display: table-row !important;
    }
    th.visible-sm,
    td.visible-sm {
      display: table-cell !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm-block {
      display: block !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm-inline {
      display: inline !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md {
      display: block !important;
    }
    table.visible-md {
      display: table !important;
    }
    tr.visible-md {
      display: table-row !important;
    }
    th.visible-md,
    td.visible-md {
      display: table-cell !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md-block {
      display: block !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md-inline {
      display: inline !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg {
      display: block !important;
    }
    table.visible-lg {
      display: table !important;
    }
    tr.visible-lg {
      display: table-row !important;
    }
    th.visible-lg,
    td.visible-lg {
      display: table-cell !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg-block {
      display: block !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg-inline {
      display: inline !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg-inline-block {
      display: inline-block !important;
    }
  }
  @media (max-width: 767px) {
    .hidden-xs {
      display: none !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .hidden-sm {
      display: none !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .hidden-md {
      display: none !important;
    }
  }
  @media (min-width: 1200px) {
    .hidden-lg {
      display: none !important;
    }
  }
  .visible-print {
    display: none !important;
  }
  @media print {
    .visible-print {
      display: block !important;
    }
    table.visible-print {
      display: table !important;
    }
    tr.visible-print {
      display: table-row !important;
    }
    th.visible-print,
    td.visible-print {
      display: table-cell !important;
    }
  }
  .visible-print-block {
    display: none !important;
  }
  @media print {
    .visible-print-block {
      display: block !important;
    }
  }
  .visible-print-inline {
    display: none !important;
  }
  @media print {
    .visible-print-inline {
      display: inline !important;
    }
  }
  .visible-print-inline-block {
    display: none !important;
  }
  @media print {
    .visible-print-inline-block {
      display: inline-block !important;
    }
  }
  @media print {
    .hidden-print {
      display: none !important;
    }
  }

  body {
      min-width: 100px;
      min-height: 100%;
      font-family: "Roboto", "-apple-system", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      font-smoothing: antialiased;
      line-height: 1.5;
      font-size: 14px;
  }

  h4, h3, h5 {
    font-family: "Roboto", "-apple-system", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
  }

  .isPay {
    background: green;
    width: 140px;
    requisitionsition: absolute;
    top: -40px;
    right: -70px;
    text-align: center;
    padding: 5px;
    color: white;
    -webkit-transform: rotate(30deg);
    -moz-transform: rotate(30deg);
    -o-transform: rotate(30deg);
    -ms-transform: rotate(30deg);
    transform: rotate(30deg);
  }
  .isNoPa
    width: 200px;
    requisitionsition: absolute;
    top: -30px;
    right: -70px;
    text-align: center;
    padding: 5px;
    color: white;
    -webkit-transform: rotate(30deg);
    -moz-transform: rotate(30deg);
    -o-transform: rotate(30deg);
    -ms-transform: rotate(30deg);
    transform: rotate(30deg);
  }
  /*# sourceMappingURL=bootstrap.css.map */
    .font-100 {
      font-size: 8pt;
    }
    .font-144 {
      font-size: 8pt;
    }
    .font-166 {
      font-size: 16pt;
    }
    .font-188 {
      font-size: 18pt;
    }
    .font-200 {
      font-size: 20pt;
    }
    .font-100 {
      font-size: 8pt;
    }
    .font-88 {
      font-size: 8pt;
    }
    .font-888 {
      font-size: 8pt;
    }
    .center {
      text-align: center !important;
    }
    .bold {
      font-weight: 600 !important;
      color: black;
    }
    .bg-primary {
      background-color: #033a78 !important;
      padding: .7em;
    }
    .mt-md {
      margin-top: .5rem;
    }
    .mt-lg {
      margin-top: 1rem;
    }
    .mt-xl {
      margin-top: 2rem;
    }
    .bolder {
      font-weight: 800;
    }
    .bg-gray {
      background-color: #e3e9ec !important;
    }
    .center{
      text-align: center;
    }
    .footer {
      requisitionsition: fixed; 
      bottom: 0cm; 
      left: 0cm; 
      right: 0cm;
    }
    .footer2 {
      requisitionsition: fixed; 
      bottom: 0cm; 
      left: 0cm; 
      right: 0cm;
    }
    div > b {
      font-weight: bold !important;
    }
    .text-right {
      text-align: right !important;
    }
    @page { margin: .5cm 1.5cm; requisitionsition: relative;}
    .page_break { page-break-before: always; }
  </style>
</head>
  <body>
    <main>
      <div style="margin-top:" class="row" style="height: 40px !important; padding-top: .1em; padding-bottom: .1em">
      	<div style="height: 35px !important; padding-top: .2em; padding-bottom: 0em; font-size: 14pt;" class="col-xs-8 bg-primary" style="color: white !important; font-weight: 600">
          @if($requisition['provider']['country'] == 'ES')
            <b>REQUISICIÓN</b><span style="float: right;"><b>RFQ# {{$requisition['code']}}</b></span>
            @else
              <b>REQUISITION</b><span style="float: right;"><b>RFQ# {{$requisition['code']}}</b></span>
          @endif
      	</div>
      	<div class="col-xs-3">
      		<center>
      			<img style="margin-right: -20px" class="mt-sm" src="{{url('images/assets/ats.png')}}" width="140px">
      		</center>
      	</div>
      </div>
      <!--Providers-->
        <div class="row" style="padding-top: .1em; padding-bottom: .1em; margin-top: -70px !important">
          <div class="col-xs-5" style="color: black; font-weight: 600; margin-left: -15px;">
            <div class="row">
                <div class="col-xs-12 font-100" style="font-weight: 600">
                @if($requisition['provider']['country'] == 'ES')
                  <b>PROVEEDOR:</b>
                  @else  
                  <b>PROVIDER:</b>
                @endif
              </div><br>
              <div class="col-xs-12 font-100">
                <b style="color: black; font-weight: normal !important">
                  @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Nombre:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Name:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">{{$requisition['provider']['name']}}</span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100" style="margin-top: 0px">
                <b style="color: black; font-weight: normal !important">
                 @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Contacto:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Contact Name:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">{{$requisition['provider']['manager']}}</span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100" style="margin-top: 0px">
                <b style="color: black; font-weight: normal !important">
                  @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Pais:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Country:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$requisition['provider']['country']}}</span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100" style="margin-top: 0px">
                <b style="color: black; font-weight: normal !important">
                  @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Dirección:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Address:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$requisition['provider']['address']}}</span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100" style="margin-top: 0px">
                <b style="color: black; font-weight: normal !important">
                  @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Teléfono:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Phone:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$requisition['provider']['phone']}}</span>
                </b>
              </div>
            </div>
          </div>
          <div class="col-xs-4" style="color: black; font-weight: 600; margin-left: -15px;">
            <div class="row">
              <div class="col-xs-12 font-100" style="font-weight: 600">
                @if($requisition['provider']['country'] == 'ES')
                  <b>DETALLES:</b>
                  @else  
                    <b>DETAILS:</b>
                @endif
              </div><br>
              <div class="col-xs-12 font-100">
                <b style="color: black; font-weight: normal !important">
                   @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Requisición:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Requisition:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">
                    <b>RFQ# {{$requisition['code']}}</b>
                  </span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100">
                <b style="color: black; font-weight: normal !important">
                   @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Pais:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Contry:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">
                    {{$requisition['country']}}
                  </span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100">
                <b style="color: black; font-weight: normal !important">
                   @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Zona sismica:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Seismic zone:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">
                    {{$requisition['seismic_zone']}}
                  </span>
                </b>
              </div><br>
              <div class="col-xs-12 font-100">
                <b style="color: black; font-weight: normal !important">
                   @if($requisition['provider']['country'] == 'ES')
                    <b style="color: black; font-weight: normal !important">Numero silo:</b>
                    @else  
                    <b style="color: black; font-weight: normal !important">Total silo:</b>
                  @endif <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">
                    {{count($requisition['silos'])}}
                  </span>
                </b>
              </div><br>
            </div>
          </div>
        </div>
      <!--end providers-->
      <!--items-->
      <br><br>
      <div class="row mt-sm" style="margin-left: -30px">
        <div class="col-xs-12">
          <table style="font-size: 8pt; width: 100%;">
            <thead>
              <tr class="bg-primary" style="color: white; padding: .4em" class="font-100">
                <td style="padding: .3em .3em; font-size: 8pt; width: 20px; text-align: center;">
                  @if($requisition['provider']['country'] == 'ES')
                    POS
                    @else  
                      POS
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; width: 70px; text-align: left">
                  @if($requisition['provider']['country'] == 'ES')
                    MODELO
                    @else  
                      MODEL
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; width: 70px; text-align: left">
                  @if($requisition['provider']['country'] == 'ES')
                    TIPO
                    @else  
                       TYPE
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; width: 120px; text-align: left">
                  @if($requisition['provider']['country'] == 'ES')
                    PRODUCTO
                    @else  
                      PRODUCT
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; ; width: 80px; text-align: right">
                  @if($requisition['provider']['country'] == 'ES')
                    DENSIDAD
                    @else  
                      DENSITY
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: right; width: 70px">
                  @if($requisition['provider']['country'] == 'ES')
                    CANTIDAD
                    @else  
                      QUANTITY
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; width: 50px; text-align: right">
                  @if($requisition['provider']['country'] == 'ES')
                    VOLTAGE
                    @else  
                      VOLTAGE
                  @endif
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; width: 50px; text-align: right">
                  @if($requisition['provider']['country'] == 'ES')
                    CAPACIDAD
                    @else  
                      CAPACITY
                  @endif
                </td>
              </tr>
            </thead>
            <tbody>
              @foreach($requisition['silos'] as $key => $item)
              <tr class="@if(($key + 1) % 2 == 0) bg-gray @endif">
                <td style="padding: .3em .3em; font-size: 8pt; text-align: center; color: black">
                  {{$key + 1}}
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: left; color: black">
                  {{$item['model']}}
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: left; color: black">
                  {{$item['type']}}
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: left; color: black">
                  {{$item['product']}}
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: right; color: black">
                  {{$item['density']}}
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: right; color: black">
                  {{$item['quantity']}}.00
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: right; color: black">
                  {{$item['voltage']}}
                </td>
                <td style="padding: .3em .3em; font-size: 8pt; text-align: right; color: black">
                  {{$item['capacity']}}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <!--end items-->
      <!--comments-->
      <br><br>
      <div class="row mt-sm" style="margin-left: -30px">
        <div class="col-xs-12 font-100" style="font-weight: 600; color: black;">
          @if($requisition['provider']['country'] == 'ES')
          <b>COMENTARIOS:</b>
          @else  
          <b>COMMENTS:</b>
          @endif
        </div><br>
        <div class="col-xs-12 font-100" style="font-weight: 600">
          <b style="color: black; font-weight: normal !important">
            <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">
            {{$requisition['comments']}}
          </span>
        </b>
        </div><br>
      </div>
    </main>
    <div class="footer" style="position: fixed; bottom: 0px;">
      <div class="row" style="">
        <div class="col-xs-11 text-center font-100">
          AGRIQUOTE VER. 1.0
        </div>
      </div>
    </div>
    <div class="page_break"></div>
      <!--items section-->
    @foreach($requisition['silos'] as $key => $item)
      <div class="row" style="margin-right: -20px">
        <div style="height: 37px !important; padding-top: .2em; padding-bottom: .3em; text-align: center" class="col-xs-9 font-188 bg-primary">
          <b>AGRI TECHNOLOGY SOLUTIONS</b>
        </div>
        <div class="col-xs-2">
          <img style="margin-right: -20px" class="mt-sm" src="{{url('images/assets/ats.png')}}" width="90px">
        </div>
      </div>
      <br>
      <div class="row mt-lg" style="margin-left: -15px">
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-1 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            POSICIÓN:
          @else  
            POSITION:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-2 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            CÓDIGO:
          @else  
            CODE:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-3 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            PRODUCTO:
          @else  
            PRODUCT:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: right" class="col-xs-2 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            DENSIDAD KG/M3:
          @else  
            DENSITY:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: right" class="col-xs-1 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            CANTIDAD:
          @else  
            QUANTITY:
          @endif
        </div>
      </div>
      <div class="row mt-sm" style="margin-left: -20px">
        <div class="col-xs-1 font-100" style="text-align: center; color: black">
          {{$key + 1}}
        </div>
        <div class="col-xs-2 font-100" style="text-align: center; color: black">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['model']}}</b>
          </p>
        </div>
        <div class="col-xs-3 font-100" style="text-align: center; color: black; margin-left: 5px; text-align: center">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['product']}}</b>
          </p>
        </div>
        <div class="col-xs-2 font-100" style="text-align: center; color: black; margin-left: 5px; text-align: right">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['density']}}</b>
          </p>
        </div>
        <div class="col-xs-1 font-100" style="text-align: center; color: black; margin-left: 5px; text-align: center">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['quantity']}}.00</b>
          </p>
        </div>
      </div>
      <div class="row mt-md" style="margin-left: -15px">
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-1 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            VOLTAGE:
          @else  
            VOLTAGE:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-2 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            TIPO:
          @else  
            TYPE:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-3 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            SISTEMA TERMOMETRIA:
          @else  
            <span style="text-transform: uppercase !important;">
              THERMOMETRY SYSTEM:
            </span>
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: right" class="col-xs-2 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            POWERSWEEP:
          @else  
            POWERSWEEP:
          @endif
        </div>
        <div style="height: 20px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: right" class="col-xs-1 font-100 bg-primary">
          @if($requisition['provider']['country'] == 'ES')
            BARREDORA:
          @else  
            SWEEPERS:
          @endif
        </div>
      </div>
      <div class="row mt-sm" style="margin-left: -20px">
        <div class="col-xs-1 font-100" style="text-align: center; color: black">
          {{$item['voltage']}}
        </div>
        <div class="col-xs-2 font-100" style="text-align: center; color: black">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['type']}}</b>
          </p>
        </div>
        <div class="col-xs-3 font-100" style="text-align: center; color: black; margin-left: 5px; text-align: center">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['thermometry_system']}}</b>
          </p>
        </div>
        <div class="col-xs-2 font-100" style="text-align: center; color: black; margin-left: 5px; text-align: right">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['powersweep']}}</b>
          </p>
        </div>
        <div class="col-xs-1 font-100" style="text-align: center; color: black; margin-left: 5px; text-align: center">
          <p style="text-transform: uppercase;">
            <b style="font-weight: normal !important">{{$item['sweepers']}}</b>
          </p>
        </div>
      </div>
      <table style="width: 450px; margin: 30px auto;">
        <tr style="height: 30px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-2 font-100 bg-primary">
          <td>
            <p style="color: white; font-size: 10pt;">
              <b>
                @if($requisition['provider']['country'] == 'ES')
                  Detalles
                  @else  
                    Details
                  @endif
              </b>
            </p>
          </td>
        </tr>
      </table>
       <table style="width: 450px; margin: -20px auto;">
        <tbody>
          <tr>
            <td style="width: 50%;">
              <p style="text-align: left; font-size: 9pt;">
                <b>
                  @if($requisition['provider']['country'] == 'ES')
                    Capacidad Barredora: <span style="font-weight: normal;">
                      @if($item['capacity_sweepers'])
                        {{$item['capacity_sweepers']}} (t/h [bu/h])
                        @else
                          No aplica.
                      @endif
                    </span>
                  @else  
                    <span style="text-transform: capitalize;">
                      Capacity sweepers: <span style="font-weight: normal;">
                        @if($item['capacity_sweepers'])
                          {{$item['capacity_sweepers']}} (t/h [bu/h])
                          @else
                            Don´t Apply.
                        @endif
                      </span>
                    </span>
                  @endif
                </b>
              </p>
            </td>
            <td style="width: 50%;">
              <p style="text-align: left; font-size: 9pt;">
                <b>
                  @if($requisition['provider']['country'] == 'ES')
                    Powersweep extension: <span style="font-weight: normal;">
                      @if($item['powersweep'] == 'Si')
                        {{$item['powersweep_extension']}} (m [ft])
                        @else
                          No aplica.
                      @endif
                    </span>
                  @else  
                    <span style="text-transform: capitalize;">
                      Powersweep extension: <span style="font-weight: normal;">
                      @if($item['powersweep'] == 'Si')
                        {{$item['powersweep_extension']}} (m [ft])
                        @else
                          Don´t apply
                      @endif
                      </span>
                    </span>
                  @endif
                </b>
              </p>
            </td>
          </tr>
        </tbody>
      </table>
      <table style="width: 450px; margin: 10px auto;">
        <tbody>
          <tr>
            <td style="width: 50%;">
              <p style="text-align: left; font-size: 9pt;">
                <b>
                  @if($requisition['provider']['country'] == 'ES')
                    Tipo termometria: <span style="font-weight: normal;">
                      @if($item['thermometry_system'] == 'Si')
                        {{$item['thermometry_system_type']}}
                        @else
                          No aplica.
                      @endif
                    </span>
                  @else  
                    <span style="text-transform: capitalize;">
                      Thermometry system type: <span style="font-weight: normal;">
                        @if($item['thermometry_system'] == 'Si')
                          {{$item['thermometry_system_type']}}
                          @else
                            Don´t Apply.
                        @endif
                      </span>
                    </span>
                  @endif
                </b>
              </p>
            </td>
            <td style="text-align: left; width: 50%;">
              <p style="text-align: left; font-size: 9pt;">
                <b>
                  @if($requisition['provider']['country'] == 'ES')
                    Sistema Aereación: <span style="font-weight: normal;">
                      {{$item['aeration_system']}}
                    </span>
                  @else  
                    <span style="text-transform: capitalize;">
                      Aeration System: <span style="font-weight: normal;">
                        {{$item['aeration_system']}}
                      </span>
                    </span>
                  @endif
                </b>
              </p>
            </td>
          </tr>
        </tbody>
      </table>
      <table style="width: 450px; margin: -20px auto;">
        <tbody>
          <tr>
            <td style="width: 50%;">
              <p style="text-align: left; font-size: 9pt;">
                <b>
                  @if($requisition['provider']['country'] == 'ES')
                    Pasarela: <span style="font-weight: normal;">
                      {{$item['runway']}}
                    </span>
                  @else  
                    <span style="text-transform: capitalize;">
                      Runway: <span style="font-weight: normal;">
                        {{$item['runway']}}
                      </span>
                    </span>
                  @endif
                </b>
            </td>
            @if(isset($item['complement']))
              <td style="text-align: left; width: 50%;">
                <p style="text-align: left; font-size: 9pt;">
                  <b>
                    @if($requisition['provider']['country'] == 'ES')
                      Ancho del Transportador: <span style="font-weight: normal;">
                        {{$item['complement']['conveyor_width']}} (mm)
                      </span>
                    @else  
                      <span style="text-transform: capitalize;">
                       Conveyor width: <span style="font-weight: normal;">
                          {{$item['complement']['conveyor_width']}} (mm)
                        </span>
                      </span>
                    @endif
                  </b>
                </p>
              </td>
            @endif
          </tr>
        </tbody>
      </table>
      @if(isset($item['complement']))
        <table style="width: 450px; margin: 10px auto;">
          <tbody>
            <tr>
              <td style="width: 50%;">
                <p style="text-align: left; font-size: 9pt;">
                  <b>
                    @if($requisition['provider']['country'] == 'ES')
                      Longitud Total: <span style="font-weight: normal;">
                        @if($item['complement']['total_length'])
                          {{$item['complement']['total_length']}}(m [ft])
                          @else
                            No aplica.
                        @endif
                      </span>
                    @else  
                      <span style="text-transform: capitalize;">
                        Total Length: <span style="font-weight: normal;">
                          @if($item['complement']['total_length'])
                            {{$item['complement']['total_length']}}(m [ft])
                            @else
                              Don´t Apply.
                          @endif
                        </span>
                      </span>
                    @endif
                  </b>
                </p>
              </td>
              <td style="text-align: left; width: 50%;">
                <p style="text-align: left; font-size: 9pt;">
                  <b>
                    @if($requisition['provider']['country'] == 'ES')
                      Soporte de Cupula: <span style="font-weight: normal;">
                        @if($item['complement']['cupula'])
                          {{$item['complement']['cupula']}}
                          @else
                            No aplica.
                        @endif
                      </span>
                    @else  
                      <span style="text-transform: capitalize;">
                        Dome Support: <span style="font-weight: normal;">
                          @if($item['complement']['cupula'])
                            {{$item['complement']['cupula']}}
                            @else
                              Don´t Apply.
                          @endif
                        </span>
                      </span>
                    @endif
                  </b>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        <table style="width: 450px; margin: -20px auto;">
          <tbody>
            <tr>
              <td style="text-align: left; width: 50%;">
                <p style="text-align: left; font-size: 9pt;">
                  <b>
                    @if($requisition['provider']['country'] == 'ES')
                      Soporte de alero: <span style="font-weight: normal;">
                        @if($item['complement']['alero'])
                          {{$item['complement']['alero']}}
                          @else
                            No aplica.
                        @endif
                      </span>
                    @else  
                      <span style="text-transform: capitalize;">
                        Eave Support: <span style="font-weight: normal;">
                          @if($item['complement']['alero'])
                            {{$item['complement']['alero']}}
                            @else
                              Don´t Apply.
                          @endif
                        </span>
                      </span>
                    @endif
                  </b>
                </p>
              </td>
            </tr>
          </tbody>
        </table>
        @if($item['type'] == 'Fondo conico')
          <table style="width: 450px; margin: 13px auto;">
            <tbody>
              <tr>
                <td style="width: 50%;">
                  <p style="text-align: left; font-size: 9pt;">
                    <b>
                      @if($requisition['provider']['country'] == 'ES')
                        Tolva: <span style="font-weight: normal;">
                          {{$item['complement']['tolva']}}º
                        </span>
                      @else  
                        <span style="text-transform: capitalize;">
                          Hopper: <span style="font-weight: normal;">
                            {{$item['complement']['tolva']}}º
                          </span>
                        </span>
                      @endif
                    </b>
                  </p>
                </td>
                <td style="text-align: left; width: 50%;">
                  <p style="text-align: left; font-size: 9pt;">
                    <b>
                      @if($requisition['provider']['country'] == 'ES')
                        Uso: <span style="font-weight: normal;">
                          {{$item['complement']['use']}}
                        </span>
                      @else  
                        <span style="text-transform: capitalize;">
                          Use: <span style="font-weight: normal;">
                            {{$item['complement']['use']}}
                          </span>
                        </span>
                      @endif
                    </b>
                  </p>
                </td>
              </tr>
            </tbody>
          </table>
          <table style="width: 450px; margin: -24px auto;">
            <tbody>
              <tr>
                <td style="width: 50%;">
                  <p style="text-align: left; font-size: 9pt;">
                    <b>
                      @if($requisition['provider']['country'] == 'ES')
                        Puerta Manual: <span style="font-weight: normal;">
                          {{$item['complement']['manual_door']}}
                        </span>
                      @else  
                        <span style="text-transform: capitalize;">
                          Manual Door: <span style="font-weight: normal;">
                            {{$item['complement']['manual_door']}}
                          </span>
                        </span>
                      @endif
                    </b>
                  </p>
                </td>
                <td style="text-align: left; width: 50%;">
                  <p style="text-align: left; font-size: 9pt;">
                    <b>
                      @if($requisition['provider']['country'] == 'ES')
                        Puerta electrica: <span style="font-weight: normal;">
                          {{$item['complement']['electric_door']}}
                        </span>
                      @else  
                        <span style="text-transform: capitalize;">
                          Electric Door: <span style="font-weight: normal;">
                            {{$item['complement']['electric_door']}}
                          </span>
                        </span>
                      @endif
                    </b>
                  </p>
                </td>
              </tr>
            </tbody>
          </table>
          <table style="width: 450px; margin: 13px auto;">
            <tbody>
              <tr>
                <td style="width: 50%;">
                  <p style="text-align: left; font-size: 9pt;">
                    <b>
                      @if($requisition['provider']['country'] == 'ES')
                        Aereación: <span style="font-weight: normal;">
                          {{$item['complement']['aeration']}}
                        </span>
                      @else  
                        <span style="text-transform: capitalize;">
                          Aeration: <span style="font-weight: normal;">
                            {{$item['complement']['aeration']}}
                          </span>
                        </span>
                      @endif
                    </b>
                  </p>
                </td>
              </tr>
            </tbody>
          </table>
        @endif
      @endif
      @if(isset($item['accesories']) and count($item['accesories']) > 0)
        <table style="width: 450px; margin: 20px auto;">
          <tr style="height: 30px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-2 font-100 bg-primary">
            <td>
              <p style="color: white; font-size: 10pt;">
                <b>
                  @if($requisition['provider']['country'] == 'ES')
                    Accesorios
                    @else  
                      Accesories
                    @endif
                </b>
              </p>
            </td>
          </tr>
        </table>
        @php
          $showOtherFiled = false;
        @endphp
        <table style="width: 450px; margin: -10px auto;">
          <tbody>
          @foreach(array_chunk(json_decode($item['accesories'], true), 3) as $accesories)
              <tr>
                @foreach($accesories as $accesory)
                  <td style="width: 50%;">
                    <p style="text-align: left; font-size: 9pt;">
                      <b>
                        -
                        <span style="font-weight: normal;">
                           {{$accesory['name']}}
                        </span>
                      </b>
                    </p>
                  </td>
                  @if($accesory['name'] == 'Otros')
                    @php
                      $showOtherFiled = true;
                    @endphp
                  @endif
                @endforeach
              </tr>
            @endforeach
            @if($showOtherFiled == 1)
            <tr>
              <td>
                <p style="font-size: 9pt;">
                  <b>
                    - <span style="font-weight: normal">{{$item['comment']}}</span>
                  </b>
                </p>
              </td>
            </tr>
            @endif
          </tbody>
        </table>
      @endif

      <div class="footer" style="position: absolute; bottom: 0px;">
        <div class="row" style="">
          <div class="col-xs-11" style="margin-bottom: 30px; font-size: 9pt;">
            <p style="text-align: right;">
              <b style="">
                SILO: {{$item['model']}}
              </b>
            </p>
          </div>
          <div class="col-xs-11 text-center font-100">
            AGRIQUOTE VER. 1.0
          </div>
        </div>
      </div>
      @if(($key + 1) < count($requisition['silos']))
      <div class="page_break"></div>
      @endif
    @endforeach
  </body>
</html>