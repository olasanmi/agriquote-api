<!DOCTYPE html>
  <html lang="es">
      <head>
          <meta charset="UTF-8">
          <title>Quote {{$quote['code']}}</title>
          <style>
              /*!
   * Bootstrap v3.3.6 (http://getbootstrap.com)
   * Copyright 2011-2015 Twitter, Inc.
   * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
   */
  /*! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css */
  html {
    font-family: sans-serif;
    -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
  }
  body {
    margin: 0;
  }

  table {
    border-spacing: 0;
    border-collapse: collapse;
  }
  td,
  th {
    padding: 0;
  }
  /*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
  @media print {
    *,
    *:before,
    *:after {
      color: #000 !important;
      text-shadow: none !important;
      background: transparent !important;
      -webkit-box-shadow: none !important;
              box-shadow: none !important;
    }
    a,
    a:visited {
      text-decoration: underline;
    }
    a[href]:after {
      content: " (" attr(href) ")";
    }
    abbr[title]:after {
      content: " (" attr(title) ")";
    }
    a[href^="#"]:after,
    a[href^="javascript:"]:after {
      content: "";
    }
    pre,
    blockquote {
      border: 1px solid #999;

      page-break-inside: avoid;
    }
    thead {
      display: table-header-group;
    }
    tr,
    img {
      page-break-inside: avoid;
    }
    img {
      max-width: 100% !important;
    }
    p,
    h2,
    h3 {
      orphans: 3;
      widows: 3;
    }
    h2,
    h3 {
      page-break-after: avoid;
    }
    .navbar {
      display: none;
    }
    .btn > .caret,
    .dropup > .btn > .caret {
      border-top-color: #000 !important;
    }
    .label {
      border: 1px solid #000;
    }
    .table {
      border-collapse: collapse !important;
    }
    .table td,
    .table th {
      background-color: #fff !important;
    }
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #ddd !important;
    }
  }

  * {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  *:before,
  *:after {
    -webkit-box-sizing: border-box;
       -moz-box-sizing: border-box;
            box-sizing: border-box;
  }
  html {
    font-size: 10px;

    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }
  body {
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 14px;
    color: #333;
    background-color: #fff;
  }
  hr {
    margin-top: 20px;
    margin-bottom: 20px;
    border: 0;
    border-top: 1px solid #eee;
  }
  p {
    margin: 0 0 10px;
  }
  .lead {
    margin-bottom: 20px;
    font-size: 16px;
    font-weight: 300;
    line-height: 1.4;
  }
  @media (min-width: 768px) {
    .lead {
      font-size: 21px;
    }
  }
  small,
  .small {
    font-size: 85%;
  }
  .text-left {
    text-align: left;
  }
  .text-right {
    text-align: right;
  }
  .text-center {
    text-align: center;
  }
  .text-justify {
    text-align: justify;
  }
  .text-nowrap {
    white-space: nowrap;
  }
  .text-lowercase {
    text-transform: lowercase;
  }
  .text-uppercase {
    text-transform: uppercase;
  }
  .text-capitalize {
    text-transform: capitalize;
  }
  .text-muted {
    color: #777;
  }
  .text-primary {
    color: #337ab7;
  }
  a.text-primary:hover,
  a.text-primary:focus {
    color: #286090;
  }
  .text-success {
    color: #3c763d;
  }
  a.text-success:hover,
  a.text-success:focus {
    color: #2b542c;
  }
  .text-info {
    color: #31708f;
  }
  a.text-info:hover,
  a.text-info:focus {
    color: #245269;
  }
  .text-warning {
    color: #8a6d3b;
  }
  a.text-warning:hover,
  a.text-warning:focus {
    color: #66512c;
  }
  .text-danger {
    color: #a94442;
  }
  a.text-danger:hover,
  a.text-danger:focus {
    color: #843534;
  }
  .bg-primary {
    color: #fff;
    background-color: #337ab7;
  }
  a.bg-primary:hover,
  a.bg-primary:focus {
    background-color: #286090;
  }
  .bg-success {
    background-color: #dff0d8;
  }
  a.bg-success:hover,
  a.bg-success:focus {
    background-color: #c1e2b3;
  }
  .bg-info {
    background-color: #d9edf7;
  }
  a.bg-info:hover,
  a.bg-info:focus {
    background-color: #afd9ee;
  }
  .bg-warning {
    background-color: #fcf8e3;
  }
  a.bg-warning:hover,
  a.bg-warning:focus {
    background-color: #f7ecb5;
  }
  .bg-danger {
    background-color: #f2dede;
  }
  a.bg-danger:hover,
  a.bg-danger:focus {
    background-color: #e4b9b9;
  }
  .page-header {
    padding-bottom: 9px;
    margin: 40px 0 20px;
    border-bottom: 1px solid #eee;
  }
  ul,
  ol {
    margin-top: 0;
    margin-bottom: 10px;
  }
  ul ul,
  ol ul,
  ul ol,
  ol ol {
    margin-bottom: 0;
  }
  .list-unstyled {
    padding-left: 0;
    list-style: none;
  }
  .list-inline {
    padding-left: 0;
    margin-left: -5px;
    list-style: none;
  }
  .list-inline > li {
    display: inline-block;
    padding-right: 5px;
    padding-left: 5px;
  }
  dl {
    margin-top: 0;
    margin-bottom: 20px;
  }
  dt,
  dd {
    line-height: 1.42857143;
  }
  dt {
    font-weight: bold;
  }
  dd {
    margin-left: 0;
  }
  @media (min-width: 768px) {
    .dl-horizontal dt {
      float: left;
      width: 160px;
      overflow: hidden;
      clear: left;
      text-align: right;
      text-overflow: ellipsis;
      white-space: nowrap;
    }
    .dl-horizontal dd {
      margin-left: 180px;
    }
  }
  abbr[title],
  abbr[data-original-title] {
    cursor: help;
    border-bottom: 1px dotted #777;
  }
  .initialism {
    font-size: 90%;
    text-transform: uppercase;
  }
  blockquote {
    padding: 10px 20px;
    margin: 0 0 20px;
    font-size: 17.5px;
    border-left: 5px solid #eee;
  }
  blockquote p:last-child,
  blockquote ul:last-child,
  blockquote ol:last-child {
    margin-bottom: 0;
  }
  blockquote footer,
  blockquote small,
  blockquote .small {
    display: block;
    font-size: 80%;
    line-height: 1.42857143;
    color: #777;
  }
  blockquote footer:before,
  blockquote small:before,
  blockquote .small:before {
    content: '\2014 \00A0';
  }
  .blockquote-reverse,
  blockquote.pull-right {
    padding-right: 15px;
    padding-left: 0;
    text-align: right;
    border-right: 5px solid #eee;
    border-left: 0;
  }
  .blockquote-reverse footer:before,
  blockquote.pull-right footer:before,
  .blockquote-reverse small:before,
  blockquote.pull-right small:before,
  .blockquote-reverse .small:before,
  blockquote.pull-right .small:before {
    content: '';
  }
  .blockquote-reverse footer:after,
  blockquote.pull-right footer:after,
  .blockquote-reverse small:after,
  blockquote.pull-right small:after,
  .blockquote-reverse .small:after,
  blockquote.pull-right .small:after {
    content: '\00A0 \2014';
  }
  address {
    margin-bottom: 20px;
    font-style: normal;
    line-height: 1.42857143;
  }
  code,
  kbd,
  pre,
  samp {
    font-family: Menlo, Monaco, Consolas, "Courier New", monospace;
  }
  code {
    padding: 2px 4px;
    font-size: 90%;
    color: #c7254e;
    background-color: #f9f2f4;
    border-radius: 4px;
  }
  kbd {
    padding: 2px 4px;
    font-size: 90%;
    color: #fff;
    background-color: #333;
    border-radius: 3px;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
            box-shadow: inset 0 -1px 0 rgba(0, 0, 0, .25);
  }
  kbd kbd {
    padding: 0;
    font-size: 100%;
    font-weight: bold;
    -webkit-box-shadow: none;
            box-shadow: none;
  }
  pre {
    display: block;
    padding: 9.5px;
    margin: 0 0 10px;
    font-size: 13px;
    line-height: 1.42857143;
    color: #333;
    word-break: break-all;
    word-wrap: break-word;
    background-color: #f5f5f5;
    border: 1px solid #ccc;
    border-radius: 4px;
  }
  pre code {
    padding: 0;
    font-size: inherit;
    color: inherit;
    white-space: pre-wrap;
    background-color: transparent;
    border-radius: 0;
  }
  .pre-scrollable {
    max-height: 340px;
    overflow-y: scroll;
  }
  .container {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  @media (min-width: 768px) {
    .container {
      width: 750px;
    }
  }
  @media (min-width: 992px) {
    .container {
      width: 970px;
    }
  }
  @media (min-width: 1200px) {
    .container {
      width: 1170px;
    }
  }
  .container-fluid {
    padding-right: 15px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
  }
  .row {
    margin-right: -15px;
    margin-left: -15px;
  }
  .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
  }
  .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
    float: left;
  }
  .col-xs-12 {
    width: 100%;
  }
  .col-xs-11 {
    width: 91.66666667%;
  }
  .col-xs-10 {
    width: 83.33333333%;
  }
  .col-xs-9 {
    width: 75%;
  }
  .col-xs-8 {
    width: 66.66666667%;
  }
  .col-xs-7 {
    width: 58.33333333%;
  }
  .col-xs-6 {
    width: 50%;
  }
  .col-xs-5 {
    width: 41.66666667%;
  }
  .col-xs-4 {
    width: 33.33333333%;
  }
  .col-xs-3 {
    width: 25%;
  }
  .col-xs-2 {
    width: 16.66666667%;
  }
  .col-xs-1 {
    width: 8.33333333%;
  }
  .col-xs-pull-12 {
    right: 100%;
  }
  .col-xs-pull-11 {
    right: 91.66666667%;
  }
  .col-xs-pull-10 {
    right: 83.33333333%;
  }
  .col-xs-pull-9 {
    right: 75%;
  }
  .col-xs-pull-8 {
    right: 66.66666667%;
  }
  .col-xs-pull-7 {
    right: 58.33333333%;
  }
  .col-xs-pull-6 {
    right: 50%;
  }
  .col-xs-pull-5 {
    right: 41.66666667%;
  }
  .col-xs-pull-4 {
    right: 33.33333333%;
  }
  .col-xs-pull-3 {
    right: 25%;
  }
  .col-xs-pull-2 {
    right: 16.66666667%;
  }
  .col-xs-pull-1 {
    right: 8.33333333%;
  }
  .col-xs-pull-0 {
    right: auto;
  }
  .col-xs-push-12 {
    left: 100%;
  }
  .col-xs-push-11 {
    left: 91.66666667%;
  }
  .col-xs-push-10 {
    left: 83.33333333%;
  }
  .col-xs-push-9 {
    left: 75%;
  }
  .col-xs-push-8 {
    left: 66.66666667%;
  }
  .col-xs-push-7 {
    left: 58.33333333%;
  }
  .col-xs-push-6 {
    left: 50%;
  }
  .col-xs-push-5 {
    left: 41.66666667%;
  }
  .col-xs-push-4 {
    left: 33.33333333%;
  }
  .col-xs-push-3 {
    left: 25%;
  }
  .col-xs-push-2 {
    left: 16.66666667%;
  }
  .col-xs-push-1 {
    left: 8.33333333%;
  }
  .col-xs-push-0 {
    left: auto;
  }
  .col-xs-offset-12 {
    margin-left: 100%;
  }
  .col-xs-offset-11 {
    margin-left: 91.66666667%;
  }
  .col-xs-offset-10 {
    margin-left: 83.33333333%;
  }
  .col-xs-offset-9 {
    margin-left: 75%;
  }
  .col-xs-offset-8 {
    margin-left: 66.66666667%;
  }
  .col-xs-offset-7 {
    margin-left: 58.33333333%;
  }
  .col-xs-offset-6 {
    margin-left: 50%;
  }
  .col-xs-offset-5 {
    margin-left: 41.66666667%;
  }
  .col-xs-offset-4 {
    margin-left: 33.33333333%;
  }
  .col-xs-offset-3 {
    margin-left: 25%;
  }
  .col-xs-offset-2 {
    margin-left: 16.66666667%;
  }
  .col-xs-offset-1 {
    margin-left: 8.33333333%;
  }
  .col-xs-offset-0 {
    margin-left: 0;
  }
  @media (min-width: 768px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }
    .col-sm-12 {
      width: 100%;
    }
    .col-sm-11 {
      width: 91.66666667%;
    }
    .col-sm-10 {
      width: 83.33333333%;
    }
    .col-sm-9 {
      width: 75%;
    }
    .col-sm-8 {
      width: 66.66666667%;
    }
    .col-sm-7 {
      width: 58.33333333%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-5 {
      width: 41.66666667%;
    }
    .col-sm-4 {
      width: 33.33333333%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-2 {
      width: 16.66666667%;
    }
    .col-sm-1 {
      width: 8.33333333%;
    }
    .col-sm-pull-12 {
      right: 100%;
    }
    .col-sm-pull-11 {
      right: 91.66666667%;
    }
    .col-sm-pull-10 {
      right: 83.33333333%;
    }
    .col-sm-pull-9 {
      right: 75%;
    }
    .col-sm-pull-8 {
      right: 66.66666667%;
    }
    .col-sm-pull-7 {
      right: 58.33333333%;
    }
    .col-sm-pull-6 {
      right: 50%;
    }
    .col-sm-pull-5 {
      right: 41.66666667%;
    }
    .col-sm-pull-4 {
      right: 33.33333333%;
    }
    .col-sm-pull-3 {
      right: 25%;
    }
    .col-sm-pull-2 {
      right: 16.66666667%;
    }
    .col-sm-pull-1 {
      right: 8.33333333%;
    }
    .col-sm-pull-0 {
      right: auto;
    }
    .col-sm-push-12 {
      left: 100%;
    }
    .col-sm-push-11 {
      left: 91.66666667%;
    }
    .col-sm-push-10 {
      left: 83.33333333%;
    }
    .col-sm-push-9 {
      left: 75%;
    }
    .col-sm-push-8 {
      left: 66.66666667%;
    }
    .col-sm-push-7 {
      left: 58.33333333%;
    }
    .col-sm-push-6 {
      left: 50%;
    }
    .col-sm-push-5 {
      left: 41.66666667%;
    }
    .col-sm-push-4 {
      left: 33.33333333%;
    }
    .col-sm-push-3 {
      left: 25%;
    }
    .col-sm-push-2 {
      left: 16.66666667%;
    }
    .col-sm-push-1 {
      left: 8.33333333%;
    }
    .col-sm-push-0 {
      left: auto;
    }
    .col-sm-offset-12 {
      margin-left: 100%;
    }
    .col-sm-offset-11 {
      margin-left: 91.66666667%;
    }
    .col-sm-offset-10 {
      margin-left: 83.33333333%;
    }
    .col-sm-offset-9 {
      margin-left: 75%;
    }
    .col-sm-offset-8 {
      margin-left: 66.66666667%;
    }
    .col-sm-offset-7 {
      margin-left: 58.33333333%;
    }
    .col-sm-offset-6 {
      margin-left: 50%;
    }
    .col-sm-offset-5 {
      margin-left: 41.66666667%;
    }
    .col-sm-offset-4 {
      margin-left: 33.33333333%;
    }
    .col-sm-offset-3 {
      margin-left: 25%;
    }
    .col-sm-offset-2 {
      margin-left: 16.66666667%;
    }
    .col-sm-offset-1 {
      margin-left: 8.33333333%;
    }
    .col-sm-offset-0 {
      margin-left: 0;
    }
  }
  @media (min-width: 992px) {
    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }
    .col-md-12 {
      width: 100%;
    }
    .col-md-11 {
      width: 91.66666667%;
    }
    .col-md-10 {
      width: 83.33333333%;
    }
    .col-md-9 {
      width: 75%;
    }
    .col-md-8 {
      width: 66.66666667%;
    }
    .col-md-7 {
      width: 58.33333333%;
    }
    .col-md-6 {
      width: 50%;
    }
    .col-md-5 {
      width: 41.66666667%;
    }
    .col-md-4 {
      width: 33.33333333%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-2 {
      width: 16.66666667%;
    }
    .col-md-1 {
      width: 8.33333333%;
    }
    .col-md-pull-12 {
      right: 100%;
    }
    .col-md-pull-11 {
      right: 91.66666667%;
    }
    .col-md-pull-10 {
      right: 83.33333333%;
    }
    .col-md-pull-9 {
      right: 75%;
    }
    .col-md-pull-8 {
      right: 66.66666667%;
    }
    .col-md-pull-7 {
      right: 58.33333333%;
    }
    .col-md-pull-6 {
      right: 50%;
    }
    .col-md-pull-5 {
      right: 41.66666667%;
    }
    .col-md-pull-4 {
      right: 33.33333333%;
    }
    .col-md-pull-3 {
      right: 25%;
    }
    .col-md-pull-2 {
      right: 16.66666667%;
    }
    .col-md-pull-1 {
      right: 8.33333333%;
    }
    .col-md-pull-0 {
      right: auto;
    }
    .col-md-push-12 {
      left: 100%;
    }
    .col-md-push-11 {
      left: 91.66666667%;
    }
    .col-md-push-10 {
      left: 83.33333333%;
    }
    .col-md-push-9 {
      left: 75%;
    }
    .col-md-push-8 {
      left: 66.66666667%;
    }
    .col-md-push-7 {
      left: 58.33333333%;
    }
    .col-md-push-6 {
      left: 50%;
    }
    .col-md-push-5 {
      left: 41.66666667%;
    }
    .col-md-push-4 {
      left: 33.33333333%;
    }
    .col-md-push-3 {
      left: 25%;
    }
    .col-md-push-2 {
      left: 16.66666667%;
    }
    .col-md-push-1 {
      left: 8.33333333%;
    }
    .col-md-push-0 {
      left: auto;
    }
    .col-md-offset-12 {
      margin-left: 100%;
    }
    .col-md-offset-11 {
      margin-left: 91.66666667%;
    }
    .col-md-offset-10 {
      margin-left: 83.33333333%;
    }
    .col-md-offset-9 {
      margin-left: 75%;
    }
    .col-md-offset-8 {
      margin-left: 66.66666667%;
    }
    .col-md-offset-7 {
      margin-left: 58.33333333%;
    }
    .col-md-offset-6 {
      margin-left: 50%;
    }
    .col-md-offset-5 {
      margin-left: 41.66666667%;
    }
    .col-md-offset-4 {
      margin-left: 33.33333333%;
    }
    .col-md-offset-3 {
      margin-left: 25%;
    }
    .col-md-offset-2 {
      margin-left: 16.66666667%;
    }
    .col-md-offset-1 {
      margin-left: 8.33333333%;
    }
    .col-md-offset-0 {
      margin-left: 0;
    }
  }
  @media (min-width: 1200px) {
    .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
      float: left;
    }
    .col-lg-12 {
      width: 100%;
    }
    .col-lg-11 {
      width: 91.66666667%;
    }
    .col-lg-10 {
      width: 83.33333333%;
    }
    .col-lg-9 {
      width: 75%;
    }
    .col-lg-8 {
      width: 66.66666667%;
    }
    .col-lg-7 {
      width: 58.33333333%;
    }
    .col-lg-6 {
      width: 50%;
    }
    .col-lg-5 {
      width: 41.66666667%;
    }
    .col-lg-4 {
      width: 33.33333333%;
    }
    .col-lg-3 {
      width: 25%;
    }
    .col-lg-2 {
      width: 16.66666667%;
    }
    .col-lg-1 {
      width: 8.33333333%;
    }
    .col-lg-pull-12 {
      right: 100%;
    }
    .col-lg-pull-11 {
      right: 91.66666667%;
    }
    .col-lg-pull-10 {
      right: 83.33333333%;
    }
    .col-lg-pull-9 {
      right: 75%;
    }
    .col-lg-pull-8 {
      right: 66.66666667%;
    }
    .col-lg-pull-7 {
      right: 58.33333333%;
    }
    .col-lg-pull-6 {
      right: 50%;
    }
    .col-lg-pull-5 {
      right: 41.66666667%;
    }
    .col-lg-pull-4 {
      right: 33.33333333%;
    }
    .col-lg-pull-3 {
      right: 25%;
    }
    .col-lg-pull-2 {
      right: 16.66666667%;
    }
    .col-lg-pull-1 {
      right: 8.33333333%;
    }
    .col-lg-pull-0 {
      right: auto;
    }
    .col-lg-push-12 {
      left: 100%;
    }
    .col-lg-push-11 {
      left: 91.66666667%;
    }
    .col-lg-push-10 {
      left: 83.33333333%;
    }
    .col-lg-push-9 {
      left: 75%;
    }
    .col-lg-push-8 {
      left: 66.66666667%;
    }
    .col-lg-push-7 {
      left: 58.33333333%;
    }
    .col-lg-push-6 {
      left: 50%;
    }
    .col-lg-push-5 {
      left: 41.66666667%;
    }
    .col-lg-push-4 {
      left: 33.33333333%;
    }
    .col-lg-push-3 {
      left: 25%;
    }
    .col-lg-push-2 {
      left: 16.66666667%;
    }
    .col-lg-push-1 {
      left: 8.33333333%;
    }
    .col-lg-push-0 {
      left: auto;
    }
    .col-lg-offset-12 {
      margin-left: 100%;
    }
    .col-lg-offset-11 {
      margin-left: 91.66666667%;
    }
    .col-lg-offset-10 {
      margin-left: 83.33333333%;
    }
    .col-lg-offset-9 {
      margin-left: 75%;
    }
    .col-lg-offset-8 {
      margin-left: 66.66666667%;
    }
    .col-lg-offset-7 {
      margin-left: 58.33333333%;
    }
    .col-lg-offset-6 {
      margin-left: 50%;
    }
    .col-lg-offset-5 {
      margin-left: 41.66666667%;
    }
    .col-lg-offset-4 {
      margin-left: 33.33333333%;
    }
    .col-lg-offset-3 {
      margin-left: 25%;
    }
    .col-lg-offset-2 {
      margin-left: 16.66666667%;
    }
    .col-lg-offset-1 {
      margin-left: 8.33333333%;
    }
    .col-lg-offset-0 {
      margin-left: 0;
    }
  }
  table {
    background-color: transparent;
  }
  caption {
    padding-top: 8px;
    padding-bottom: 8px;
    color: #777;
    text-align: left;
  }
  th {
    text-align: left;
  }
  .table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
  }
  .table > thead > tr > th,
  .table > tbody > tr > th,
  .table > tfoot > tr > th,
  .table > thead > tr > td,
  .table > tbody > tr > td,
  .table > tfoot > tr > td {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #ddd;
  }
  .table > thead > tr > th {
    vertical-align: bottom;
    border-bottom: 2px solid #ddd;
  }
  .table > caption + thead > tr:first-child > th,
  .table > colgroup + thead > tr:first-child > th,
  .table > thead:first-child > tr:first-child > th,
  .table > caption + thead > tr:first-child > td,
  .table > colgroup + thead > tr:first-child > td,
  .table > thead:first-child > tr:first-child > td {
    border-top: 0;
  }
  .table > tbody + tbody {
    border-top: 2px solid #ddd;
  }
  .table .table {
    background-color: #fff;
  }
  .table-condensed > thead > tr > th,
  .table-condensed > tbody > tr > th,
  .table-condensed > tfoot > tr > th,
  .table-condensed > thead > tr > td,
  .table-condensed > tbody > tr > td,
  .table-condensed > tfoot > tr > td {
    padding: 5px;
  }
  .table-bordered {
    border: 1px solid #ddd;
  }
  .table-bordered > thead > tr > th,
  .table-bordered > tbody > tr > th,
  .table-bordered > tfoot > tr > th,
  .table-bordered > thead > tr > td,
  .table-bordered > tbody > tr > td,
  .table-bordered > tfoot > tr > td {
    border: 1px solid #ddd;
  }
  .table-bordered > thead > tr > th,
  .table-bordered > thead > tr > td {
    border-bottom-width: 2px;
  }
  .table-striped > tbody > tr:nth-of-type(odd) {
    background-color: #f9f9f9;
  }
  .table-hover > tbody > tr:hover {
    background-color: #f5f5f5;
  }
  table col[class*="col-"] {
    position: static;
    display: table-column;
    float: none;
  }
  table td[class*="col-"],
  table th[class*="col-"] {
    position: static;
    display: table-cell;
    float: none;
  }
  .table > thead > tr > td.active,
  .table > tbody > tr > td.active,
  .table > tfoot > tr > td.active,
  .table > thead > tr > th.active,
  .table > tbody > tr > th.active,
  .table > tfoot > tr > th.active,
  .table > thead > tr.active > td,
  .table > tbody > tr.active > td,
  .table > tfoot > tr.active > td,
  .table > thead > tr.active > th,
  .table > tbody > tr.active > th,
  .table > tfoot > tr.active > th {
    background-color: #f5f5f5;
  }
  .table-hover > tbody > tr > td.active:hover,
  .table-hover > tbody > tr > th.active:hover,
  .table-hover > tbody > tr.active:hover > td,
  .table-hover > tbody > tr:hover > .active,
  .table-hover > tbody > tr.active:hover > th {
    background-color: #e8e8e8;
  }
  .table > thead > tr > td.success,
  .table > tbody > tr > td.success,
  .table > tfoot > tr > td.success,
  .table > thead > tr > th.success,
  .table > tbody > tr > th.success,
  .table > tfoot > tr > th.success,
  .table > thead > tr.success > td,
  .table > tbody > tr.success > td,
  .table > tfoot > tr.success > td,
  .table > thead > tr.success > th,
  .table > tbody > tr.success > th,
  .table > tfoot > tr.success > th {
    background-color: #dff0d8;
  }
  .table-hover > tbody > tr > td.success:hover,
  .table-hover > tbody > tr > th.success:hover,
  .table-hover > tbody > tr.success:hover > td,
  .table-hover > tbody > tr:hover > .success,
  .table-hover > tbody > tr.success:hover > th {
    background-color: #d0e9c6;
  }
  .table > thead > tr > td.info,
  .table > tbody > tr > td.info,
  .table > tfoot > tr > td.info,
  .table > thead > tr > th.info,
  .table > tbody > tr > th.info,
  .table > tfoot > tr > th.info,
  .table > thead > tr.info > td,
  .table > tbody > tr.info > td,
  .table > tfoot > tr.info > td,
  .table > thead > tr.info > th,
  .table > tbody > tr.info > th,
  .table > tfoot > tr.info > th {
    background-color: #d9edf7;
  }
  .table-hover > tbody > tr > td.info:hover,
  .table-hover > tbody > tr > th.info:hover,
  .table-hover > tbody > tr.info:hover > td,
  .table-hover > tbody > tr:hover > .info,
  .table-hover > tbody > tr.info:hover > th {
    background-color: #c4e3f3;
  }
  .table > thead > tr > td.warning,
  .table > tbody > tr > td.warning,
  .table > tfoot > tr > td.warning,
  .table > thead > tr > th.warning,
  .table > tbody > tr > th.warning,
  .table > tfoot > tr > th.warning,
  .table > thead > tr.warning > td,
  .table > tbody > tr.warning > td,
  .table > tfoot > tr.warning > td,
  .table > thead > tr.warning > th,
  .table > tbody > tr.warning > th,
  .table > tfoot > tr.warning > th {
    background-color: #fcf8e3;
  }
  .table-hover > tbody > tr > td.warning:hover,
  .table-hover > tbody > tr > th.warning:hover,
  .table-hover > tbody > tr.warning:hover > td,
  .table-hover > tbody > tr:hover > .warning,
  .table-hover > tbody > tr.warning:hover > th {
    background-color: #faf2cc;
  }
  .table > thead > tr > td.danger,
  .table > tbody > tr > td.danger,
  .table > tfoot > tr > td.danger,
  .table > thead > tr > th.danger,
  .table > tbody > tr > th.danger,
  .table > tfoot > tr > th.danger,
  .table > thead > tr.danger > td,
  .table > tbody > tr.danger > td,
  .table > tfoot > tr.danger > td,
  .table > thead > tr.danger > th,
  .table > tbody > tr.danger > th,
  .table > tfoot > tr.danger > th {
    background-color: #f2dede;
  }
  .table-hover > tbody > tr > td.danger:hover,
  .table-hover > tbody > tr > th.danger:hover,
  .table-hover > tbody > tr.danger:hover > td,
  .table-hover > tbody > tr:hover > .danger,
  .table-hover > tbody > tr.danger:hover > th {
    background-color: #ebcccc;
  }
  .table-responsive {
    min-height: .01%;
    overflow-x: auto;
  }
  @media screen and (max-width: 767px) {
    .table-responsive {
      width: 100%;
      margin-bottom: 15px;
      overflow-y: hidden;
      -ms-overflow-style: -ms-autohiding-scrollbar;
      border: 1px solid #ddd;
    }
    .table-responsive > .table {
      margin-bottom: 0;
    }
    .table-responsive > .table > thead > tr > th,
    .table-responsive > .table > tbody > tr > th,
    .table-responsive > .table > tfoot > tr > th,
    .table-responsive > .table > thead > tr > td,
    .table-responsive > .table > tbody > tr > td,
    .table-responsive > .table > tfoot > tr > td {
      white-space: nowrap;
    }
    .table-responsive > .table-bordered {
      border: 0;
    }
    .table-responsive > .table-bordered > thead > tr > th:first-child,
    .table-responsive > .table-bordered > tbody > tr > th:first-child,
    .table-responsive > .table-bordered > tfoot > tr > th:first-child,
    .table-responsive > .table-bordered > thead > tr > td:first-child,
    .table-responsive > .table-bordered > tbody > tr > td:first-child,
    .table-responsive > .table-bordered > tfoot > tr > td:first-child {
      border-left: 0;
    }
    .table-responsive > .table-bordered > thead > tr > th:last-child,
    .table-responsive > .table-bordered > tbody > tr > th:last-child,
    .table-responsive > .table-bordered > tfoot > tr > th:last-child,
    .table-responsive > .table-bordered > thead > tr > td:last-child,
    .table-responsive > .table-bordered > tbody > tr > td:last-child,
    .table-responsive > .table-bordered > tfoot > tr > td:last-child {
      border-right: 0;
    }
    .table-responsive > .table-bordered > tbody > tr:last-child > th,
    .table-responsive > .table-bordered > tfoot > tr:last-child > th,
    .table-responsive > .table-bordered > tbody > tr:last-child > td,
    .table-responsive > .table-bordered > tfoot > tr:last-child > td {
      border-bottom: 0;
    }
  }
  fieldset {
    min-width: 0;
    padding: 0;
    margin: 0;
    border: 0;
  }
  legend {
    display: block;
    width: 100%;
    padding: 0;
    margin-bottom: 20px;
    font-size: 21px;
    line-height: inherit;
    color: #333;
    border: 0;
    border-bottom: 1px solid #e5e5e5;
  }
  label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: bold;
  }
  .collapse {
    display: none;
  }
  .collapse.in {
    display: block;
  }
  tr.collapse.in {
    display: table-row;
  }
  tbody.collapse.in {
    display: table-row-group;
  }
  .carousel {
    position: relative;
  }
  .carousel-inner {
    position: relative;
    width: 100%;
    overflow: hidden;
  }
  .carousel-inner > .item {
    position: relative;
    display: none;
    -webkit-transition: .6s ease-in-out left;
         -o-transition: .6s ease-in-out left;
            transition: .6s ease-in-out left;
  }
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    line-height: 1;
  }
  @media all and (transform-3d), (-webkit-transform-3d) {
    .carousel-inner > .item {
      -webkit-transition: -webkit-transform .6s ease-in-out;
           -o-transition:      -o-transform .6s ease-in-out;
              transition:         transform .6s ease-in-out;

      -webkit-backface-visibility: hidden;
              backface-visibility: hidden;
      -webkit-perspective: 1000px;
              perspective: 1000px;
    }
    .carousel-inner > .item.next,
    .carousel-inner > .item.active.right {
      left: 0;
      -webkit-transform: translate3d(100%, 0, 0);
              transform: translate3d(100%, 0, 0);
    }
    .carousel-inner > .item.prev,
    .carousel-inner > .item.active.left {
      left: 0;
      -webkit-transform: translate3d(-100%, 0, 0);
              transform: translate3d(-100%, 0, 0);
    }
    .carousel-inner > .item.next.left,
    .carousel-inner > .item.prev.right,
    .carousel-inner > .item.active {
      left: 0;
      -webkit-transform: translate3d(0, 0, 0);
              transform: translate3d(0, 0, 0);
    }
  }
  .carousel-inner > .active,
  .carousel-inner > .next,
  .carousel-inner > .prev {
    display: block;
  }
  .carousel-inner > .active {
    left: 0;
  }
  .carousel-inner > .next,
  .carousel-inner > .prev {
    position: absolute;
    top: 0;
    width: 100%;
  }
  .carousel-inner > .next {
    left: 100%;
  }
  .carousel-inner > .prev {
    left: -100%;
  }
  .carousel-inner > .next.left,
  .carousel-inner > .prev.right {
    left: 0;
  }
  .carousel-inner > .active.left {
    left: -100%;
  }
  .carousel-inner > .active.right {
    left: 100%;
  }
  .carousel-control {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 15%;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
    background-color: rgba(0, 0, 0, 0);
    filter: alpha(opacity=50);
    opacity: .5;
  }
  .carousel-control.left {
    background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .5)), to(rgba(0, 0, 0, .0001)));
    background-image:         linear-gradient(to right, rgba(0, 0, 0, .5) 0%, rgba(0, 0, 0, .0001) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
    background-repeat: repeat-x;
  }
  .carousel-control.right {
    right: 0;
    left: auto;
    background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    background-image:      -o-linear-gradient(left, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    background-image: -webkit-gradient(linear, left top, right top, from(rgba(0, 0, 0, .0001)), to(rgba(0, 0, 0, .5)));
    background-image:         linear-gradient(to right, rgba(0, 0, 0, .0001) 0%, rgba(0, 0, 0, .5) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);
    background-repeat: repeat-x;
  }
  .carousel-control:hover,
  .carousel-control:focus {
    color: #fff;
    text-decoration: none;
    filter: alpha(opacity=90);
    outline: 0;
    opacity: .9;
  }
  .carousel-control .icon-prev,
  .carousel-control .icon-next,
  .carousel-control .glyphicon-chevron-left,
  .carousel-control .glyphicon-chevron-right {
    position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
    margin-top: -10px;
  }
  .carousel-control .icon-prev,
  .carousel-control .glyphicon-chevron-left {
    left: 50%;
    margin-left: -10px;
  }
  .carousel-control .icon-next,
  .carousel-control .glyphicon-chevron-right {
    right: 50%;
    margin-right: -10px;
  }
  .carousel-control .icon-prev,
  .carousel-control .icon-next {
    width: 20px;
    height: 20px;
    font-family: serif;
    line-height: 1;
  }
  .carousel-control .icon-prev:before {
    content: '\2039';
  }
  .carousel-control .icon-next:before {
    content: '\203a';
  }
  .carousel-indicators {
    position: absolute;
    bottom: 10px;
    left: 50%;
    z-index: 15;
    width: 60%;
    padding-left: 0;
    margin-left: -30%;
    text-align: center;
    list-style: none;
  }
  .carousel-indicators li {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 1px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000 \9;
    background-color: rgba(0, 0, 0, 0);
    border: 1px solid #fff;
    border-radius: 10px;
  }
  .carousel-indicators .active {
    width: 12px;
    height: 12px;
    margin: 0;
    background-color: #fff;
  }
  .carousel-caption {
    position: absolute;
    right: 15%;
    bottom: 20px;
    left: 15%;
    z-index: 10;
    padding-top: 20px;
    padding-bottom: 20px;
    color: #fff;
    text-align: center;
    text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
  }
  .carousel-caption .btn {
    text-shadow: none;
  }
  @media screen and (min-width: 768px) {
    .carousel-control .glyphicon-chevron-left,
    .carousel-control .glyphicon-chevron-right,
    .carousel-control .icon-prev,
    .carousel-control .icon-next {
      width: 30px;
      height: 30px;
      margin-top: -10px;
      font-size: 30px;
    }
    .carousel-control .glyphicon-chevron-left,
    .carousel-control .icon-prev {
      margin-left: -10px;
    }
    .carousel-control .glyphicon-chevron-right,
    .carousel-control .icon-next {
      margin-right: -10px;
    }
    .carousel-caption {
      right: 20%;
      left: 20%;
      padding-bottom: 30px;
    }
    .carousel-indicators {
      bottom: 20px;
    }
  }
  .clearfix:before,
  .clearfix:after,
  .dl-horizontal dd:before,
  .dl-horizontal dd:after,
  .container:before,
  .container:after,
  .container-fluid:before,
  .container-fluid:after,
  .row:before,
  .row:after,
  .form-horizontal .form-group:before,
  .form-horizontal .form-group:after,
  .btn-toolbar:before,
  .btn-toolbar:after,
  .btn-group-vertical > .btn-group:before,
  .btn-group-vertical > .btn-group:after,
  .nav:before,
  .nav:after,
  .navbar:before,
  .navbar:after,
  .navbar-header:before,
  .navbar-header:after,
  .navbar-collapse:before,
  .navbar-collapse:after,
  .pager:before,
  .pager:after,
  .panel-body:before,
  .panel-body:after,
  .modal-header:before,
  .modal-header:after,
  .modal-footer:before,
  .modal-footer:after {
    display: table;
    content: " ";
  }
  .clearfix:after,
  .dl-horizontal dd:after,
  .container:after,
  .container-fluid:after,
  .row:after,
  .form-horizontal .form-group:after,
  .btn-toolbar:after,
  .btn-group-vertical > .btn-group:after,
  .nav:after,
  .navbar:after,
  .navbar-header:after,
  .navbar-collapse:after,
  .pager:after,
  .panel-body:after,
  .modal-header:after,
  .modal-footer:after {
    clear: both;
  }
  .center-block {
    display: block;
    margin-right: auto;
    margin-left: auto;
  }
  .pull-right {
    float: right !important;
  }
  .pull-left {
    float: left !important;
  }
  .hide {
    display: none !important;
  }
  .show {
    display: block !important;
  }
  .invisible {
    visibility: hidden;
  }
  .text-hide {
    font: 0/0 a;
    color: transparent;
    text-shadow: none;
    background-color: transparent;
    border: 0;
  }
  .hidden {
    display: none !important;
  }
  .affix {
    position: fixed;
  }
  @-ms-viewport {
    width: device-width;
  }
  .visible-xs,
  .visible-sm,
  .visible-md,
  .visible-lg {
    display: none !important;
  }
  .visible-xs-block,
  .visible-xs-inline,
  .visible-xs-inline-block,
  .visible-sm-block,
  .visible-sm-inline,
  .visible-sm-inline-block,
  .visible-md-block,
  .visible-md-inline,
  .visible-md-inline-block,
  .visible-lg-block,
  .visible-lg-inline,
  .visible-lg-inline-block {
    display: none !important;
  }
  @media (max-width: 767px) {
    .visible-xs {
      display: block !important;
    }
    table.visible-xs {
      display: table !important;
    }
    tr.visible-xs {
      display: table-row !important;
    }
    th.visible-xs,
    td.visible-xs {
      display: table-cell !important;
    }
  }
  @media (max-width: 767px) {
    .visible-xs-block {
      display: block !important;
    }
  }
  @media (max-width: 767px) {
    .visible-xs-inline {
      display: inline !important;
    }
  }
  @media (max-width: 767px) {
    .visible-xs-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm {
      display: block !important;
    }
    table.visible-sm {
      display: table !important;
    }
    tr.visible-sm {
      display: table-row !important;
    }
    th.visible-sm,
    td.visible-sm {
      display: table-cell !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm-block {
      display: block !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm-inline {
      display: inline !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .visible-sm-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md {
      display: block !important;
    }
    table.visible-md {
      display: table !important;
    }
    tr.visible-md {
      display: table-row !important;
    }
    th.visible-md,
    td.visible-md {
      display: table-cell !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md-block {
      display: block !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md-inline {
      display: inline !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .visible-md-inline-block {
      display: inline-block !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg {
      display: block !important;
    }
    table.visible-lg {
      display: table !important;
    }
    tr.visible-lg {
      display: table-row !important;
    }
    th.visible-lg,
    td.visible-lg {
      display: table-cell !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg-block {
      display: block !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg-inline {
      display: inline !important;
    }
  }
  @media (min-width: 1200px) {
    .visible-lg-inline-block {
      display: inline-block !important;
    }
  }
  @media (max-width: 767px) {
    .hidden-xs {
      display: none !important;
    }
  }
  @media (min-width: 768px) and (max-width: 991px) {
    .hidden-sm {
      display: none !important;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px) {
    .hidden-md {
      display: none !important;
    }
  }
  @media (min-width: 1200px) {
    .hidden-lg {
      display: none !important;
    }
  }
  .visible-print {
    display: none !important;
  }
  @media print {
    .visible-print {
      display: block !important;
    }
    table.visible-print {
      display: table !important;
    }
    tr.visible-print {
      display: table-row !important;
    }
    th.visible-print,
    td.visible-print {
      display: table-cell !important;
    }
  }
  .visible-print-block {
    display: none !important;
  }
  @media print {
    .visible-print-block {
      display: block !important;
    }
  }
  .visible-print-inline {
    display: none !important;
  }
  @media print {
    .visible-print-inline {
      display: inline !important;
    }
  }
  .visible-print-inline-block {
    display: none !important;
  }
  @media print {
    .visible-print-inline-block {
      display: inline-block !important;
    }
  }
  @media print {
    .hidden-print {
      display: none !important;
    }
  }

  body {
      min-width: 100px;
      min-height: 100%;
      font-family: "Roboto", "-apple-system", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
      -ms-text-size-adjust: 100%;
      -webkit-text-size-adjust: 100%;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      font-smoothing: antialiased;
      line-height: 1.5;
      font-size: 14px;
  }

  h4, h3, h5 {
    font-family: "Roboto", "-apple-system", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
  }

  .isPay {
    background: green;
    width: 140px;
    position: absolute;
    top: -40px;
    right: -70px;
    text-align: center;
    padding: 5px;
    color: white;
    -webkit-transform: rotate(30deg);
    -moz-transform: rotate(30deg);
    -o-transform: rotate(30deg);
    -ms-transform: rotate(30deg);
    transform: rotate(30deg);
  }
  .isNoPa
    width: 200px;
    position: absolute;
    top: -30px;
    right: -70px;
    text-align: center;
    padding: 5px;
    color: white;
    -webkit-transform: rotate(30deg);
    -moz-transform: rotate(30deg);
    -o-transform: rotate(30deg);
    -ms-transform: rotate(30deg);
    transform: rotate(30deg);
  }
  /*# sourceMappingURL=bootstrap.css.map */
    .font-100 {
      font-size: 8pt;
    }
    .font-144 {
      font-size: 8pt;
    }
    .font-166 {
      font-size: 16pt;
    }
    .font-188 {
      font-size: 18pt;
    }
    .font-200 {
      font-size: 20pt;
    }
    .font-100 {
      font-size: 8pt;
    }
    .font-88 {
      font-size: 8pt;
    }
    .font-888 {
      font-size: 8pt;
    }
    .center {
      text-align: center !important;
    }
    .bold {
      font-weight: 600 !important;
      color: black;
    }
    .bg-primary {
      background-color: #033a78 !important;
      padding: .7em;
    }
    .mt-md {
      margin-top: .5rem;
    }
    .mt-lg {
      margin-top: 1rem;
    }
    .mt-xl {
      margin-top: 2rem;
    }
    .bolder {
      font-weight: 800;
    }
    .bg-gray {
      background-color: #e3e9ec !important;
    }
    .center{
      text-align: center;
    }
    .footer {
       position: fixed; 
      bottom: 0cm; 
      left: 0cm; 
      right: 0cm;
    }
    .footer2 {
       position: fixed; 
      bottom: 0cm; 
      left: 0cm; 
      right: 0cm;
    }
    div > b {
      font-weight: bold !important;
    }
    .text-right {
      text-align: right !important;
    }
    @page { margin: .5cm 1.5cm; position: relative;}
    .page_break { page-break-before: always; }
  </style>
</head>
  <body>
    <main>
      @foreach($quote->newItems as $keNew => $newItem)
        <div style="margin-top:" class="row" style="height: 40px !important; padding-top: .1em; padding-bottom: .1em">
         <div style="height: 35px !important; padding-top: .2em; padding-bottom: 0em; font-size: 14pt;" class="col-xs-8 bg-primary" style="color: white !important; font-weight: 600">
           <b>
             @if($quote->lang == 'en')
              PROFORM INVOICE
             @endif
             @if($quote->lang == 'pt')
              FATURA PROFORMA
             @endif
             @if($quote->lang == 'es')
              FACTURA PROFORMA
             @endif
             @if($quote->lang == null)
              FACTURA PROFORMA
             @endif
           </b>
         </div>
         <div class="col-xs-3">
           <center>
             <img style="margin-right: -20px" class="mt-sm" src="{{url('images/assets/ats.png')}}" width="140px">
           </center>
         </div>
       </div>
       <div class="row" style="padding-top: .1em; padding-bottom: .1em; margin-top: -70px !important">
         <div class="col-xs-4" style="color: black; font-weight: 600; margin-left: -15px;">
           <div class="row">
             <div class="col-xs-12 font-100" style="font-weight: 600">
                <b>
                @if($quote->lang == 'en')
                  CUSTOMER
                @endif
                @if($quote->lang == 'pt')
                  CLIENTE
                @endif
                @if($quote->lang == 'es')
                  CLIENTE
                @endif
                @if($quote->lang == null)
                  CLIENTE
                @endif
                </b>
             </div><br>
             <div class="col-xs-12 font-100">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Company Name:
                @endif
                @if($quote->lang == 'pt')
                  Empresa:
                @endif
                @if($quote->lang == 'es')
                  Empresa:
                @endif
                @if($quote->lang == null)
                  Empresa:
                @endif 
                <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">{{$quote->Client->brand}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100" style="margin-top: 0px">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Contact Name:
                @endif
                @if($quote->lang == 'pt')
                  Nome contato:
                @endif
                @if($quote->lang == 'es')
                  Contacto:
                @endif
                @if($quote->lang == null)
                  Contacto:
                @endif 
                <span style="padding-top: .3em; padding-bottom: .5em; text-transform: capitalize; font-weight: normal">{{$quote->Client->name}} {{$quote->Client->last_name}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100" style="margin-top: 0px">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Country:
                @endif
                @if($quote->lang == 'pt')
                  País:
                @endif
                @if($quote->lang == 'es')
                  Pais:
                @endif
                @if($quote->lang == null)
                  Pais:
                @endif 
                <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$quote->Client->country_label}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100" style="margin-top: 0px">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Phone:
                @endif
                @if($quote->lang == 'pt')
                  Telefone:
                @endif
                @if($quote->lang == 'es')
                  Teléfono:
                @endif
                @if($quote->lang == null)
                  Teléfono:
                @endif 
                <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$quote->Client->cellphone}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100" style="margin-top: 0px">
               <b style="color: black; font-weight: normal !important">
                E-mail: <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$quote->Client->email}}</span>
               </b>
             </div>
           </div>
         </div>
         @if($quote->Dealer and $quote->Dealer->id)
          <div class="col-xs-5" style="color: black; font-weight: 600" style="margin-top: -20px">
           <div class="row">
             <div class="col-xs-12 font-100" style="font-weight: 600">
                <b>
                  @if($quote->lang == 'en')
                    DEALER
                  @endif
                  @if($quote->lang == 'pt')
                    DISTRIBUIDOR
                  @endif
                  @if($quote->lang == 'es')
                    DISTRIBUIDOR
                  @endif
                  @if($quote->lang == null)
                    DISTRIBUIDOR
                  @endif
                </b>
             </div><br>
             <div class="col-xs-12 font-100">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Company Name:
                @endif
                @if($quote->lang == 'pt')
                  Empresa:
                @endif
                @if($quote->lang == 'es')
                  Empresa:
                @endif
                @if($quote->lang == null)
                  Empresa:
                @endif
                <span style="padding-top: .3em; padding-bottom: .5em; height: 50px; text-transform: capitalize; font-weight: normal">{{$quote->Dealer->brand}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Contact Name:
                @endif
                @if($quote->lang == 'pt')
                  Nome de contato:
                @endif
                @if($quote->lang == 'es')
                  Contacto:
                @endif
                @if($quote->lang == null)
                  Contacto:
                @endif
                <span style="padding-top: .3em; padding-bottom: .5em; height: 50px; text-transform: capitalize; font-weight: normal">{{$quote->Dealer->name}}  {{$quote->Dealer->last_name}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Country:
                @endif
                @if($quote->lang == 'pt')
                  País:
                @endif
                @if($quote->lang == 'es')
                  Pais:
                @endif
                @if($quote->lang == null)
                  Pais:
                @endif
                <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$quote->Dealer->country_label}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100">
               <b style="color: black; font-weight: normal !important">
                @if($quote->lang == 'en')
                  Phone:
                @endif
                @if($quote->lang == 'pt')
                  Telefone:
                @endif
                @if($quote->lang == 'es')
                  Teléfono:
                @endif
                @if($quote->lang == null)
                  Teléfono:
                @endif 
                <span style="padding-bottom: .5em; font-weight: normal">{{$quote->Dealer->cellphone}}</span>
               </b>
             </div><br>
             <div class="col-xs-12 font-100">
               <b style="color: black; font-weight: normal !important">
                E-mail: <span style="padding-top: .3em; padding-bottom: .5em; font-weight: normal">{{$quote->Dealer->email}}</span>
               </b>
             </div>
           </div>
          </div>
         @endif
       </div>
       <br>
       <div class="row mt-md">
           <div class="col-xs-4" style="font-weight: 600; border-bottom: 1px solid; margin-right: 20px; padding-left: 5px">
             <b style="color: black;" class="font-100">
                @if($quote->lang == 'en')
                  PROFORM No:
                @endif
                @if($quote->lang == 'pt')
                  PROFORME #:
                @endif
                @if($quote->lang == 'es')
                  PROFORMA No:
                @endif
                @if($quote->lang == null)
                  PROFORMA No:
                @endif 
                <br><span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">{{$quote->code}}</span>
              </b>
           </div>
           <div class="col-xs-3" style="font-weight: 600; border-bottom: 1px solid; margin-right: 20px; padding-left: 5px">
             <b style="color: black;" class="font-100">
                @if($quote->lang == 'en')
                  DATE:
                @endif
                @if($quote->lang == 'pt')
                  DATA:
                @endif
                @if($quote->lang == 'es')
                  FECHA:
                @endif
                @if($quote->lang == null)
                  FECHA:
                @endif
                <br><span class="font-100" style="padding-top: .3em; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">{{$quote->date_creation}}</span>
              </b>
           </div>
           <div class="col-xs-3" style="font-weight: 600; border-bottom: 1px solid; margin-right: 20px; padding-left: 5px">
             <b style="color: black;" class="font-100">
                @if($quote->lang == 'en')
                  VALIDITY:
                @endif
                @if($quote->lang == 'pt')
                  VALIDADE:
                @endif
                @if($quote->lang == 'es')
                  VALIDEZ:
                @endif
                @if($quote->lang == null)
                  VALIDEZ:
                @endif
                <br> <span class="font-100" style="padding-top: .3em; font-size: 8pt !important; padding-bottom: .5em; height: 50px; color: red !important; font-weight: normal">{{$quote->date_validation}}</span>
              </b>
           </div>
       </div>
       <br>
       <div class="row">
         <div class="col-xs-12">
            @if($keNew == 1) 
              <p style="text-align: right; font-size: 8pt;">
                <b>@if($quote->lang == 'en')
                  Come:
                @endif
                @if($quote->lang == 'pt')
                  Vem:
                @endif
                @if($quote->lang == 'es')
                  Vienen:
                @endif
                @if($quote->lang == null)
                  Vienen:
                @endif ${{number_format($firstPageTotal, 2)}}</b>
              </p>
            @endif
         </div>
       </div>
       <div class="row mt-sm" style="margin-left: -30px">
         <div class="col-xs-12">
           <center>
             <table style="width: 100%; font-size: 8pt">
               <thead>
                 <tr class="bg-primary" style="color: white; padding: .4em" class="font-100">
                   <td style="padding: .3em .3em; font-size: 8pt; width: 40px; text-align: center">POS</td>
                   <td style="padding: .3em .3em; font-size: 8pt; width: 270px; text-align: left">
                    @if($quote->lang == 'en')
                      NAME
                    @endif
                    @if($quote->lang == 'pt')
                      NOME
                    @endif
                    @if($quote->lang == 'es')
                      NOMBRE
                    @endif
                    @if($quote->lang == null)
                      NOMBRE
                    @endif
                   </td>
                   <td style="padding: .3em .3em; font-size: 8pt; text-align: center">
                   @if($quote->lang == 'en')
                      QTY
                    @endif
                    @if($quote->lang == 'pt')
                      CANT.
                    @endif
                    @if($quote->lang == 'es')
                      CANT.
                    @endif
                    @if($quote->lang == null)
                      CANT.
                    @endif 
                  </td>
                   <td style="padding: .3em .3em; font-size: 8pt; text-align: right">
                    @if($quote->lang == 'en')
                      UNIT PRICE
                    @endif
                    @if($quote->lang == 'pt')
                      PREÇO UNIT.
                    @endif
                    @if($quote->lang == 'es')
                      PRECIO UNIT.
                    @endif
                    @if($quote->lang == null)
                      PRECIO UNIT.
                    @endif
                   </td>
                   <td style="padding: .3em .3em; font-size: 8pt; text-align: right">
                    @if($quote->lang == 'en')
                      TOTAL PRICE
                    @endif
                    @if($quote->lang == 'pt')
                      PREÇO TOTAL
                    @endif
                    @if($quote->lang == 'es')
                      PRECIO TOT.
                    @endif
                    @if($quote->lang == null)
                      PRECIO TOT.
                    @endif
                    </td>
                    <td style="padding: .3em .3em; font-size: 8pt; width: 50px; text-align: right">
                    @if($quote->lang == 'en')
                      WEIGHT
                    @endif
                    @if($quote->lang == 'pt')
                      PESO
                    @endif
                    @if($quote->lang == 'es')
                      PESO
                    @endif
                    @if($quote->lang == null)
                      PESO
                    @endif
                    </td>
                   <td style="padding: .3em .3em; font-size: 8pt; width: 80px; text-align: right">@if($quote->lang == 'en')
                      TOT. WEIGHT
                    @endif
                    @if($quote->lang == 'pt')
                      PESO
                    @endif
                    @if($quote->lang == 'es')
                      PESO TOT.
                    @endif
                    @if($quote->lang == null)
                      PESO TOT.
                    @endif</td>
                 </tr>
               </thead>
               <tbody>
                  @php
                    $firstPageTotal = 0;
                  @endphp
                 @foreach($newItem as $key => $item)
                 <tr class="@if(($key + 1) % 2 == 0) bg-gray @endif">
                   <td style="padding: .3em .3em; font-size: 8pt; width: 50px; width: 40px; text-align: center; color: black">{{$item['position']}}</td>
                   <td style="padding: .3em .3em; font-size: 8pt; width: 270px; text-align: left; color: black">
                    @if($quote->Client->country == 'US')
                      {{App\Models\Item\Item::find($item['item_id'])->name_en}}
                    @endif
                    @if($quote->Client->country == 'BR')
                      {{App\Models\Item\Item::find($item['item_id'])->name_br}}
                    @endif
                    @if($quote->Client->country != 'BR' and $quote->Client->country != 'US')
                      {{App\Models\Item\Item::find($item['item_id'])->name}}
                    @endif
                  </td>
                   <td style="padding: .3em .3em; font-size: 8pt; text-align: center; color: black">{{$item['quantity']}}.00</td>
                   <td style="padding: .3em .3em; font-size: 8pt; text-align: right; color: black"><span>$</span>{{number_format($item['price'], 2)}}</td>
                   <td style="padding: .3em .3em; font-size: 8pt; text-align: right; color: black"><span>$</span>{{number_format($item['total'], 2)}}</td>
                   <td style="padding: .3em .3em; font-size: 8pt; width: 50px; text-align: right; color: black">
                     {{number_format($item['weight'], 2)}}
                   </td>
                   <td style="padding: .3em .3em; font-size: 8pt; width: 80px; text-align: right; color: black">
                     {{number_format($item['totalWeight'], 2)}}
                   </td>
                 </tr>
                  @php
                    $firstPageTotal = $firstPageTotal + $item['total'];
                  @endphp
                 @endforeach
               </tbody>
             </table>
           </center>
         </div>
       </div>

       <div class="footer2">
        <div class="row">
         <div class="col-xs-3 mt-md font-100" style="font-weight: 600; margin-right: 20px; padding-left: 15px; border-bottom: 0px solid;">
           <b style="color: black;">
            @if($quote->lang == 'en')
              TOTAL WEIGHT
                    @endif
                    @if($quote->lang == 'pt')
                      PESO TOTAL
                    @endif
                    @if($quote->lang == 'es')
                      PESO TOTAL
                    @endif
                    @if($quote->lang == null)
                      PESO TOTAL
                    @endif: <span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">{{number_format($quote->totalWeight, 2)}} [TN]</span>
           </b>
         </div>
         <div class="col-xs-8 font-100" style="font-weight: 300; margin-right: 0px; text-align: right">
           <div class="row">
             <div class="col-xs-11 font-100" style="font-weight: 300; margin-right: 0px; text-align: right;">
               <b style="color: black;">
                @if($keNew == 0)
                    @if(count($quote->newItems) < 2)
                       @if($quote->lang == 'en')
                          SUBTOTAL PRICE (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          SUBTOTAL (USD):
                        @endif
                        @if($quote->lang == 'es')
                          SUBTOTAL (USD):
                        @endif
                        @if($quote->lang == null)
                          SUBTOTAL (USD):
                        @endif <span class="font-88" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['subtotal'], 2)}}</span>
                      @else
                        VAN (USD): <span class="font-88" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($firstPageTotal, 2)}}</span>
                    @endif
                  @else
                    @if($quote->lang == 'en')
                      SUBTOTAL PRICE (USD):
                    @endif
                    @if($quote->lang == 'pt')
                      SUBTOTAL (USD):
                    @endif
                    @if($quote->lang == 'es')
                      SUBTOTAL (USD):
                    @endif
                    @if($quote->lang == null)
                      SUBTOTAL (USD):
                    @endif <span class="font-88" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['subtotal'], 2)}}</span>
                @endif
               </b>
             </div><br>
             <div class="col-xs-11 font-100">
               <b style="color: black;">
                  @if($keNew == 0)
                      @if(count($quote->newItems) < 2)
                        @if($quote->lang == 'en')
                          Crating/Packaging/Container Loading (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          Engradado/Embalagem/Carregamento de Recipiente (USD):
                        @endif
                        @if($quote->lang == 'es')
                          Embalaje/carga de contenedores (USD):
                        @endif
                        @if($quote->lang == null)
                          Embalaje/carga de contenedores (USD):
                        @endif <span class="font-88" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['packaging_value'], 2)}}</span>
                      @endif
                    @else
                        @if($quote->lang == 'en')
                          Crating/Packaging/Container Loading (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          Engradado/Embalagem/Carregamento de Recipiente (USD):
                        @endif
                        @if($quote->lang == 'es')
                          Embalaje/carga de contenedores (USD):
                        @endif
                        @if($quote->lang == null)
                          Embalaje/carga de contenedores (USD):
                        @endif <span class="font-88" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['packaging_value'], 2)}}</span>
                  @endif
               </b>
             </div>
             @if($quote->discount > 0)
               <br>
               <div class="col-xs-11 font-100" style="text-align: right;">
                 <b style="color: red;">
                  @if($keNew == 0)
                      @if(count($quote->newItems) < 2)
                        @if($quote->lang == 'en')
                          DISCOUNT (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          DESCONTO (USD):
                        @endif
                        @if($quote->lang == 'es')
                          DESCUNTO (USD):
                        @endif
                        @if($quote->lang == null)
                          DESCUENTO (USD):
                        @endif <span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['discount'], 2)}}</span>
                      @endif
                    @else
                        @if($quote->lang == 'en')
                          DISCOUNT (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          DESCONTO (USD):
                        @endif
                        @if($quote->lang == 'es')
                          DESCUENTO (USD):
                        @endif
                        @if($quote->lang == null)
                          DESCUENTO (USD):
                        @endif -<span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['discount'], 2)}}</span>
                  @endif
                 </b>
               </div>
             @endif<br>
             <div class="col-xs-11 font-100" style="text-align: right;">
               <b style="color: black;">
                @if($keNew == 0)
                      @if(count($quote->newItems) < 2)
                        @if($quote->lang == 'en')
                          INTERNATIONAL FREIGHT (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          FRETE INTERNACIONAL (USD):
                        @endif
                        @if($quote->lang == 'es')
                          FLETE INTERNACIONAL (USD):
                        @endif
                        @if($quote->lang == null)
                          FLETE INTERNACIONAL (USD):
                        @endif <span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['freight_value'], 2)}}</span>
                      @endif
                    @else
                        @if($quote->lang == 'en')
                          INTERNATIONAL FREIGHT (USD):
                        @endif
                        @if($quote->lang == 'pt')
                          FRETE INTERNACIONAL (USD):
                        @endif
                        @if($quote->lang == 'es')
                          FLETE INTERNACIONAL (USD):
                        @endif
                        @if($quote->lang == null)
                          FLETE INTERNACIONAL (USD):
                        @endif <span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['freight_value'], 2)}}</span>
                  @endif
               </b>
             </div><br>
             <div class="col-xs-11 font-100" style="text-align: right;">
               <b style="color: black;">
                @if($keNew == 0)
                      @if(count($quote->newItems) < 2)
                        @if($quote->lang == 'en')
                                TOTAL (USD):
                              @endif
                              @if($quote->lang == 'pt')
                                TOTAL (USD):
                              @endif
                              @if($quote->lang == 'es')
                                TOTAL (USD):
                              @endif
                              @if($quote->lang == null)
                                TOTAL (USD):
                              @endif <span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['total'], 2)}}</span>
                      @endif
                    @else
                      @if($quote->lang == 'en')
                      TOTAL (USD):
                            @endif
                            @if($quote->lang == 'pt')
                            TOTAL (USD):
                            @endif
                            @if($quote->lang == 'es')
                            TOTAL (USD):
                            @endif
                            @if($quote->lang == null)
                            TOTAL (USD):
                            @endif <span class="font-100" style="padding-top: .3em; ; font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">${{number_format($quote['total'], 2)}}</span>
                  @endif
               </b>
             </div>
           </div>
         </div>
        </div>
        <br>
        <div class="row" style="min-height: 130px;">
           <div class="col-xs-6">
             <div class="row">
               <div class="col-xs-12 font-100" style="font-weight: 600; color: black">
                  <b>
                    @if($quote->lang == 'en')
                      SHIPPING OR MISC INSTRUCTIONS:
                    @endif
                    @if($quote->lang == 'pt')
                      INSTRUÇÕES DE ENVIO OU DIVERSAS:
                    @endif
                    @if($quote->lang == 'es')
                      INSTRUCCIONES DE ENVÍO O VARIAS:
                    @endif
                    @if($quote->lang == null)
                      INSTRUCCIONES DE ENVÍO O VARIAS:
                    @endif
                  </b>
               </div><br>
               <div class="col-xs-11 description font-100" style="font-weight: 400; color: black; margin-top: 0rem !important;">
                  @if($quote->lang == 'en')
                    {!! $quote['description_en'] !!}
                  @endif
                  @if($quote->lang == 'pt')
                    {!! $quote['description_br'] !!}
                  @endif
                  @if($quote->lang == 'es')
                  {!! $quote['description'] !!}
                  @endif
                  @if($quote->lang == null)
                    {!! $quote['description'] !!}
                  @endif
               </div>
             </div>
           </div>
        </div>
        <div class="row" style="height: 200px">
           <div class="col-xs-3" style="margin-top: 0px">
            <div><p></p></div>
            <div><p></p></div>
            <div class="font-100" style="color: black; margin-top: -2px"><b>
              @if($quote->lang == 'en')
                BANK INSTRUCTIONS
              @endif
              @if($quote->lang == 'pt')
                INSTRUÇÕES DO BANCO
              @endif
              @if($quote->lang == 'es')
                INSTRUCCIONES BANCARIAS
              @endif
              @if($quote->lang == null)
                INSTRUCCIONES BANCARIAS
              @endif
            </b></div>
            <div style="font-size: 8pt" style="color: black; margin-top: -10px"><b>Bank of America</b></div>
            <div style="font-size: 8pt" style="color: black; margin-top: -10px"><b>Agri Technology Solutions, LLC</b></div>
            <div style="font-size: 8pt" style="color: black; margin-top: -10px"><b>ABA WIRE No. 026009593</b></div>
            <div style="font-size: 8pt" style="color: black; margin-top: -10px"><b>SWIFT No. BOFAUS3N </b></div>
            <div style="font-size: 8pt" style="color: black; margin-top: -10px"><b>Account Number: 229045809704</b></div>
           </div>
           <div class="col-xs-4 mt-xl">
            <br>
             <div class="row mt-sm">
              <div class="col-xs-12 center" style="margin-top: -90px; padding-bottom: 3px">
                <p class="mt-md">
                  <b style="color: black;" class="font-100">
                  @if($quote->lang == 'en')
                    QUOTED BY:
                  @endif
                  @if($quote->lang == 'pt')
                    COTIZADO POR:
                  @endif
                  @if($quote->lang == 'es')
                    COTIZADO POR:
                  @endif
                  @if($quote->lang == null)
                    COTIZADO POR:
                  @endif <br>
                    @if(isset($quote->User))
                      <span style="font-size: 8pt; font-weight: normal !important; text-transform: capitalize;" style="margin-top: -40px!important; font-weight: normal">{{$quote->User->first_name}} {{$quote->User->last_name}}</span>
                      @else
                      -
                    @endif
                  </b><br>
                  <b style="color: black;" class="font-100">
                  @if($quote->lang == 'en')
                    APPROVED BY:
                  @endif
                  @if($quote->lang == 'pt')
                    APROVADO POR:
                  @endif
                  @if($quote->lang == 'es')
                    APROBADO POR:
                  @endif
                  @if($quote->lang == null)
                    APROBADO POR:
                  @endif <br>
                    @if(isset($quote->UserConfirm))
                      <span style="font-size: 8pt; font-weight: normal !important; text-transform: capitalize;" style="margin-top: -40px!important; font-weight: normal">{{$quote->UserConfirm->first_name}} {{$quote->UserConfirm->last_name}}</span>
                      @else
                      -
                    @endif
                  </b>
                </p>
              </div>
               <div class="col-xs-12 center">
                 <b style="color: black;" class="font-88">
                  <span class="font-88" style="font-size: 8pt !important; padding-bottom: .5em; font-weight: normal">
                    <img style="margin-top: 5px" src="{{url('images/assets/phone.png')}}" width="15px"> 1(407)978-3038
                  </span>
                </b>
              </div><br>
              <div class="col-xs-12 center mt-lg">
                 <b style="color: black;" class="font-88">
                  <span class="font-88" style="font-size: 8pt !important; padding-bottom: .5em; height: 50px; font-weight: normal">
                    <img style="margin-top: 5px" src="{{url('images/assets/email.png')}}" width="15px"> info@agri-techsolutions.com
                  </span>
                </b>
              </div><br>
              <div class="col-xs-12 center mt-lg">
                 <b style="color: black;" class="font-100">
                  <span class="font-100" style="font-size: 8pt !important; padding-bottom: .5em; font-weight: normal">
                    <a href="http://www.agri-techsolutions.com" style="color: black; text-decoration: none" target="_blank"><img style="margin-top: 4px" src="{{url('images/assets/url.png')}}" width="15px">  http://www.agri-techsolutions.com</a>
                  </span>
                </b>
              </div>
             </div>
           </div>
           <div class="col-xs-3" style="text-align: center; margin-top: -18px !important;">
            <p class="mt-md" style="margin-right: -30px;">
              <b style="color: black;" class="font-100">
                  @if($quote->lang == 'en')
                    CUSTOMER CONFIRMATION
                  @endif
                  @if($quote->lang == 'pt')
                    CONFIRMAÇÃO DO CLIENTE
                  @endif
                  @if($quote->lang == 'es')
                    CONFIRMACIÓN DEL CLIENTE
                  @endif
                  @if($quote->lang == null)
                    CONFIRMACIÓN DEL CLIENTE
                  @endif <br>
                (Signature)<br><br>
                <span style="font-size: 8pt; font-weight: normal !important; text-transform: capitalize;" style="margin-top: -40px!important; font-weight: normal">
                </span>
              </b>
            </p>
            <div style="border: 1px solid black; margin-right: -30px;"></div><br>
              <p style="margin-top: -10px; margin-right: -30px">
                <b style="color: black;" class="font-100">
                  @if($quote->lang == 'en')
                    DATE:
                  @endif
                  @if($quote->lang == 'pt')
                    DATA:
                  @endif
                  @if($quote->lang == 'es')
                    FECHA:
                  @endif
                  @if($quote->lang == null)
                    FECHA:
                  @endif <br>
                </b>
              </p><br>
              <div style="border: 1px solid black; margin-right: -30px;"></div>
           </div>
         </div>
         <div class="row" style="margin-top: -70px">
           <div class="col-xs-11 text-center font-100">
             Agriquote Ver. 1.0
           </div>
         </div>
      </div>
      @if($keNew == 0)
        @if(count($quote->newItems) == 2)
          <div class="page_break"></div>
        @endif
      @endif
      @endforeach

      <div class="page_break"></div>
      <!--items section-->
      @foreach($quote->Items as $key => $item)
        <div class="row" style="margin-right: -20px">
          <div style="height: 37px !important; padding-top: .2em; padding-bottom: .3em; text-align: center" class="col-xs-9 font-188 bg-primary">
            <b>AGRI TECHNOLOGY SOLUTIONS</b>
          </div>
          <div class="col-xs-2">
            @if($item->Item->image)
              <img src="{{url('images/items', $item->Item->image)}}" width="90px">
            @endif
          </div>
        </div>
        <div class="row mt-lg" style="margin-left: -15px">
          <div style="height: 30px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-1 font-122 bg-primary">
            @if($quote->lang == 'en')
              POSITION
            @endif
            @if($quote->lang == 'pt')
              POSIÇÃO
            @endif
            @if($quote->lang == 'es')
              POSICIÓN
            @endif
            @if($quote->lang == null)
              POSICIÓN
            @endif
          </div>
          <div style="height: 30px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: center" class="col-xs-7 font-122 bg-primary">
            @if($quote->lang == 'en')
              ITEM NAME
            @endif
            @if($quote->lang == 'pt')
              NOME DO ITEM
            @endif
            @if($quote->lang == 'es')
              NOMBRE DEL ITEM
            @endif
            @if($quote->lang == null)
              NOMBRE DEL ITEM
            @endif
          </div>
          <div style="height: 30px !important; padding-top: .3em; padding-bottom: .3em; margin-left: 2px; text-align: right" class="col-xs-2 font-122 bg-primary">
            @if($quote->lang == 'en')
              PRICE
            @endif
            @if($quote->lang == 'pt')
              PREÇO
            @endif
            @if($quote->lang == 'es')
              PRECIO
            @endif
            @if($quote->lang == null)
              PRECIO
            @endif
          </div>
        </div>
        <div class="row mt-sm" style="margin-left: -20px">
          <div class="col-xs-1 font-100" style="text-align: center; color: black">
            {{$item['position']}}
          </div>
          <div class="col-xs-7 font-100" style="color: black">
            <p style="text-transform: uppercase;">
              @if($quote->lang == 'en')
                <b>{{$item->Item->name_en}}</b>
              @endif
              @if($quote->lang == 'pt')
                <b>{{$item->Item->name_br}}</b>
              @endif
              @if($quote->lang == 'es')
                <b>{{$item->Item->name}}</b>
              @endif
              @if($quote->lang == null)
                <b>{{$item->Item->name}}</b>
              @endif
            </p>
          </div>
          <div class="col-xs-2 font-100" style="text-align: right; color: black; margin-left: 5px; text-align: right">
            @if($item['costWithTax'] > 0)
              @if($item['cost_type'] == true)
                <span>$</span>{{number_format($item['costWithTax']  - $item->getSubitemTotal(), 2)}}
                @else
                <span>$</span>{{number_format($item['costWithTax'], 2)}}
              @endif
            @endif
          </div>
        </div>
        <div class="row mt-md" style="margin-left: -20px">
          <div class="col-xs-1 font-100" style="text-align: center; color: black">
          </div>
          <div class="col-xs-7 font-100">
            <div class="row">
              <div class="col-xs-11" style="color: black; text-align: justify; font-weight: normal !important">
                <p><b>
                  @if($quote->lang == 'en')
                    <b>{!! $item->Item->description_en !!}</b>
                  @endif
                  @if($quote->lang == 'pt')
                    <b>{!! $item->Item->description_br !!}</b>
                  @endif
                  @if($quote->lang == 'es')
                    <b>{!! $item->Item->description !!}</b>
                  @endif
                  @if($quote->lang == null)
                    <b>{!! $item->Item->description !!}</b>
                  @endif
                </b></p>
              </div>
            </div>
          </div>
          <div class="col-xs-2 font-100" style="text-align: center; color: black">
          </div>
        </div>
        @if(count($item->Attributes) > 0)
          <div class="row" style="margin-left: -20px !important; margin-top: -10px">
            <div class="col-xs-1">
              <p class="font-100" style="text-transform: uppercase; color: black; text-align: center;">
              </p>
            </div>
            <div class="col-xs-7">
              @if($item->show_title)
                <p class="font-100" style="color: BLACK;"><b>
                  <p class="font-100" style="color: black;"><b>
                    @if($quote->lang == 'en')
                      <b>FEATURES</b>
                    @endif
                    @if($quote->lang == 'pt')
                      <b>CARACTERISTICAS</b>
                    @endif
                    @if($quote->lang == 'es')
                      <b>CARACTERISTICAS</b>
                    @endif
                    @if($quote->lang == null)
                      <b>CARACTERISTICAS</b>
                    @endif
                  </b></p>
                </b></p>
              @endif
            </div>
            <div class="col-xs-2">
              <p class="font-100" style="text-transform: uppercase; color: black; text-align: center;">
              </p>
            </div>
          </div>
        @endif
        <div class="row" style="margin-left: -20px !important; margin-top: -12px;">
          <div class="col-xs-1 font-100" style="text-align: center; color: black">
          </div>
          <div class="col-xs-7 font-100">
            <table style="width: 450px; margin-left: 0px;">
              @foreach(array_chunk(json_decode($item->Attributes->where('selected', true), true), 2) as $attribute)
                <tr style="color: black;">
                  @foreach($attribute as $attr)
                    @if($attr['selected'] == true)
                      <td>
                        <b>
                          <span style="text-transform: capitalize; font-weight: normal !important">
                          @if($quote->lang == 'en')
                            {{App\Models\Attribute\Attribute::find($attr['attribute_id'])->name_en}}:
                          @endif
                          @if($quote->lang == 'pt')
                            {{App\Models\Attribute\Attribute::find($attr['attribute_id'])->name_br}}:
                          @endif
                          @if($quote->lang == 'es')
                            {{App\Models\Attribute\Attribute::find($attr['attribute_id'])->name}}:
                          @endif
                          @if($quote->lang == null)
                            {{App\Models\Attribute\Attribute::find($attr['attribute_id'])->name}}:
                          @endif</span> <span style="font-weight: normal">
                          {{$attr['value']}}
                          </span>
                        </b>
                      </td>
                    @endif
                  @endforeach
                </tr>
              @endforeach
            </table>
          </div>
          <div class="col-xs-2 font-100" style="text-align: center; color: black">
          </div>
        </div>
        @if(count($item->Subitems) > 0)
        <div class="row mt-md" style="margin-left: -20px !important;">
          <div class="col-xs-1">
            <p class="font-100" style="text-transform: uppercase; color: black; text-align: center;">
            </p>
          </div>
          <div class="col-xs-7">
            @if($item->show_title2)
              <p class="font-100" style="color: BLACK;"><b>
                @if($quote->lang == 'en')
                      <b>ACCESSORIES</b>
                    @endif
                    @if($quote->lang == 'pt')
                      <b>ACESSÓRIOS</b>
                    @endif
                    @if($quote->lang == 'es')
                      <b>ACCESORIOS</b>
                    @endif
                    @if($quote->lang == null)
                      <b>ACCESORIOS</b>
                    @endif
              </b></p>
            @endif
          </div>
          <div class="col-xs-2">
            <p class="font-100" style="text-transform: uppercase; color: black; text-align: center;">
            </p>
          </div>
        </div>
        @endif
        @foreach($item->Subitems as $key3 => $subitem)
        <div class="row" style="margin-left: -20px !important; margin-top: -10px">
          <div class="col-xs-1">
            <p class="font-100" style="text-transform: uppercase; color: black; text-align: center;">
              {{($item['position'])}}.{{($key3 + 1)}}
            </p>
          </div>
          <div class="col-xs-7">
            <p class="font-100" style="text-transform: capitalize; color: black;">
              <b style="font-weight: normal;">
                    @if($quote->lang == 'en')
                      {{$subitem->Subitem->name_en}}
                    @endif
                    @if($quote->lang == 'pt')
                      {{$subitem->Subitem->name_br}}
                    @endif
                    @if($quote->lang == 'es')
                      {{$subitem->Subitem->name}}
                    @endif
                    @if($quote->lang == null)
                    {{$subitem->Subitem->name}}
                    @endif .Qty=<span style="margin-left: 10px">{{$subitem['quantity']}}</span></b>
            </p>
          </div>
          <div class="col-xs-2">
            <p class="font-100" style="text-transform: uppercase; color: black; text-align: right;">
              ${{number_format($subitem['quantity'] * $subitem['costWithTax'], 2)}}
            </p>
          </div>
        </div>
        @endforeach
        <div class="footer">
          <div class="row" style="margin-top: -70px;">
            <div class="col-xs-1">
              <p class="font-100" style="text-transform: uppercase; color: black; text-align: center;">
              </p>
            </div>
            <div class="col-xs-7">
              <p class="font-100" style="text-transform: uppercase; color: black;">
              </p>
            </div>
            <div class="col-xs-2">
              <p class="font-100" style="text-transform: capitalize; color: black; text-align: right;">
                @if($item['cost_type'] == true)
                  <span>$</span>{{number_format($item['costWithTax'], 2)}}
                  @else
                  <span>$</span>{{number_format($item['price'], 2)}}
                @endif
              </p>
            </div>
          </div>
           <div class="row" style="margin-top: -70px">
             <div class="col-xs-11 text-center font-100">
               Agriquote Ver. 1.0
             </div>
           </div>
        </div>
        <div class="page_break"></div>
      @endforeach
      <div class="row">
        <div style="height: 37px !important; padding-top: .2em; padding-bottom: .3em; text-align: center" class="col-xs-11 font-188 bg-primary">
          <b>AGRI TECHNOLOGY SOLUTIONS</b>
        </div><br>
      </div>
      <div class="row mt-lg">
        <div style="height: 37px !important; padding-top: .3em; padding-bottom: .3em; text-align: center; font-size: 14pt;" class="col-xs-11 bg-primary">
          <b>
            @if($quote->lang == 'en')
              Explanatory notes
            @endif
            @if($quote->lang == 'pt')
              Notas explicativas
            @endif
            @if($quote->lang == 'es')
              Notas Aclaratorias
            @endif
            @if($quote->lang == null)
              Notas Aclaratorias
            @endif
          </b>
        </div><br>
      </div>
      <div class="row mt-lg">
        <div class="col-xs-11">
          <p class="font-100" style="color: black; text-align: justify;">
            <b>
              @if($quote->lang == 'en')
                GENERAL CONDITIONS OF SALE
              @endif
              @if($quote->lang == 'pt')
                CONDIÇÕES GERAIS DE VENDA
              @endif
              @if($quote->lang == 'es')
                CONDICIONES GENERALES DE VENTA
              @endif
              @if($quote->lang == null)
                CONDICIONES GENERALES DE VENTA
              @endif</b>:<br>
          </p>
              @if($quote->lang == 'en')
                {!! html_entity_decode($quote['last_page_en']) !!}
              @endif
              @if($quote->lang == 'pt')
              {!! html_entity_decode($quote['last_page_br']) !!}
              @endif
              @if($quote->lang == 'es')
                {!! html_entity_decode($quote['last_page']) !!}
              @endif
              @if($quote->lang == null)
                {!! html_entity_decode($quote['last_page']) !!}
              @endif
        </div>
      </div>
    </main>
  </body>
</html>