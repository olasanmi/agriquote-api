<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DealerController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\AttributeController;
use App\Http\Controllers\SubitemController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\QuoteController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\ItemQuoteSubitemController;
use App\Http\Controllers\QuoteItemController;
use App\Http\Controllers\PurchaseOrderController;
use App\Http\Controllers\RevisionController;
use App\Http\Controllers\TranslateController;
use App\Http\Controllers\SiloModelController;
use App\Http\Controllers\RequisitionController;
use App\Http\Controllers\RequisitionSiloController;
use App\Http\Controllers\RequisitionPdfController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// load all controllers in routes

Route::group(['prefix' => 'v1'], function () {
    // auth
    Route::group(['prefix' => 'auth'], function () {
        Route::post('recovery', [AuthController::class, 'recovery']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout'])->middleware(['auth:sanctum']);
    });
    Route::group(['prefix' => 'confirm'], function () {
        Route::post('user', [UserController::class, 'confirm']);
    });
    
    Route::resource('SiloModelController', SiloModelController::class);

    // client quote routes
    Route::get('/get/quote/{uuid}', [QuoteController::class, 'filter']);
    Route::post('/quote/client/response', [QuoteController::class, 'clientComment']);
    Route::post('/quote/terms/accept', [QuoteController::class, 'acceptTermsQuotePdf']);

    Route::post('/clients/call', [ClientController::class, 'callClient']);

    // rutas protegidas
    Route::group(['middleware' => 'auth:sanctum'], function() {
        // resources rutas
        Route::resource('client', ClientController::class);
        // client search
        Route::post('/search/client', [ClientController::class, 'search']);
        //end client routes
        Route::resource('users', UserController::class);
        // profile upload
        Route::post('/profile/photo', [UserController::class, 'loadProfileImage']);
        //end users routes
        Route::resource('dealer', DealerController::class);
        // dealer search
        Route::post('/search/dealer', [DealerController::class, 'search']);
        //end dealer routes
        Route::resource('item', ItemController::class);
        // items search
        Route::post('/search/items', [ItemController::class, 'search']);
        //end items routes
        Route::resource('attribute', AttributeController::class);
        Route::resource('subitem', SubitemController::class);
        //end items routes
        Route::resource('ItemQuoteSubitem', ItemQuoteSubitemController::class);
        // purchase orders
        Route::resource('purchaseOrder', PurchaseOrderController::class);
        Route::get('/generate-pdf/{purchaseOrder}', [PurchaseOrderController::class, 'showPdf']);
        Route::get('/send-email/{purchaseOrder}', [PurchaseOrderController::class, 'sendEmailPdf']);
        //end purchase orders routes
        //quote routes
        //end items routes
        Route::resource('QuoteItem', QuoteItemController::class);
        //quote routes
        Route::resource('quote', QuoteController::class);
        Route::get('opportunities/quote', [RevisionController::class, 'index']);
        Route::get('opportunities/delete/{revision}', [RevisionController::class, 'destroy']);
        Route::get('pdf/quote', [QuoteController::class, 'showPdf']);
        Route::get('get/quote', [QuoteController::class, 'getByRefs']);
        Route::post('complete/quote', [RevisionController::class, 'completeOpportunities']);
        Route::post('quotes/blueprints/delete', [QuoteController::class, 'clearBluePrints']);
        Route::post('opportunities/remove', [RevisionController::class, 'deleteOportunities']);
        Route::post('interval/quote', [RevisionController::class, 'changeEmailInterval']);
        Route::post('email/play', [RevisionController::class, 'emailPause']);
        Route::get('quotes/trashed', [RevisionController::class, 'listTrash']);
        Route::post('restore/trashed', [RevisionController::class, 'restoreTrash']);
        Route::post('send/email', [RevisionController::class, 'sendEmail']);
        //end reference routes
        //reference routes
        Route::post('quote/reference', [QuoteController::class, 'generateReference']);
        Route::post('quote/status/change', [QuoteController::class, 'changeStatus']);
        //end reference routes
        // blueprints upload
        Route::post('/quote/blueprints', [QuoteController::class, 'uploadBluePrint']);
        //end blueprints routes
        Route::resource('provider', ProviderController::class);
        // main route
        Route::get('main/count', [MainController::class, 'mainCount']);
        // translate upload
        Route::post('/translate', [TranslateController::class, 'translate']);
        // requisition
        Route::resource('requisition', RequisitionController::class);
        Route::resource('requisitionSilo', RequisitionSiloController::class);
        Route::post('upload/croquis', [RequisitionSiloController::class, 'uploadCroquis']);
        Route::get('filter/silos/models', [SiloModelController::class, 'fulterByBolumen']);
        Route::get('send/requisition', [RequisitionController::class, 'sendEmail']);
        Route::get('code/requisition', [RequisitionController::class, 'generateCode']);
        Route::post('email/requisition', [RequisitionPdfController::class, 'sendFirstEmail']);
        Route::get('oportunities/requisition', [RequisitionPdfController::class, 'index']);
        Route::post('complete/requisition', [RequisitionPdfController::class, 'completeRequisition']);
        Route::post('requisition-email/play', [RequisitionPdfController::class, 'emailPause']);
    });
});
