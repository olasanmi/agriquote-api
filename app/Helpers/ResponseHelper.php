<?php
namespace App\Helpers;

use Response;

class ResponseHelper
{
	function __construct()
	{
	}

	public static function response($status, $message, $code) {
		$response = Response::make(json_encode([$status => $message]), $code)->header('Content-Type','application/json');
        return $response;
	}
}