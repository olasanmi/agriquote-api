<?php
namespace App\Helpers;

use File;

class MainHelper
{
	function __construct()
	{
	}

	// generate reference
	public static function reference($type, $length) {
		$key = '';
		if ($type == 'string') {
        	$keys = array_merge(range(0, 6), range('A', 'Z'));
		} else {
			$keys = array_merge(range(0, 9));
		}
		for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
	}

	//upload file
    public static function uploadFile($file, $pathModel) {
        $name = $file->getClientOriginalName();
        $file->move('images/' . $pathModel, $name);
        return $name;
    }

    //delete file
    public static function deleteFile($file, $pathModel) {
        $oldfile = public_path('images/'.$pathModel.'/'.$file);
        if (File::exists($oldfile)) {
            unlink($oldfile);
        }
    }

    public function deleteQuotePdf($file) {
    	$oldfile = public_path('pdf/quote/' . $file);
    	if (File::exists($oldfile)) {
            unlink($oldfile);
        }
    }

}