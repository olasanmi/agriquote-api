<?php

namespace App\Observers\Dealer;

use App\Models\Dealer\Dealer;
use Uuid;

class DealerObserver
{
    /**
     * Handle the Dealer "created" event.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return void
     */
    public function created(Dealer $dealer)
    {
        //
        $dealer->update(['uuid' => Uuid::generate()->string]);
        $dealer->update(['fullname' => $dealer->name . ' ' . $dealer->last_name]);
    }

    /**
     * Handle the Dealer "updated" event.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return void
     */
    public function updated(Dealer $dealer)
    {
        //
    }

    /**
     * Handle the Dealer "deleted" event.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return void
     */
    public function deleted(Dealer $dealer)
    {
        //
    }

    /**
     * Handle the Dealer "restored" event.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return void
     */
    public function restored(Dealer $dealer)
    {
        //
    }

    /**
     * Handle the Dealer "force deleted" event.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return void
     */
    public function forceDeleted(Dealer $dealer)
    {
        //
    }
}
