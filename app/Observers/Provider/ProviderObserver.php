<?php

namespace App\Observers\Provider;

use App\Models\Provider\Provider;
use Uuid;

class ProviderObserver
{
    /**
     * Handle the Provider "created" event.
     *
     * @param  \App\Models\Provider\Provider  $provider
     * @return void
     */
    public function created(Provider $provider)
    {
        //
        $provider->update(['uuid' => Uuid::generate()->string]);
    }

    /**
     * Handle the Provider "updated" event.
     *
     * @param  \App\Models\Provider\Provider  $provider
     * @return void
     */
    public function updated(Provider $provider)
    {
        //
    }

    /**
     * Handle the Provider "deleted" event.
     *
     * @param  \App\Models\Provider\Provider  $provider
     * @return void
     */
    public function deleted(Provider $provider)
    {
        //
    }

    /**
     * Handle the Provider "restored" event.
     *
     * @param  \App\Models\Provider\Provider  $provider
     * @return void
     */
    public function restored(Provider $provider)
    {
        //
    }

    /**
     * Handle the Provider "force deleted" event.
     *
     * @param  \App\Models\Provider\Provider  $provider
     * @return void
     */
    public function forceDeleted(Provider $provider)
    {
        //
    }
}
