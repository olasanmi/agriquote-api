<?php

namespace App\Observers\Quote;

use App\Models\Quote\Quote;
use App\Models\QuoteItem\QuoteItem;
use Uuid;

class QuoteObserver
{
    /**
     * Handle the Quote "created" event.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return void
     */
    public function created(Quote $quote)
    {
        //
        $quote->update(['uuid' => Uuid::generate()->string]);
        $quote->update(['country' =>  $quote->Client->country]);
        switch ($quote->Client->country) {
            case 'US':
                $quote->update(['lang' =>  'en']);
                break;
            case 'BR':
                    $quote->update(['lang' =>  'pt']);
                    break;
            default:
            $quote->update(['lang' =>  'es']);
                break;
        }
    }

    /**
     * Handle the Quote "updated" event.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return void
     */
    public function updated(Quote $quote)
    {
    }

    /**
     * Handle the Quote "deleted" event.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return void
     */
    public function deleted(Quote $quote)
    {
        //
    }

    /**
     * Handle the Quote "restored" event.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return void
     */
    public function restored(Quote $quote)
    {
        //
    }

    /**
     * Handle the Quote "force deleted" event.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return void
     */
    public function forceDeleted(Quote $quote)
    {
        //
    }
}
