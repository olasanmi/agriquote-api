<?php

namespace App\Observers\Item;

use App\Models\Item\Item;
use Uuid;

class ItemObserver
{
    /**
     * Handle the Item "created" event.
     *
     * @param  \App\Models\Item\Item  $item
     * @return void
     */
    public function created(Item $item)
    {
        //
        $item->update(['uuid' => Uuid::generate()->string]);
    }

    /**
     * Handle the Item "updated" event.
     *
     * @param  \App\Models\Item\Item  $item
     * @return void
     */
    public function updated(Item $item)
    {
        //
    }

    /**
     * Handle the Item "deleted" event.
     *
     * @param  \App\Models\Item\Item  $item
     * @return void
     */
    public function deleted(Item $item)
    {
        //
    }

    /**
     * Handle the Item "restored" event.
     *
     * @param  \App\Models\Item\Item  $item
     * @return void
     */
    public function restored(Item $item)
    {
        //
    }

    /**
     * Handle the Item "force deleted" event.
     *
     * @param  \App\Models\Item\Item  $item
     * @return void
     */
    public function forceDeleted(Item $item)
    {
        //
    }
}
