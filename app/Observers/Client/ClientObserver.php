<?php

namespace App\Observers\Client;

use App\Models\Client\Client;
use Uuid;

class ClientObserver
{
    /**
     * Handle the Client "created" event.
     *
     * @param  \App\Models\Client\Client  $client
     * @return void
     */
    public function created(Client $client)
    {
        //
        $client->update(['uuid' => Uuid::generate()->string]);
        $client->update(['fullname' => $client->name . ' ' . $client->last_name]);
    }

    /**
     * Handle the Client "updated" event.
     *
     * @param  \App\Models\Client\Client  $client
     * @return void
     */
    public function updated(Client $client)
    {
        //
    }

    /**
     * Handle the Client "deleted" event.
     *
     * @param  \App\Models\Client\Client  $client
     * @return void
     */
    public function deleted(Client $client)
    {
        //
    }

    /**
     * Handle the Client "restored" event.
     *
     * @param  \App\Models\Client\Client  $client
     * @return void
     */
    public function restored(Client $client)
    {
        //
    }

    /**
     * Handle the Client "force deleted" event.
     *
     * @param  \App\Models\Client\Client  $client
     * @return void
     */
    public function forceDeleted(Client $client)
    {
        //
    }
}
