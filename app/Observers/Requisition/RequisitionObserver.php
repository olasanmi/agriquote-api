<?php

namespace App\Observers\Requisition;

use App\Models\Requisition\Requisition;
use Uuid;

class RequisitionObserver
{
    /**
     * Handle the Requisition "created" event.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return void
     */
    public function created(Requisition $requisition)
    {
        //
        $requisition->update(['uuid' => Uuid::generate()->string]);
    }

    /**
     * Handle the Requisition "updated" event.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return void
     */
    public function updated(Requisition $requisition)
    {
        //
    }

    /**
     * Handle the Requisition "deleted" event.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return void
     */
    public function deleted(Requisition $requisition)
    {
        //
    }

    /**
     * Handle the Requisition "restored" event.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return void
     */
    public function restored(Requisition $requisition)
    {
        //
    }

    /**
     * Handle the Requisition "force deleted" event.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return void
     */
    public function forceDeleted(Requisition $requisition)
    {
        //
    }
}
