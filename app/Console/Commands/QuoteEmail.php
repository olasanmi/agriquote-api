<?php

namespace App\Console\Commands;

ini_set('max_execution_time', '5000000000000');

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\Revision\Revision;
use App\Services\Quote\QuoteServices;
use Log;

class QuoteEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'quote:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send quote email everyday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->quoteService = new QuoteServices();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d');
        $revision = Revision::where('next_email', '=', $now)
        ->where('user_status', '!=' , '4')
        // ->where('date_validation', '>=' , $now)
        ->where('user_status', '!=' , '7')
        ->where('accept_terms', true)
        ->where('is_complete', false)
        ->where('email_pause', '=', false)
        ->get();
        // dd(count($revision));
        // return true;
        foreach ($revision as $key => $value) {
            $this->quoteService->sendQuoteMail($value->Quote, $value->Quote->Client->email, $value);
            if ($value->Quote->email_contact_2)
                $this->quoteService->sendQuoteMail($value->Quote, $value->Quote->email_contact_2, $value);
            // send email to quote contact 3
            if ($value->Quote->email_contact_3)
                $this->quoteService->sendQuoteMail($value->Quote, $value->Quote->email_contact_3, $value);
            Log::error("Se ha enviado el correo de la cotización " . $value->code);
        }
        if (count($revision) < 1) {
            Log::error('No posee cotizaciones para la fecha ' . $now);
            echo "No posee correos de cotizaciones para enviar hoy.";
        }
    }
}
