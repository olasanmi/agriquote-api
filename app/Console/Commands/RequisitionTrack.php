<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\RequisitionPdf\RequisitionPdf;
use App\Services\Requisition\RequisitionServices;
use Log;

class RequisitionTrack extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'requisition:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Track al the requisition with status is don`t complete';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->requisitionServices = new RequisitionServices();
        $this->now = Carbon::now();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $requisitions = RequisitionPdf::where('next_email', '=', $this->now->format('Y-m-d'))
        ->where('is_pause', false)
        ->where('is_complete', false)
        ->get();
        if (count($requisitions) < 1) {
            Log::error("No existe requisición para enviar...");
        }
        foreach ($requisitions as $key => $value) {
            $this->requisitionServices->sendFirstEmail(RequisitionPdf::find($value->id));
            Log::error("Se ha enviado el correo de la requisición " . $value->Requisition->reference);
        }
        return 0;
    }
}
