<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Quote\Quote;
use App\Models\Revision\Revision;

class SetAllOpportunitiesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opportunities:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Config all the opportunities in first time.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    { 
        $quotes = Quote::where('quote_statuse_id', '=', 3)->get();
        foreach ($quotes as $quote => $q) {
            $explode = explode('-', $q->code);
            for ($i=0; $i <= $explode[3] ; $i++) { 
                $code = $explode[0] . '-' . $explode[1] . '-' . $explode[2] . '-' . str_pad( $i, 2, 0, STR_PAD_LEFT);
                $revision = Revision::create([
                    'quote_id' => $q->id,
                    'pdf' => 'https://hellenross.com/app/agriquote/api/public/pdf/quote/'. $code . '.pdf',
                    'code' => $code,
                    'total' => $q->total,
                    'next_email' => $q->next_email,
                    'email_interval' => $q->email_interval,
                    'is_complete' => $q->is_complete,
                    'is_trash' => $q->is_trash,
                    'accept_terms' => $q->accept_terms,
                    'comment' => $q->comment,
                    'user_status' => $q->user_status
                ]);
            }
        }
    }
}
