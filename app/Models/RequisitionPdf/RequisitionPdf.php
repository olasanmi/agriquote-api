<?php

namespace App\Models\RequisitionPdf;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RequisitionPdf extends Model
{
    use HasFactory;
    // fillable
    protected $fillable = ['pdf', 'provider_id', 'requisition_id', 'croquis', 'is_complete', 'is_pause', 'next_email'];

    // relations
    public function Requisition() {
        return $this->belongsTo('App\Models\Requisition\Requisition', 'requisition_id');
    }

    public function Provider() {
        return $this->belongsTo('App\Models\Provider\Provider', 'provider_id');
    }

    // last email send
    public function getLastEmailDate() {
        if ($this->next_email) {
            $date = Carbon::createFromFormat('Y-m-d', $this->next_email)->subDays(3);
            return $date->format('Y-m-d');
        } else {
            return '-';
        }
    }
}
