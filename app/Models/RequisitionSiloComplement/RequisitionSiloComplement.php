<?php

namespace App\Models\RequisitionSiloComplement;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequisitionSiloComplement extends Model
{
    use HasFactory;

    // fillable
    protected $fillable = ['tolva', 'use', 'manual_door', 'electric_door', 'aeration', 'conveyor_width', 'total_length', 'cupula', 'alero', 'croquis', 'requisition_silo_id'];

    //relations
    public function Requisition() {
        return $this->belongsTo('App\Models\RequisitionSilo\RequisitionSilo');
    }
}
