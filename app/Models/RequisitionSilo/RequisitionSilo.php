<?php

namespace App\Models\RequisitionSilo;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequisitionSilo extends Model
{
    use HasFactory;

    // fillable
    protected $fillable = ['quantity', 'product', 'density', 'voltage', 'seismic_zone', 'type', 'capacity', 'model', 'aeration_system', 'sweepers', 'capacity_sweepers', 'powersweep', 'powersweep_extension', 'thermometry_system', 'thermometry_system_type', 'requisition_id', 'silo_model_id', 'runway', 'comment', 'croquis'];

    //relations
    public function Requisition() {
        return $this->belongsTo('App\Models\Requisition\Requisition');
    }

    public function Accesories() {
        return $this->hasMany('App\Models\RequisitionSiloAccesory\RequisitionSiloAccesory');
    }

    public function Complements() {
        return $this->hasOne('App\Models\RequisitionSiloComplement\RequisitionSiloComplement');
    }

    public function SiloModel() {
        return $this->belongsTo('App\Models\SiloModel\SiloModel', 'silo_model_id');
    }
}
