<?php

namespace App\Models\RequisitionSiloAccesory;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequisitionSiloAccesory extends Model
{
    use HasFactory;

    // fillable
    protected $fillable = ['name', 'requisition_silo_id'];

    //relations
    public function Requisition() {
        return $this->belongsTo('App\Models\RequisitionSilo\RequisitionSilo');
    }
}
