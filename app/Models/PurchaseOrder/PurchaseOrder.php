<?php

namespace App\Models\PurchaseOrder;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class PurchaseOrder extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['date', 'reference', 'payment_conditions', 'shipping_conditions', 'quote_id', 'provider_id', 'uuid', 'customer_id', 'currency', 'approved', 'date_delivered', 'description', 'pdf', 'subtotal', 'total'];

    // set uuid in primary key
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    // relations
    public function Quote() {
        return $this->belongsTo('App\Models\Quote\Quote', 'quote_id');
    }

    public function Provider() {
        return $this->belongsTo('App\Models\Provider\Provider', 'provider_id');
    }

    public function PurchaseOrderItems() {
        return $this->hasMany('App\Models\PurchaseOrderItem\PurchaseOrderItem');
    }
}
