<?php

namespace App\Models\Requisition;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requisition extends Model
{
    use HasFactory;

    // fillable
    protected $fillable = ['title', 'country', 'reference', 'comments', 'total_silo', 'uuid', 'seismic_zone', 'selected_provider'];

    //relations
    public function Silos() {
        return $this->hasMany('App\Models\RequisitionSilo\RequisitionSilo');
    }

    public function RequisitionPdf() {
        return $this->hasMany('App\Models\RequisitionPdf\RequisitionPdf');
    }

    public function Providers() {
        return $this->belongsToMany('App\Models\Provider\Provider')->orderBy('id')->withPivot('id');
    }

    // scoope
    public function scopeDesc($query) {
        return $query->orderBy('id', 'DESC')->get();
    }

    public function scopeBetween($query, $start, $end) {
        return $query->whereBetween('created_at', [$start, $end]);
    }

    // seteamos el uuid como principal key
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
