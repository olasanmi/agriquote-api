<?php

namespace App\Models\QuoteItem;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use DB;

class QuoteItem extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['id', 'profit', 'cost', 'dealerProfit', 'position', 'price', 'quantity', 'total', 'weight', 'totalWeight', 'quote_id', 'item_id', 'tCurrency', 'subitemsTotal', 'costWithTax', 'show_title', 'show_title2', 'cost_type', 'oldCost'];

    // relations
    public function Quote() {
        return $this->belongsTo('App\Models\Quote\Quote', 'quote_id');
    }

    public function Item() {
        return $this->belongsTo('App\Models\Item\Item', 'item_id');
    }

    public function Attributes() {
        return $this->hasMany('App\Models\ItemQuoteAttribute\ItemQuoteAttribute');
    }

    public function Subitems() {
        return $this->hasMany('App\Models\ItemQuoteSubitem\ItemQuoteSubitem');
    }

    public function PurchaseItem() {
        return $this->hasOne('App\Models\PurchaseOrderItem\PurchaseOrderItem');
    }

    //functions
    public function getSubitemTotal() {
        $total = $daily = DB::table('item_quote_subitems')
        ->join('quote_items', 'quote_items.id', '=', 'item_quote_subitems.quote_item_id')
        ->select(DB::raw('item_quote_subitems.quantity * item_quote_subitems.costWithTax as totalCost'))
        ->where('quote_items.id', '=', $this->id)
        ->get();
        return $total->sum('totalCost');
    }

}
