<?php

namespace App\Models\Revision;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Revision extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['pdf', 'quote_id', 'total', 'next_email', 'email_interval', 'is_complete', 'email_pause', 'is_trash', 'user_status', 'comment', 'code', 'status', 'accept_terms'];

    // relations
    public function Quote() {
        return $this->belongsTo('App\Models\Quote\Quote', 'quote_id');
    }

    // scopes
    public function scopeDesc($query) {
        return $query->orderBy('id', 'DESC')->get();
    }
    
    public function scopeTrash($query) {
        return $query->where('is_trash', '=', true);
    }

    public function scopeNoTrash($query) {
        return $query->where('is_trash', '=', false);
    }

    public function scopeAvailable($query) {
        return $query->where('status', '=', true);
    }

    public function scopeNoComplete($query) {
        return $query->where('is_complete', '=', false);
    }

    public function getUserStatus() {
        switch ($this->user_status) {
            case 5:
                return 'El proyecto sufrira una modificación.';
                break;

            case 2:
                return 'Suspendido temporalmente.';
                break;

            case 3:
                return 'Se ha decidido por otro proveedor.';
                break;
            
            case 4:
                return 'Suspendido definitivamente.';
                break;
            
            case 7:
                return 'Estamos listos para negociar el proyecto.';
                break;

            case 6:
                return 'Esta en estudio. aun no se ha tomado decisión.';
                break;
            
            default:
                return 'Sin confirmar.';
                break;
        }
    }
    
    // last email send
    public function getLastEmailDate() {
        if ($this->Quote->user_status == 2) {
            if ($this->next_email) {
                $date = Carbon::createFromFormat('Y-m-d', $this->next_email)->subDays(20);
                return $date->format('Y-m-d');
            } else {
                return '-';
            }
        } else {
            if ($this->next_email) {
                $date = Carbon::createFromFormat('Y-m-d', $this->next_email)->subDays($this->email_interval);
                return $date->format('Y-m-d');
            } else {
                return '-';
            }
        }
    }
}
