<?php

namespace App\Models\PurchaseOrderItem;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class PurchaseOrderItem extends Model
{
    use HasFactory;
    
    // fillable
    protected $fillable = ['quote_number', 'discount', 'cost', 'total', 'quote_item_id', 'purchase_order_id', 'quantity', 'show', 'description'];

    // relations
    public function PurchaseOrder() {
        return $this->belongsTo('App\Models\PurchaseOrder\PurchaseOrder', 'purchase_order_id');
    }

    public function QuoteItem() {
        return $this->belongsTo('App\Models\QuoteItem\QuoteItem', 'quote_item_id');
    }
}
