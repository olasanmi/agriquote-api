<?php

namespace App\Models\QuoteProvider;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteProvider extends Model
{
    use HasFactory;
    // fillable
    protected $fillable = ['tCurrency', 'container40', 'container20', 'price40', 'price20', 'rate', 'provide_id', 'quote_id'];

    // relations
    public function Quote() {
        return $this->belongsTo('App\Models\Quote\Quote', 'quote_id');
    }

    public function Provider() {
        return $this->belongsTo('App\Models\Provider\Provider', 'provide_id');
    }
}
