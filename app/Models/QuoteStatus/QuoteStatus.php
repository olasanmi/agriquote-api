<?php

namespace App\Models\QuoteStatus;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteStatus extends Model
{
    use HasFactory;
    // fillable
    protected $fillable = ['name'];

    // relations
    public function Quotes() {
        return $this->hasMany('App\Models\Quote\Quote');
    }
}
