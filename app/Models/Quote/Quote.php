<?php

namespace App\Models\Quote;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use Carbon\Carbon;

class Quote extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['code', 'date_creation', 'date_validation', 'subtotal', 'discount', 'packaging_value', 'freight_value', 'load_containers', 'country', 'user_id', 'dealer_id', 'client_id', 'uuid', 'totalWeight', 'total', 'description', 'description_en', 'description_br', 'confirm_date', 'user_confirm_id', 'quote_statuse_id', 'is_confirm', 'blueprints', 'next_email', 'user_status', 'comment', 'pdf', 'first_email', 'accept_terms', 'last_page', 'last_page_en', 'last_page_br', 'email_count', 'is_complete', 'email_interval', 'email_pause', 'is_trash', 'email_contact_2', 'email_contact_3', 'lang'];

    // seteamos el uuid como principal key
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    // relations
    public function User() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function UserConfirm() {
        return $this->belongsTo('App\Models\User', 'user_confirm_id');
    }

    public function Client() {
        return $this->belongsTo('App\Models\Client\Client', 'client_id');
    }

    public function Dealer() {
        return $this->belongsTo('App\Models\Dealer\Dealer', 'dealer_id');
    }

    public function Items() {
        return $this->hasMany('App\Models\QuoteItem\QuoteItem')->orderBy('position', 'ASC');
    }

    public function Providers() {
        return $this->hasMany('App\Models\QuoteProvider\QuoteProvider');
    }

    public function Revisions() {
        return $this->hasMany('App\Models\Revision\Revision');
    }

    public function Status() {
        return $this->belongsTo('App\Models\QuoteStatus\QuoteStatus', 'quote_statuse_id');
    }

    public function PurchaseOrders() {
        return $this->hasMany('App\Models\PurchaseOrder\PurchaseOrder');
    }

    // scoope
    public function scopeDesc($query) {
        return $query->orderBy('created_at', 'DESC')->get();
    }

    public function scopeResponseImport($query) {
        return $query->orderBy('user_status', 'DESC');
    }

    public function scopeIsConfirm($query) {
        return $query->where('quote_statuse_id', '>=', 2);
    }

    public function scopeAcceptTerms($query) {
        return $query->where('accept_terms', '=', true);
    }

    public function scopeNoComplete($query) {
        return $query->where('is_complete', '=', false);
    }

    public function scopeTrash($query) {
        return $query->where('is_trash', '=', true);
    }

    public function scopeNoTrash($query) {
        return $query->where('is_trash', '=', false);
    }

    public function scopeBetween($query, $start, $end)
    {
        return $query->whereBetween('date_creation', [$start, $end]);
    }

    // models functions
    public function getUserStatus() {
        switch ($this->user_status) {
            case 5:
                return 'El proyecto sufrira una modificación.';
                break;

            case 2:
                return 'Suspendido temporalmente.';
                break;

            case 3:
                return 'Se ha decidido por otro proveedor.';
                break;
            
            case 4:
                return 'Suspendido definitivamente.';
                break;
            
            case 7:
                return 'Estamos listos para negociar el proyecto.';
                break;

            case 6:
                return 'Esta en estudio. aun no se ha tomado decisión.';
                break;
            
            default:
                return 'Sin confirmar.';
                break;
        }
    }

    // last email send
    public function getLastEmailDate() {
        if ($this->user_status == 2) {
            if ($this->next_email) {
                $date = Carbon::createFromFormat('Y-m-d', $this->next_email)->subDays(20);
                return $date->format('Y-m-d');
            } else {
                return '-';
            }
        } else {
            if ($this->next_email) {
                $date = Carbon::createFromFormat('Y-m-d', $this->next_email)->subDays($this->email_interval);
                return $date->format('Y-m-d');
            } else {
                return '-';
            }
        }
    }

}
