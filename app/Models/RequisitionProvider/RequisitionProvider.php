<?php

namespace App\Models\RequisitionProvider;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequisitionProvider extends Model
{
    use HasFactory;
    protected $table = 'provider_requisition';
    //fillable
    protected $fillable = ['provider_id', 'requisition_id'];
}
