<?php

namespace App\Models\Dealer;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Dealer extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['name', 'last_name', 'fullname', 'email', 'cellphone', 'phone', 'country', 'brand', 'uuid', 'country_label'];

    // seteamos el uuid como principal key
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    // relations
    public function Quotes() {
        return $this->hasMany('App\Models\Quote\Quote');
    }
}
