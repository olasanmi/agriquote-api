<?php

namespace App\Models\UserType;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    use HasFactory;
    protected $table = 'user_type';

    // relaciones
    public function User() {
        return $this->hasMany('App\Models\User');
    }
}
