<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Item extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['uuid', 'reference', 'maker', 'quote_number', 'name', 'name_en', 'image', 'description', 'provider_id', 'description_en', 'name_br', 'description_br'];

    // relations
    public function Attributes() {
        return $this->hasMany('App\Models\Attribute\Attribute');
    }

    public function Subitems() {
        return $this->hasMany('App\Models\Subitem\Subitem');
    }

    public function QuoteItems() {
        return $this->hasMany('App\Models\QuoteItem\QuoteItem');
    }

    public function Provider() {
        return $this->belongsTo('App\Models\Provider\Provider', 'provider_id');
    }

    // seteamos el uuid como principal key
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
