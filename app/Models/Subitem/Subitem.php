<?php

namespace App\Models\Subitem;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Subitem extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['name', 'name_en', 'name_br', 'item_id'];

    // relations
    public function Item() {
        return $this->belongsTo('App\Models\Item\Item');
    }

    public function ItemQuoteSubitems() {
        return $this->hasMany('App\Models\ItemQuoteSubitem\ItemQuoteSubitem', 'subitem_id');
    }
}
