<?php

namespace App\Models\ItemQuoteSubitem;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemQuoteSubitem extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['name', 'price', 'quantity', 'quote_item_id', 'subitem_id', 'costWithTax'];

    // relations
    public function Item() {
        return $this->belongsTo('App\Models\QuoteItem\QuoteItem', 'quote_item_id');
    }

    public function Subitem() {
        return $this->belongsTo('App\Models\Subitem\Subitem');
    }
}
