<?php

namespace App\Models\SiloModel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiloModel extends Model
{
    use HasFactory;

    // fillable
    protected $fillable = ['reference', 'diameter', 'cylinder_height', 'Total_height', 'volumen', 'type', 'provider_id'];

    // relations
    public function Provider() {
        return $this->belongsTo('App\Models\Provider\Provider');
    }

    public function ReqisitionSilos() {
        return $this->hasMany('App\Models\RequisitionSilo\RequisitionSilo');
    }
}
