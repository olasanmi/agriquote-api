<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use HasApiTokens, HasFactory, Notifiable;
    use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'user_type_id',
        'uuid',
        'image',
        'first_name',
        'last_name'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // seteamos el uuid como principal key
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    // relaciones
    public function UserType() {
        return $this->belongsTo('App\Models\UserType\UserType', 'user_type_id');
    }

    public function Quotes() {
        return $this->hasMany('App\Models\Quote\Quote');
    }

    public function QuoteConfirms() {
        return $this->hasMany('App\Models\Quote\Quote');
    }

    // scopes
    public function isAdmin() {
        return $this->UserType->name === 'admin' ? true : false;
    }

    public function scopeAdmins($query) {
        return $query->where('user_type_id', 3)->get();
    }
}
