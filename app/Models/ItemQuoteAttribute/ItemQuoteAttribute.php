<?php

namespace App\Models\ItemQuoteAttribute;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ItemQuoteAttribute extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['name', 'value', 'selected', 'quote_item_id', 'attribute_id'];

    // relations
    public function Item() {
        return $this->belongsTo('App\Models\QuoteItem\QuoteItem', 'quote_item_id');
    }

    public function Attribute() {
        return $this->belongsTo('App\Models\Attribute\Attribute', 'attribute_id');
    }
}
