<?php

namespace App\Models\Provider;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Provider extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['name', 'country', 'uuid', 'manager', 'address', 'phone', 'email'];

    // seteamos el uuid como principal key
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    // relations
    public function Items() {
        return $this->hasMany('App\Models\Item\Item');
    }

    public function SiloModels() {
        return $this->hasMany('App\Models\SiloModel\SiloModel');
    }

    public function QuoteProviders() {
        return $this->hasMany('App\Models\QuoteProvider\QuoteProvider');
    }

    public function PurchaseOrders() {
        return $this->hasMany('App\Models\PurchaseOrder\PurchaseOrder');
    }

    public function Requisitions() {
        return $this->belongsToMany('App\Models\Requisition\Requisition')->orderBy('id')->withPivot('id');
    }

    public function RequisitionPdf() {
        return $this->hasMany('App\Models\RequisitionPdf\RequisitionPdf');
    }
}
