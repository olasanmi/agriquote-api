<?php

namespace App\Models\Attribute;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Attribute extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    // fillable
    protected $fillable = ['variable', 'name', 'name_en', 'name_br', 'value', 'item_id'];

    // relations
    public function Item() {
        return $this->belongsTo('App\Models\Item\Item');
    }

    public function QuoteItems() {
        return $this->hasMany('App\Models\ItemQuoteAttribute\ItemQuoteAttribute');
    }
}
