<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'phone' => 'min:0|max:17',
            'cellphone' => 'required|min:9|max:17',
            'email' => 'required|email',
            'country' => 'required',
            'brand' => 'max:60'
        ];
    }
}
