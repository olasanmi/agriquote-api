<?php

namespace App\Http\Requests\Item;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:60',
            // 'name_en' => 'max:60',
            'reference' => 'required|max:120|unique:items',
            'maker' => 'max:60',
            'file' => 'file|max:1024'
        ];
    }
}
