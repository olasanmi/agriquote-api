<?php

namespace App\Http\Requests\Provider;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:60',
            'country' => 'required|max:5',
            'manager' => 'required|max:60',
            'address' => 'required|max:120',
            'phone' => 'required|max:20',
            'email' => 'required|max:90',
        ];
    }
}
