<?php

namespace App\Http\Requests\Requisition;

use Illuminate\Foundation\Http\FormRequest;

class RequisitionPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:120',
            'country' => 'required',
            'seismic_zone' => 'required',
            'total_silo' => 'required',
            'selected_provider' => 'required|exists:providers,id',
            'silos' => 'required|array|min:1',
            'silos.*.accesories' => 'array|min:0',
            'silos.*.aeration_system' => 'required',
            'silos.*.capacity' => 'required',
            'silos.*.density' => 'required',
            'silos.*.silo_model_id' => 'required',
            'silos.*.powersweep' => 'required',
            'silos.*.product' => 'required',
            'silos.*.quantity' => 'required',
            'silos.*.runway' => 'required',
            'silos.*.seismic_zone' => 'required',
            'silos.*.sweepers' => 'required',
            'silos.*.thermometry_system' => 'required',
            'silos.*.type' => 'required',
            'silos.*.voltage' => 'required'
        ];
    }
}
