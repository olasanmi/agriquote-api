<?php

namespace App\Http\Requests\Quote;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|max:20',
            'date_creation' => 'required|date',
            'date_validation' => 'required|date',
            'subtotal' => 'required',
            'discount' => '',
            'packaging_value' => 'required',
            'freight_value' => 'required',
            'dealer_id' => 'exists:dealers,id',
            'client_id' => 'required|exists:clients,id',
            'items' => 'required|array|min:1'
        ];
    }
}
