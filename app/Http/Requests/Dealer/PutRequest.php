<?php

namespace App\Http\Requests\Dealer;

use Illuminate\Foundation\Http\FormRequest;

class PutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'phone' => 'min:1|max:15',
            'cellphone' => 'required|min:9|max:15',
            'email' => 'required|email|unique:clients',
            'country' => 'required',
            'brand' => 'max:60'
        ];
    }
}
