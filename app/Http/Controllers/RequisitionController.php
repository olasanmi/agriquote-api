<?php

namespace App\Http\Controllers;

use App\Models\Requisition\Requisition;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
// request
use App\Http\Requests\Requisition\RequisitionPost;
use App\Http\Requests\Requisition\RequisitionSend;
// resources del modelo
use App\Http\Resources\Requisition\RequisitionCollection;
use App\Http\Resources\Requisition\RequisitionResource;
// use carbon
use Carbon\Carbon;
// use services
use App\Services\Requisition\RequisitionServices;
use Uuid;

class RequisitionController extends Controller
{
    public $start, $end, $requisitionServices;
    function __construct()
    {
        $this->start = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $this->end = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:00';
        $this->requisitionServices = new RequisitionServices();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            $start = $request->start ? $request->start . ' 00:00:00' : $this->start;
            $end = $request->end ? $request->end . ' 23:59:00' : $this->end;
            $quotes = Requisition::Between($start, $end)->Desc();
            return ResponseHelper::response('success', new RequisitionCollection($quotes), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequisitionPost $request)
    {
        //
        try {
            $requisition = Requisition::create(array_merge($request->all(), ['uuid' => Uuid::generate()->string]));
            if ($request->has('silos'))
                $this->requisitionServices->saveComplements($requisition, $request->silos);
            return ResponseHelper::response('success', 'Requisition created success', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function show(Requisition $requisition)
    {
        //
        try {
            return ResponseHelper::response('success', new RequisitionResource($requisition), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function edit(Requisition $requisition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function update(RequisitionPost $request, Requisition $requisition)
    {
        //
        try {
            if ($requisition->RequisitionPdf) {
                $requisition->RequisitionPdf()->delete();
            }
            $requisition->update($request->all());
            if ($request->has('silos'))
                $this->requisitionServices->updateComplements($requisition, $request->silos);
            return ResponseHelper::response('success', 'Requisition updated success', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Requisition $requisition)
    {
        //
        try {
            $requisition->delete();
            return ResponseHelper::response('success', 'Requisition delete success', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * send the specified email resource
     *
     * @param  \App\Models\Requisition\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function sendEmail(RequisitionSend $request)
    {
        //
        try {
            $requisition = Requisition::findOrFail($request->id);
            $message = $this->requisitionServices->sendRequisitionEmail($requisition);
            return ResponseHelper::response('success', new RequisitionResource($message['success']), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * generate requisition code
     */
    public function generateCode(Request $request)
    {
        //
        try {
            $code = $this->requisitionServices->generateReference($request->country);
            return ResponseHelper::response('success', $code, 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
