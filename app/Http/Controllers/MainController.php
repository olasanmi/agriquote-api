<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// import services
use App\Services\Main\MainServices;
// import helpers
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;

class MainController extends Controller
{
    // construct
    protected $mainServices;
    function __construct()
    {
        $this->mainServices = new MainServices();
    }

    //get main count
    public function mainCount(Request $request) {
        try {
            return ResponseHelper::response('success',  $this->mainServices->loadMainContability(), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
