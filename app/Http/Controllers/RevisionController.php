<?php

namespace App\Http\Controllers;

use App\Models\Revision\Revision;
use App\Models\Quote\Quote;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
// resources del modelo
use App\Http\Resources\Revision\RevisionCollection;
// services import
use App\Services\Quote\QuoteServices;
// use DB
use DB;
// use DB
use Carbon\Carbon;

class RevisionController extends Controller
{
    protected $quoteService;

    function __construct()
    {
        $this->quoteService = new QuoteServices();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $opportunities = Revision::NoComplete()->NoTrash()->Desc();
            return ResponseHelper::response('success', ['opportunities' => new RevisionCollection($opportunities), 'total' => $opportunities->sum('total')], 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Revision\Revision  $revision
     * @return \Illuminate\Http\Response
     */
    public function show(Revision $revision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Revision\Revision  $revision
     * @return \Illuminate\Http\Response
     */
    public function edit(Revision $revision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Revision\Revision  $revision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Revision $revision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Revision\Revision  $revision
     * @return \Illuminate\Http\Response
     */
    public function destroy(Revision $revision)
    {
        //
        try {
            $revision->delete();
            return ResponseHelper::response('success', 'The opportunity has been removed.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // complete opportunities
    public function completeOpportunities(Request $request) {
        DB::beginTransaction();
        try {
            // complete opportunities
            $revision = Revision::findOrFail($request->uuid);
            $revision->update(['is_complete' => true]);
            // complete the opportunities quote.
            $revision->Quote->update(['is_complete' => true]);
            // trash all other opportunities
            Revision::where('quote_id', '=', $revision->quote_id)->where('id', '!=',  $revision->id)->update(['is_trash' => true]);
            // generate all PO
            $this->quoteService->generatePo($revision->Quote);
            DB::commit();
            return ResponseHelper::response('success', 'Opportunity complete.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // get trashed
    public function listTrash(Request $request) {
        try {
            $revisions = Revision::Trash()->Desc();
            return ResponseHelper::response('success', new RevisionCollection($revisions), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // restore trashed
    public function restoreTrash(Request $request) {
        DB::beginTransaction();
        try {
            $revision = Revision::findOrFail($request->quote_id);
            $revision->update(['is_trash' => false, 'is_complete' => false]);
            DB::commit();
            return ResponseHelper::response('success', 'Opportunity restore.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // set trash a opportunities
    public function deleteOportunities(Request $request) {
        DB::beginTransaction();
        try {
            $revision = Revision::findOrFail($request->uuid);
            $revision->update(['is_trash' => true, 'is_complete' => false]);
            DB::commit();
            return ResponseHelper::response('success', 'Opportunity removed.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // pause email
    public function emailPause(Request $request) {
        DB::beginTransaction();
        try {
            $revision = Revision::find($request->quote_id);
            $revision->update(['email_pause' => $request->pause]);
            if (!$revision->email_pause) {
                $now = Carbon::now();
                if (Carbon::parse($revision->next_email)->isPast()) {
                    $newDate = $now->addDays(1);
                    $revision->update(['next_email' => $newDate->format('Y-m-d')]);
                } elseif (!$revision->next_email) {
                    $newDate = $now->addDays(1);
                    $revision->update(['next_email' => $newDate->format('Y-m-d')]);
                }
            }
            DB::commit();
            return ResponseHelper::response('success', 'Opportunity email...', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // change  quote email interval
    public function changeEmailInterval(Request $request) {
        DB::beginTransaction();
        try {
            $revision = Revision::find( $request->quote_id);
            $revision->update(['email_interval' => $request->day]);
            DB::commit();
            return ResponseHelper::response('success', 'Interval updated.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // send email
    public function sendEmail(Request $request) {
        DB::beginTransaction();
        try {
            // send email
            $revision = Revision::find($request->id);
            $revision->Quote->update(['pdf' => $revision->pdf]);
            $this->quoteService->sendNewEmailOportunities(Quote::find($revision->Quote->id), $request->all(), $revision);
            DB::commit();
            return ResponseHelper::response('success', 'mail send correctly', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
