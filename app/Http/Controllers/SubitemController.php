<?php

namespace App\Http\Controllers;

use App\Models\Subitem\Subitem;
use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;

class SubitemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subitem\Subitem  $subitem
     * @return \Illuminate\Http\Response
     */
    public function show(Subitem $subitem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subitem\Subitem  $subitem
     * @return \Illuminate\Http\Response
     */
    public function edit(Subitem $subitem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subitem\Subitem  $subitem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subitem $subitem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subitem\Subitem  $subitem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subitem $subitem)
    {
        //
        try {
            $subitem->delete();
            return ResponseHelper::response('success', 'The accessory has been removed.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
