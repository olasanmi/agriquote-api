<?php

namespace App\Http\Controllers;
ini_set('max_execution_time', '5000000000000');

use App\Models\Quote\Quote;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
// validaciones de request
use App\Http\Requests\Quote\PostRequest;
use App\Http\Requests\Quote\ReferenceRequest;
use App\Http\Requests\Quote\StatusRequest;
use App\Http\Requests\Quote\BluePrintRequest;
// resources del modelo
use App\Http\Resources\Quote\QuoteCollection;
use App\Http\Resources\Quote\QuoteOportunities;
use App\Http\Resources\Quote\QuoteResource;
use App\Http\Resources\QuoteItem\QuoteItemCollection;
// services import
use App\Services\Quote\QuoteServices;
// use db traith
use DB;
// use carbon
use Carbon\Carbon;
// use File class
use File;

class QuoteController extends Controller
{
    // construct
    protected $quoteService;
    public $start, $end;
    function __construct()
    {
        $this->quoteService = new QuoteServices();
        $this->start = Carbon::now()->startOfMonth()->format('Y-m-d') . ' 00:00:00';
        $this->end = Carbon::now()->endOfMonth()->format('Y-m-d') . ' 23:59:00';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            $start = $request->start ? $request->start . ' 00:00:00' : $this->start;
            $end = $request->end ? $request->end . ' 23:59:00' : $this->end;
            if ($request->user()->isAdmin())
                $quotes = Quote::Between($start, $end)->NoTrash()->Desc();
            else
                $quotes = Quote::Between($start, $end)->NoTrash()->where('user_id', $request->user()->id)->get();
            return ResponseHelper::response('success', new QuoteCollection($quotes), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        //
        DB::beginTransaction();
        try {
            $quote = Quote::create(array_merge($request->all(), ['user_id' => $request->user()->id]));
            $quote->update(['user_id' => $request->user()->id]);
            if ($request->has('items'))
                $this->quoteService->createItems($quote, $request->items);
            if ($request->has('providers'))
                $this->quoteService->createQuoteProviders($quote, $request->providers);
            DB::commit();
            return ResponseHelper::response('success', $quote->uuid, 201);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function show(Quote $quote)
    {
        //
        try {
            return ResponseHelper::response('success', new QuoteResource($quote), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function edit(Quote $quote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quote\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote)
    {
        //
        DB::beginTransaction();
        try {
            $quote->update($request->all());
            if ($quote->quote_statuse_id === 3) {
                // $revision = $this->quoteService->setQuoteRevision($quote);
                $code = $this->quoteService->upRevisinNumber($quote);
                $quote->update(['code' => $code, 'quote_statuse_id' => 2]);
            }
            if ($request->items)
                $this->quoteService->updateQuoteItem($quote, $request->items);
            if ($request->has('providers'))
                $this->quoteService->createQuoteProviders($quote, $request->providers);
            DB::commit();
            return ResponseHelper::response('success', 'The quote has been modified.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quote\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quote $quote)
    {
        //
        try {
            $quote->delete();
            return ResponseHelper::response('success', 'The quote has been removed.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // generate reference
    public function generateReference(ReferenceRequest $request) {
        try {
            return ResponseHelper::response('success', ['code' => $this->quoteService->generateReference($request->client_id)], 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // change status
    public function changeStatus(StatusRequest $request) {
        DB::beginTransaction();
        try {
            $status = $this->quoteService->changeStatus(Quote::find($request->id), $request->all(), $request->user()->id);
            DB::commit();
            return ResponseHelper::response('success', $status, 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // upload blueprints
    public function uploadBluePrint(BluePrintRequest $request) {
        try {
            $quote = Quote::find($request->id);
            if ($request->file) {
                $newFile = $request->file->getClientOriginalName();
                $oldfile = public_path('images/blueprints/' . $newFile);
                if (File::exists($oldfile))
                    return ResponseHelper::response('error', 'This file isset in our server.', 400);
                if ($quote->blueprints)
                    MainHelper::deleteFile($quote->blueprints, 'blueprints');
                $path = MainHelper::uploadFile($request->file, 'blueprints');
                $quote->update(['blueprints' => $path]);
                return ResponseHelper::response('success', new QuoteResource($quote), 200);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // client filter
    public function filter($uuid)
    {
        //
        try {
            $quote = Quote::where('uuid', $uuid)->first();
            return ResponseHelper::response('success', new QuoteResource($quote), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // client comment
    public function clientComment(Request $request)
    {
        //
        try {
            $quote = Quote::where('uuid', $request->uuid)->first();
            $quote->update($request->all());
            $this->quoteService->commentRevisionForUser($quote);
            $this->quoteService->sendAdminResponseEmail($quote);
            return ResponseHelper::response('success', 'Quote client response save.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // get quotes with opportunities
    public function filterQuoteHaveOpportunities(Request $request) {
        try {
            $opportunities = Quote::ResponseImport()->NoComplete()->IsConfirm()->where('first_email', true)->get();
            return ResponseHelper::response('success', ['opportunities' => new QuoteOportunities($opportunities), 'total' => $opportunities->sum('total')], 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // generate quote pdf
    public function showPdf(Request $request) {
        try {
            $quote = Quote::where('uuid', $request->uuid)->first();
            $arr = array_chunk(json_decode($quote->Items, true), 20);
            $quote->newItems = $arr;
            $pdf = $this->quoteService->generatePdf($quote);
            return ResponseHelper::response('success', new QuoteResource(Quote::find($quote->id)), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // acept terms quote pdf
    public function acceptTermsQuotePdf(Request $request) {
        DB::beginTransaction();
        try {
            $quote = Quote::where('uuid', $request->uuid)->first();
            $quote->update($request->all());
            $this->quoteService->acceptRevisionTerms($quote->id);
            DB::commit();
            return ResponseHelper::response('success', new QuoteResource($quote), 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // show by refs
    public function getByRefs(Request $request) {
        try {
            $quote = Quote::where('code', $request->code)->first();
            if ($quote) {
                return ResponseHelper::response('success', new QuoteResource($quote), 200);
            } else {
                return ResponseHelper::response('error', 'Code don`t match with any quote.', 400);
            }
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // acept terms quote pdf
    public function clearBluePrints(Request $request) {
        try {
            $quote = Quote::where('uuid', $request->uuid)->first();
            $quote->update(['blueprints' => '']);
            return ResponseHelper::response('success', new QuoteResource($quote), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // complete quote pdf
    public function completeQuote(Request $request) {
        DB::beginTransaction();
        try {
            $quote = Quote::where('uuid', $request->uuid)->first();
            $quote->update(['is_complete' => true]);
            $this->quoteService->generatePo($quote);
            DB::commit();
            return ResponseHelper::response('success', new QuoteResource($quote), 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // delete quote oportunities
    public function deleteOportunities(Request $request) {
        DB::beginTransaction();
        try {
            $quote = Quote::where('uuid', $request->uuid)->first();
            $quote->is_complete = true;
            $quote->is_trash = true;
            $quote->save();
            DB::commit();
            return ResponseHelper::response('success', 'Opportunity removed.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // change  quote email interval
    public function changeEmailInterval(Request $request) {
        DB::beginTransaction();
        try {
            $quote = Quote::where('uuid', $request->quote_id)->first();
            $quote->update(['email_interval' => $request->day]);
            DB::commit();
            return ResponseHelper::response('success', 'Interval updated.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // pause email
    public function emailPause(Request $request) {
        DB::beginTransaction();
        try {
            $quote = Quote::where('uuid', $request->quote_id)->first();
            $quote->update(['email_pause' => $request->pause]);
            if (!$quote->email_pause) {
                $now = Carbon::now();
                if (Carbon::parse($quote->next_email)->isPast()) {
                    $newDate = $now->addDays(1);
                    $quote->update(['next_email' => $newDate->format('Y-m-d')]);
                }
            }
            DB::commit();
            return ResponseHelper::response('success', 'Quote email ', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // get trashed
    public function listTrash(Request $request) {
        try {
            if ($request->user()->isAdmin())
                $quotes = Quote::Trash()->Desc();
            else
                $quotes = Quote::Trash()->where('user_id', $request->user()->id)->get();
            return ResponseHelper::response('success', new QuoteCollection($quotes), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // change  quote email interval
    public function restoreTrash(Request $request) {
        DB::beginTransaction();
        try {
            $quote = Quote::where('uuid', $request->quote_id)->first();
            $quote->update(['is_trash' => false, 'is_complete' => false]);
            DB::commit();
            return ResponseHelper::response('success', 'Quote restore.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
