<?php

namespace App\Http\Controllers;

use App\Models\RequisitionSilo\RequisitionSilo;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
use App\Http\Requests\RequisitionSilo\UploadCroquis;

class RequisitionSiloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequisitionSilo  $requisitionSilo
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionSilo $requisitionSilo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequisitionSilo  $requisitionSilo
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionSilo $requisitionSilo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequisitionSilo  $requisitionSilo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionSilo $requisitionSilo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequisitionSilo  $requisitionSilo
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionSilo $requisitionSilo)
    {
        //
        try {
            $requisitionSilo->delete();
            return ResponseHelper::response('success', 'Requisition silo delete success', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // upload croquis
    public function uploadCroquis(UploadCroquis $request) {
        try {
            if ($request->oldCroquis and !is_null($request->oldCroquis))
                MainHelper::deleteFile($request->oldCroquis, 'croquis');
            $path = MainHelper::uploadFile($request->croquis, 'croquis');
            return ResponseHelper::response('success', ['croquis' => url('images/croquis', $path), 'name' => $path], 200); 
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
