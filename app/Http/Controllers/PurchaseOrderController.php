<?php

namespace App\Http\Controllers;

use App\Models\PurchaseOrder\PurchaseOrder;
use Illuminate\Http\Request;
// response helper
use App\Helpers\ResponseHelper;
// services
use App\Services\PurchaseOrder\PurchaseOrderServices;
// resources
use App\Http\Resources\PurchaseOrder\PurchaseOrderCollection;
use App\Http\Resources\PurchaseOrder\PurchaseOrderResource;
// validaciones de request
use App\Http\Requests\PurchaseOrder\PurchaseOrderRequest;
// use DB
use DB;

class PurchaseOrderController extends Controller
{
    private $purchaseOrderService;

    function __construct()
    {
        $this->purchaseOrderService = new PurchaseOrderServices();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $purchaseOrders = PurchaseOrder::orderBy('id', 'DESC')
            ->get();
            return ResponseHelper::response('success', new PurchaseOrderCollection($purchaseOrders), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurchaseOrderRequest $request)
    {
        //
        try {
            // validate po with quote and provider id
            $isset = PurchaseOrder::where('quote_id', '=', $request->quote_id)->where('provider_id', '=', $request->provider_id)->first();
            if ($isset)
                return ResponseHelper::response('error', 'A purchase order exists for this order with this provider', 400);
            // save po
            $purchaseOrder = PurchaseOrder::create($request->all());
            // save po items
            $this->purchaseOrderService->saveItems($purchaseOrder, $request->creationForm, $request->items);
            return ResponseHelper::response('success', 'The purchase order has been saved.', 201);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PurchaseOrder\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder $purchaseOrder)
    {
        //
        try {
            return ResponseHelper::response('success', new PurchaseOrderResource($purchaseOrder), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PurchaseOrder\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $purchaseOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PurchaseOrder\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(PurchaseOrderRequest $request, PurchaseOrder $purchaseOrder)
    {
        //
        try {
            $purchaseOrder = $purchaseOrder->update($request->all());
            // update po items
            $this->purchaseOrderService->itemDataUpdate($request->items);
            return ResponseHelper::response('success', 'The purchase order has been updated.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PurchaseOrder\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $purchaseOrder)
    {
        //
        try {
            $purchaseOrder->delete();
            return ResponseHelper::response('success', 'The purchase order has been deleted.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // generate pdf
    public function showPdf(PurchaseOrder $purchaseOrder) {
        try {
            $pdf = $this->purchaseOrderService->generatePdf($purchaseOrder);
            return ResponseHelper::response('success', new PurchaseOrderResource($purchaseOrder), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // send pdf
    public function sendEmailPdf(PurchaseOrder $purchaseOrder) {
        try {
            $pdf = $this->purchaseOrderService->sendEmailPdf($purchaseOrder);
            return ResponseHelper::response('success', 'Purchase order send success.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
