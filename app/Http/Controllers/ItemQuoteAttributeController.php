<?php

namespace App\Http\Controllers;

use App\Models\ItemQuoteAttribute;
use Illuminate\Http\Request;

class ItemQuoteAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ItemQuoteAttribute  $itemQuoteAttribute
     * @return \Illuminate\Http\Response
     */
    public function show(ItemQuoteAttribute $itemQuoteAttribute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ItemQuoteAttribute  $itemQuoteAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemQuoteAttribute $itemQuoteAttribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ItemQuoteAttribute  $itemQuoteAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemQuoteAttribute $itemQuoteAttribute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ItemQuoteAttribute  $itemQuoteAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemQuoteAttribute $itemQuoteAttribute)
    {
        //
    }
}
