<?php

namespace App\Http\Controllers;

use App\Models\RequisitionPdf\RequisitionPdf;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
use App\Http\Resources\RequisitionPdf\RequisitionPdfCollection;
// use services
use App\Services\Requisition\RequisitionServices;
use Carbon\Carbon;
use DB;

class RequisitionPdfController extends Controller
{
    public $requisitionServices, $now;

    function __construct()
    {
        $this->requisitionServices = new RequisitionServices();
        $this->now = Carbon::now();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            $followings = RequisitionPdf::where('next_email', '!=', null)
            ->where('is_complete', false)
            ->get();
            return ResponseHelper::response('success', new RequisitionPdfCollection($followings), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequisitionPdf  $requisitionPdf
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionPdf $requisitionPdf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequisitionPdf  $requisitionPdf
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionPdf $requisitionPdf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequisitionPdf  $requisitionPdf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionPdf $requisitionPdf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequisitionPdf  $requisitionPdf
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionPdf $requisitionPdf)
    {
        //
    }

    /**
     * Send requisition email
     *
     */
    public function sendFirstEmail(Request $request) {
        DB::beginTransaction();
        try {
            $requisitionPdf = RequisitionPdf::find($request->id);
            if ($request->has('all') and $request->all == true) {
            } else {
                $code = $this->requisitionServices->sendFirstEmail($requisitionPdf);
            }
            DB::commit();
            return ResponseHelper::response('success', 'Email send success.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // complete requisition tracks
    public function completeRequisition(Request $request) {
        DB::beginTransaction();
        try {
            // complete opportunities
            $track = RequisitionPdf::findOrFail($request->id);
            $track->update(['is_complete' => true]);
            DB::commit();
            return ResponseHelper::response('success', 'Requisition complete.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // pause email
    public function emailPause(Request $request) {
        DB::beginTransaction();
        try {
            $track = RequisitionPdf::findOrFail($request->id);
            $track->update(['is_pause' => $request->pause]);
            if (!$track->is_pause) {
                $now = Carbon::now();
                if (Carbon::parse($track->next_email)->isPast()) {
                    $newDate = $now->addDays(1);
                    $track->update(['next_email' => $newDate->format('Y-m-d')]);
                }
            }
            DB::commit();
            return ResponseHelper::response('success', 'Opportunity email...', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
