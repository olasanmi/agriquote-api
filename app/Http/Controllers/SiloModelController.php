<?php

namespace App\Http\Controllers;

use App\Imports\SiloModelImport;
use App\Imports\SimagaImport;
use App\Imports\SukupImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\SiloModel\SiloModel;
use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;
// use carbon
use Carbon\Carbon;
use Log;

class SiloModelController extends Controller
{   
    public $providerArray, $suggestion;
    function __construct()
    {
        $this->providerArray = [15, 27, 16];
        $this->suggestion = array();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rows = Excel::import(new SukupImport, public_path('imports/sukup.xlsx'));
        return ResponseHelper::response('success', 'Importacion correcta.', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SiloModel\SiloModel  $siloModel
     * @return \Illuminate\Http\Response
     */
    public function show(SiloModel $siloModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SiloModel\SiloModel  $siloModel
     * @return \Illuminate\Http\Response
     */
    public function edit(SiloModel $siloModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SiloModel\SiloModel  $siloModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiloModel $siloModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SiloModel\SiloModel  $siloModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiloModel $siloModel)
    {
        //
    }

    public function fulterByBolumen(Request $request) {
        try {
           foreach ($this->providerArray as $prov => $provider) {
               $silos = $this->filterSilosUp($provider, $request->main);
               if (!is_numeric($silos)) {
                   array_push($this->suggestion, $silos);
               }
           }
           foreach ($this->providerArray as $prov => $provider) {
               $silos = $this->filterSilosDown($provider, $request->main);
               if (!is_numeric($silos)) {
                   array_push($this->suggestion, $silos);
               }
           }
           return ResponseHelper::response('success', $this->suggestion, 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    public function filterSilosUp($provider, $main) {
        $silos = null;
        $startUp = $main;
        do {
            $silos = SiloModel::whereBetween('volumen', [$main, $startUp])
            ->where('provider_id', $provider)
            ->with('Provider')
            ->first();
            $startUp += 5;
            if (($main + 500) < $startUp) {
                $silos = 1;
            }
        } while (!$silos);
        return $silos;
    }

    public function filterSilosDown($provider, $main) {
        $silos = null;
        $startDown = $main;
        do {
            $silos = SiloModel::whereBetween('volumen', [$startDown, $main])
            ->where('provider_id', $provider)
            ->with('Provider')
            ->first();
            $startDown = $startDown - 5;
            if (($main - 500) > $startDown) {
                $silos = 1;
            }
        } while (!$silos);
        return $silos;
    }
}
