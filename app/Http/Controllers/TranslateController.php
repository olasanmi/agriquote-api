<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ResponseHelper;

class TranslateController extends Controller
{
    //
    public function translate(Request $request) {
        try {
            $translate = $this->getTranslate($request->text, $request->source, $request->target);
            return ResponseHelper::response('success', $translate, 200);
        } catch (\Throwable $e) {
            return ResponseHelper::response('error', $e->getMessage(), 422);
        }
    }

    function getTranslate($q, $sl, $tl){
        $res= file_get_contents("https://translate.googleapis.com/translate_a/single?client=gtx&ie=UTF-8&oe=UTF-8&dt=bd&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=t&dt=at&sl=".$sl."&tl=".$tl."&hl=hl&q=".urlencode($q));
        $res=json_decode($res);
        $text = '';
        foreach ($res[0] as $key => $value) {
            $text .= $value[0];
        }
        return $text;
    }
}
