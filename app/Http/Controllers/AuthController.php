<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// modelos importe
use App\Models\User;
// helpers importe
use App\Helpers\ResponseHelper;
// importe de respuesta
use Response;
// validaciones de request
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RecoveryRequest;
// resources
use App\Http\Resources\User\UserResource;
// email
use App\Mail\User\ChangePassword;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    // iniciamos sesion y obtenemos el token
    public function login(LoginRequest $request) {
        try {
            // verificamos si existe un usuario con el nombre de usuario
            $user = User::where('email', $request->email)->first();
            // si existe el usuario procedemos a crear el token
            if (isset($user->id)) {
                if (Hash::check($request->password, $user->password)) {
                    $data['token'] = $user->createToken($user->email)->plainTextToken;
                    $user->token = $data['token'];
                    return ResponseHelper::response('success', new UserResource($user), 200);
                } else {
                    // si ingresa la contraseña equivocada retornamos
                    return ResponseHelper::response('error', 'The password entered is incorrect', 400);
                }
            } else {
                // retornamos al usuario si ingresa un username que no existe.
                return ResponseHelper::response('error', 'The email entered does not exist', 404);
            }
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // logout
    public function logout(Request $request) {
        try {
            // saco el usuario logueado
            $user = request()->user();
            // saco el token del usuario para cancelarlo
            $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
            return ResponseHelper::response('success', 'Logged out successfully.', 200);

        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // recovery
    public function recovery(RecoveryRequest $request) {
        try {
            // saco el usuario por su email
            $user = User::where('email', $request->email)->first();
            if (isset($user->id)) {
                Mail::to($user->email)->send(new ChangePassword($user));
                return ResponseHelper::response('success', 'An email has been sent with the instructions to recover your password.', 200);
            } else {
                return ResponseHelper::response('error', 'The email entered does not exist.', 400);
            }

        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
