<?php

namespace App\Http\Controllers;

use App\Models\Dealer\Dealer;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
// validaciones de request
use App\Http\Requests\Dealer\PostRequest;
use App\Http\Requests\Dealer\PutRequest;
// resources del modelo
use App\Http\Resources\Dealer\DealerCollection;
use App\Http\Resources\Dealer\DealerResources;
// import services
use App\Services\Dealer\DealerServices;

class DealerController extends Controller
{
    protected $dealerService;
    function __construct()
    {
        $this->dealerService = new DealerServices();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $dealers = Dealer::all();
            return ResponseHelper::response('success', new DealerCollection($dealers), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $dealer = Dealer::create($request->all());
            return ResponseHelper::response('success', 'The dealer has been saved.', 201);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function show(Dealer $dealer)
    {
        //
        try {
            return ResponseHelper::response('success', new DealerResources($dealer), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function edit(Dealer $dealer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dealer $dealer)
    {
        //
        try {
            $dealer->update($request->all());
            return ResponseHelper::response('success', 'The dealer has been modified.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dealer\Dealer  $dealer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dealer $dealer)
    {
        //
        try {
            $dealer->delete();
            return ResponseHelper::response('success', 'The dealer has been removed.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // filtramos el cliente
    public function search(Request $request) {
        try {
            return ResponseHelper::response('success', $this->dealerService->searchDealer($request->name), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
