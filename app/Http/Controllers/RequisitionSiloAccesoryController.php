<?php

namespace App\Http\Controllers;

use App\Models\RequisitionSiloAccesory;
use Illuminate\Http\Request;

class RequisitionSiloAccesoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequisitionSiloAccesory  $requisitionSiloAccesory
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionSiloAccesory $requisitionSiloAccesory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequisitionSiloAccesory  $requisitionSiloAccesory
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionSiloAccesory $requisitionSiloAccesory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequisitionSiloAccesory  $requisitionSiloAccesory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionSiloAccesory $requisitionSiloAccesory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequisitionSiloAccesory  $requisitionSiloAccesory
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionSiloAccesory $requisitionSiloAccesory)
    {
        //
    }
}
