<?php

namespace App\Http\Controllers;

use App\Models\RequisitionProvider;
use Illuminate\Http\Request;

class RequisitionProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequisitionProvider  $requisitionProvider
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionProvider $requisitionProvider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequisitionProvider  $requisitionProvider
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionProvider $requisitionProvider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequisitionProvider  $requisitionProvider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionProvider $requisitionProvider)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequisitionProvider  $requisitionProvider
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionProvider $requisitionProvider)
    {
        //
    }
}
