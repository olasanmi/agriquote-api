<?php

namespace App\Http\Controllers;

use App\Models\Item\Item;
use App\Models\Subitem\Subitem;
use App\Models\Attribute\Attribute;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
// validaciones de request
use App\Http\Requests\Item\PostRequest;
use App\Http\Requests\Item\PutRequest;
// resources del modelo
use App\Http\Resources\Item\ItemCollection;
use App\Http\Resources\Item\ItemResource;
// import services
use App\Services\Item\ItemServices;

class ItemController extends Controller
{
    protected $itemService;
    function __construct()
    {
        $this->itemService = new ItemServices();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $items = Item::orderBy('id', 'DESC')
            ->paginate(400);
            $data['items'] = new ItemCollection($items);
            $data['last_page'] = $items->lastPage();
            return ResponseHelper::response('success', $data, 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        //
        try {
            $item = Item::create($request->all());
            if ($request->has('file')) {
                if ($item->image)
                    MainHelper::deleteFile($item->image, 'items');
                $path = MainHelper::uploadFile($request->file, 'items');
                $item->update(['image' => $path]);
            }
            // proccess attributes
            if ($request->has('attributesAll')) {
                foreach (json_decode($request->attributesAll, true) as $key => $value) {
                    Attribute::create([
                        'name' => $value['name'],
                        'name_en' => $value['name_en'],
                        'name_br' => $value['name_br'],
                        'value' => $value['value'],
                        'variable' => $value['variable'],
                        'item_id' => $item->id
                    ]);
                }
            }
            if ($request->has('subitems')) {
                foreach (json_decode($request->subitems, true) as $key => $value) {
                    Subitem::create([
                        'name' => $value['name'],
                        'name_en' => $value['name_en'],
                        'name_br' => $value['name_br'],
                        'item_id' => $item->id
                    ]);
                }
            }
            return ResponseHelper::response('success', 'The item has been saved.', 201);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
        try {
            return ResponseHelper::response('success', new ItemResource($item), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
        try {
            $item->update($request->all());
            if ($request->has('file')) {
                $path = MainHelper::uploadFile($request->file, 'items');
                $item->update(['image' => $path]);
            }
            foreach (json_decode($request->attributesAll, true) as $key => $value) {
                if (isset($value['id'])) {
                    $attribute = Attribute::find($value['id']);
                    $attribute->update([
                        'name' => $value['name'],
                        'name_en' => $value['name_en'],
                        'name_br' => $value['name_br'],
                        'value' => $value['value'],
                        'variable' => $value['variable']
                    ]);
                } else {
                    Attribute::create([
                        'name' => $value['name'],
                        'name_en' => $value['name_en'],
                        'name_br' => $value['name_br'],
                        'value' => $value['value'],
                        'variable' => $value['variable'],
                        'item_id' => $item->id
                    ]);
                }
            }
            foreach (json_decode($request->subitems, true) as $key => $value) {
                if (isset($value['id'])) {
                    $subtitem = Subitem::find($value['id']);
                    $subtitem->update(['name' => $value['name'], 'name_en' => $value['name_en'], 'name_br' => $value['name_br']]);
                } else {
                    Subitem::create([
                        'name' => $value['name'],
                        'name_en' => $value['name_en'],
                        'name_br' => $value['name_br'],
                        'item_id' => $item->id
                    ]);
                }
            }
            return ResponseHelper::response('success', 'The item has been modified.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
        try {
            if ($item->image)
                MainHelper::deleteFile($item->image, 'items');
                $item->delete();
                return ResponseHelper::response('success', 'The item has been removed.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }


    // filtramos el item
    public function search(Request $request) {
        try {
            return ResponseHelper::response('success', new ItemCollection($this->itemService->searchItem($request->name)), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }
}
