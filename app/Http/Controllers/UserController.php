<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
// importamos el helper de respuestas
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
// resources
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
// request validadores
use App\Http\Requests\User\PostRequest;
use App\Http\Requests\User\PutRequest;
use App\Http\Requests\User\ImageRequest;
// importamos db traith
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            // sacamos todos los usuarios.
            return ResponseHelper::response('success', new UserCollection(User::all()), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        //
        DB::beginTransaction();
        try {
            // creamos un nuevo usuario en la base de datos.
            $user = User::create(array_merge($request->all(), ['password' => Hash::make($request->password)]));
            DB::commit();
            return ResponseHelper::response('success', 'User created successfully.', 200);
        } catch (\Exception $e) {
            DB::rollback();
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        try {
            // filtramos el usuario en la bbdd
            return ResponseHelper::response('success', $user, 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(PutRequest $request, User $user)
    {
        //
        try {
            // creamos un nuevo usuario en la base de datos.
            $user->update(array_merge($request->all(), ['password' => Hash::make($request->password)]));
            if ($request->has('profile') and $request->profile == true)
                return ResponseHelper::response('user', new UserResource($user), 200);
            return ResponseHelper::response('success', 'User updated successfully.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        try {
            // creamos un nuevo usuario en la base de datos.
            if ($user->image != 'default-profile')
                MainHelper::deleteFile($user->image, 'profile');
            $user->delete();
            return ResponseHelper::response('success', 'User deleted successfully.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // confirmar contraseña de los nuevos usuarios
    public function confirm(Request $request)
    {
        //
        try {
            $user = User::where('uuid', '=', $request->uuid)->first();
            $user->update(['password' => Hash::make($request->password)]);
            return ResponseHelper::response('success', 'User password set successfully.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // upload image
    public function loadProfileImage(ImageRequest $request) {
        try {
            // obtenemos el usuario logueado
            $user = User::findOrFail($request->user()->id);
            if ($user->image != 'default-profile')
                MainHelper::deleteFile($user->image, 'profile');
            $path = MainHelper::uploadFile($request->file, 'profile');
            $user->update(['image' => $path]);
            return ResponseHelper::response('user', new UserResource($user), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

}
