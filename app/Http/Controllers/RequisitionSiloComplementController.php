<?php

namespace App\Http\Controllers;

use App\Models\RequisitionSiloComplement;
use Illuminate\Http\Request;

class RequisitionSiloComplementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RequisitionSiloComplement  $requisitionSiloComplement
     * @return \Illuminate\Http\Response
     */
    public function show(RequisitionSiloComplement $requisitionSiloComplement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RequisitionSiloComplement  $requisitionSiloComplement
     * @return \Illuminate\Http\Response
     */
    public function edit(RequisitionSiloComplement $requisitionSiloComplement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RequisitionSiloComplement  $requisitionSiloComplement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RequisitionSiloComplement $requisitionSiloComplement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RequisitionSiloComplement  $requisitionSiloComplement
     * @return \Illuminate\Http\Response
     */
    public function destroy(RequisitionSiloComplement $requisitionSiloComplement)
    {
        //
    }
}
