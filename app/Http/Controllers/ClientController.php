<?php

namespace App\Http\Controllers;

// modelos importe
use App\Models\Client\Client;
use Illuminate\Http\Request;
// helpers importe
use App\Helpers\ResponseHelper;
use App\Helpers\MainHelper;
// validaciones de request
use App\Http\Requests\Client\PostRequest;
use App\Http\Requests\Client\PutRequest;
// resources del modelo
use App\Http\Resources\Client\ClientCollection;
use App\Http\Resources\Client\ClientResource;
// import services
use App\Services\Client\ClientServices;
use App\Services\NetPhone\NetPhoneServices;

class ClientController extends Controller
{
    protected $clientService, $net2phone;
    function __construct()
    {
        $this->clientService = new ClientServices();
        $this->net2phone = new NetPhoneServices();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $clients = Client::all();
            return ResponseHelper::response('success', new ClientCollection($clients), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        //
        try {
            $client = Client::create($request->all());
            return ResponseHelper::response('success', 'The client has been saved.', 201);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
        try {
            return ResponseHelper::response('success', new ClientResource($client), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Client\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(PutRequest $request, Client $client)
    {
        //
        try {
            $client->update($request->all());
            return ResponseHelper::response('success', 'The client has been modified.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Client\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
        try {
            $client->delete();
            return ResponseHelper::response('success', 'The client has been removed.', 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // filtramos el cliente
    public function search(Request $request) {
        try {
            return ResponseHelper::response('success', $this->clientService->searchClient($request->name), 200);
        } catch (\Exception $e) {
            return ResponseHelper::response('error', $e->getMessage(), 400);
        }
    }

    // call client
    public function callClient(Request $request) {
        $token = $this->net2phone->generateToken();
        return $token;
    }
}
