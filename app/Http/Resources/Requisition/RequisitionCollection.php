<?php

namespace App\Http\Resources\Requisition;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RequisitionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'uuid' => $models->uuid,
                'title' => $models->title,
                'reference' => $models->reference,
                'country' => $models->country,
                'comments' => $models->comments,
                'total_silo' => $models->total_silo,
                'seismic_zone' => $models->seismic_zone
            ];
        });
    }
}
