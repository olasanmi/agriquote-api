<?php

namespace App\Http\Resources\Requisition;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\RequisitionSilo\RequisitionSiloCollection;
use App\Http\Resources\RequisitionPdf\RequisitionPdfCollection;

class RequisitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'uuid' => $this->uuid,
            'title' => $this->title,
            'country' => $this->country,
            'reference' => $this->reference,
            'comments' => $this->comments,
            'total_silo' => $this->total_silo,
            'selected_provider' => $this->selected_provider,
            'silos' => new RequisitionSiloCollection($this->Silos),
            'seismic_zone' => $this->seismic_zone,
            'pdf' => new RequisitionPdfCollection($this->RequisitionPdf)
        ];
    }
}
