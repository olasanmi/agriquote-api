<?php

namespace App\Http\Resources\QuoteItemAttribute;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuoteItemAttributeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'idd' => $models->id,
                'name' => $models->Attribute ? $models->Attribute->name : $models->name,
                'value' => $models->value,
                'isset' => true,
                'selected' => $models->selected ? true : false,
                'attribute_id' => $models->attribute_id,
                'variable' => $models->Attribute and $models->Attribute->variable ? true : false
            ];
        });
    }
}
