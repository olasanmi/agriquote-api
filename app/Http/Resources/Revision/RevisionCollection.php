<?php

namespace App\Http\Resources\Revision;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Quote\QuoteResource;

class RevisionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'pdf' => $models->pdf,
                'total' => $models->total,
                'code' => $models->code,
                'client' => $models->Quote->Client ? $models->Quote->Client->name . ' ' . $models->Quote->Client->last_name : '-',
                'uuid' => $models->Quote ? $models->Quote->uuid : '-',
                'lastEmail' => $models->getLastEmailDate(),
                'clientStatus' => $models->getUserStatus(),
                'user_status' => $models->user_status,
                'nextEmail' => $models->next_email ? $models->next_email : '-',
                'emailInterval' => $models->email_interval,
                'emailPause' => $models->email_pause ? true : false,
                'blueprints' => $models->Quote->blueprints,
                'blueprintsUrl' => $models->Quote->blueprints ? url('images/blueprints', $models->Quote->blueprints) : '',
                'status' => $models->status ? true : false
            ];
        });
    }
}
