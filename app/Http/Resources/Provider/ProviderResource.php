<?php

namespace App\Http\Resources\Provider;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'country' => $this->country,
            'uuid' => $this->uuid,
            'address' => $this->address,
            'email' => $this->email,
            'phone' => $this->phone,
            'purchase_orders' => $this->PurchaseOrders,
            'manager' => $this->manager
        ];
    }
}
