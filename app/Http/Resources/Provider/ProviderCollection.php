<?php

namespace App\Http\Resources\Provider;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProviderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'country' => $models->country,
                'uuid' => $models->uuid,
                'address' => $models->address,
                'email' => $models->email,
                'phone' => $models->phone,
                'manager' => $models->manager
            ];
        });
    }
}
