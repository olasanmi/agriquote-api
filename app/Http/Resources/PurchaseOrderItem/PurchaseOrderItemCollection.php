<?php

namespace App\Http\Resources\PurchaseOrderItem;

use Illuminate\Http\Resources\Json\ResourceCollection;


class PurchaseOrderItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'quote_number' => $models->quote_number,
                'discount' => $models->discount,
                'cost' => $models->cost,
                'totalCost' => $models->total,
                'quote_item_id' => $models->quote_item_id,
                'quantity' => $models->quantity,
                'show' => $models->show ? true : false,
                'name' => $models->QuoteItem->Item->name,
                'reference' => $models->QuoteItem->Item->reference
            ];
        });
    }
}
