<?php

namespace App\Http\Resources\QuoteItem;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\QuoteItemAttribute\QuoteItemAttributeCollection;
use App\Http\Resources\QuoteItemSubitem\QuoteItemSubitemCollection;
use App\Http\Resources\QuoteItemSubitem\CheckGroupCollection;
use App\Http\Resources\SubItem\SubItemCollection;

class QuoteItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'profit' => $models->profit,
                'cost' => $models->cost,
                'dealerProfit' => $models->dealerProfit,
                'position' => $models->position,
                'price' => $models->price,
                'item_id' => $models->item_id,
                'quantity' => $models->quantity,
                'total' => $models->total,
                'subitemsTotal' => $models->subitemsTotal,
                'weight' => number_format($models->weight, 2),
                'totalWeight' => number_format($models->totalWeight, 2),
                'quote_id' => $models->quote_id,
                'tCurrency' => $models->tCurrency,
                'isset' => true,
                'name' => $models->Item ? $models->Item->name : '-',
                'attributes' => $models->Attributes ? new QuoteItemAttributeCollection($models->Attributes) : [],
                'subitems' => $models->Subitems ? new QuoteItemSubitemCollection($models->Subitems) : [],
                'optionSubitems' => new SubItemCollection($models->Item->Subitems),
                'provider' => $models->Item->Provider ? : '',
                'provider_id' => $models->Item->provider_id ? : '',
                'reference' => $models->Item->reference,
                'totalCost' => ($models->quantity * $models->cost),
                'show' => true,
                'costWithTax' => $models->costWithTax,
                'show_title' => $models->show_title ? true : false,
                'show_title2' => $models->show_title2 ? true : false,
                'checkGroup' => new CheckGroupCollection($models->Subitems),
                'cost_type' => $models->cost_type ? true : false,
                'oldCost' => $models->oldCost
            ];
        });
    }
}
