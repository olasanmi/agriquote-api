<?php

namespace App\Http\Resources\QuoteItemSubitem;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuoteItemSubitemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'idd' => $models->id,
                'name' => $models->Subitem->name,
                'quantity' => $models->quantity,
                'price' => $models->price,
                'subitem_id' => $models->subitem_id,
                'subitem' => $models->Subitem and $models->Subitem->name,
                'costWithTax' => $models->costWithTax,
                'load' => true
            ];
        });
    }
}
