<?php

namespace App\Http\Resources\RequisitionSiloComplement;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\RequisitionSilo\RequisitionSiloCollection;

class RequisitionSiloComplementResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tolva' => $this->tolva,
            'use' => $this->use,
            'manual_door' => $this->manual_door,
            'electric_door' => $this->electric_door,
            'aeration' => $this->aeration,
            'conveyor_width' => $this->conveyor_width,
            'total_length' => $this->total_length,
            'cupula' => $this->cupula,
            'alero' => $this->alero,
            'croquis' => $this->croquis
        ];
    }
}
