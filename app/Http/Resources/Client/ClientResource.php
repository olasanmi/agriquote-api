<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Appointment\AppointmentCollection;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'fullname' => $this->fullname,
            'uuid' => $this->uuid,
            'cellphone' => $this->cellphone,
            'country' => $this->country,
            'brand' => $this->brand,
            'country_label' => $this->country_label
        ];
    }
}
