<?php

namespace App\Http\Resources\Dealer;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DealerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name . ' ' . $models->last_name,
                'email' => $models->email,
                'uuid' => $models->uuid,
                'phone' => $models->phone ? $models->phone : ' - ',
                'cellphone' => $models->cellphone,
                'country' => $models->country,
                'brand' => $models->brand ? $models->brand : ' - ',
                'country_label' => $models->country_label
            ];
        });
    }
}
