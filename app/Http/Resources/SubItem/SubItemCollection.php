<?php

namespace App\Http\Resources\SubItem;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SubItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'name_en' => $models->name_en,
                'name_br' => $models->name_br,
                'price' => 0,
                'quantity' => 1,
                'label' => $models->name,
                'value' => $models->id
            ];
        });
    }
}
