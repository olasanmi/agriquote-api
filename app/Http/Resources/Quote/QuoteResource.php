<?php

namespace App\Http\Resources\Quote;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\QuoteItem\QuoteItemCollection;
use App\Http\Resources\Revision\RevisionCollection;
use App\Http\Resources\QuoteProvider\QuoteProvierCollection;

class QuoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'date_creation' => $this->date_creation,
            'date_validation' => $this->date_validation,
            'subtotal' => $this->subtotal,
            'uuid' => $this->uuid,
            'discount' => $this->discount,
            'packaging_value' => $this->packaging_value,
            'freight_value' => $this->freight_value,
            'load_containers' => $this->load_containers,
            'country' => $this->country,
            'user' => $this->User,
            'totalWeight' => $this->totalWeight,
            'total' => $this->total,
            'email_interval' => $this->email_interval,
            'client' => $this->Client,
            'dealer' => $this->Dealer,
            'client_id' => $this->client_id,
            'dealer_id' => $this->dealer_id,
            'description' => $this->description ? $this->description : '',
            'description_en' => $this->description_en ? $this->description_en : '',
            'description_br' => $this->description_br ? $this->description_br : '',
            'confirm_date' => $this->confirm_date,
            'is_confirm' => $this->is_confirm ? true : false,
            'items' => new QuoteItemCollection($this->Items),
            'status' => $this->Status ? $this->Status->name : '-',
            'status_id' => $this->Status->id,
            'clietComment' => $this->comment ? $this->comment : '',
            'blueprints' => $this->blueprints,
            'blueprintsLabel' => $this->blueprints,
            'blueprintsUrl' => $this->blueprints ? url('images/blueprints', $this->blueprints) : '',
            'user_statuses' => $this->getUserStatus(),
            'user_status' => $this->user_status,
            'pdf' => $this->pdf,
            'accept_terms' => $this->accept_terms ? true : false,
            'last_page' => $this->last_page,
            'last_page_en' => $this->last_page_en,
            'last_page_br' => $this->last_page_br,
            'revisions' => $this->Revisions,
            'email_count' => $this->email_count,
            'email_contact_2' => $this->email_contact_2,
            'email_contact_3' => $this->email_contact_3,
            'purchase_orders' => $this->PurchaseOrders,
            'providers' => new QuoteProvierCollection($this->Providers)
        ];
    }
}
