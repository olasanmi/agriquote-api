<?php

namespace App\Http\Resources\Quote;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuoteOportunities extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'code' => $models->code,
                'date_creation' => $models->date_creation,
                'date_validation' => $models->date_validation,
                'subtotal' => $models->subtotal,
                'uuid' => $models->uuid,
                'totalWeight' => $models->totalWeight,
                'discount' => $models->discount,
                'total' => $models->total,
                'packaging_value' => $models->packaging_value,
                'freight_value' => $models->freight_value,
                'load_containers' => $models->load_containers,
                'country' => $models->country,
                'user' => $models->User ? $models->User->name . ' ' . $models->User->last_name : '-',
                'client' => $models->Client ? $models->Client->name . ' ' . $models->Client->last_name : '-',
                'dealer' => $models->Dealer ? $models->Dealer->name . ' ' . $models->Dealer->last_name : '-',
                'status' => $models->Status ? $models->Status->name : '-',
                'status_id' => $models->Status->id,
                'user_id' => $models->user_id,
                'user_status' => $models->user_status,
                'clietComment' => $models->comment ? $models->comment : '-',
                'clientStatus_id' => $models->user_status,
                'complete' => $models->is_complete ? true : false,
                'clientStatus' => $models->getUserStatus(),
                'lastEmail' => $models->getLastEmailDate(),
                'nextEmail' => $models->next_email,
                'emailInterval' => $models->email_interval,
                'emailPause' => $models->email_pause ? true : false
            ];
        });
    }
}
