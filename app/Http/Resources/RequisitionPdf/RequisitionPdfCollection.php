<?php

namespace App\Http\Resources\RequisitionPdf;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RequisitionPdfCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'pdf' => $models->pdf,
                'provider' => $models->Provider->name,
                'reference' => $models->Requisition->reference,
                'providerEmail' => $models->Provider->email,
                'croquis' => json_decode($models->croquis, true),
                'nextEmail' => $models->next_email,
                'lastEmail' => $models->getLastEmailDate(),
                'emailPause' => $models->is_pause ? true : false
            ];
        });
    }
}
