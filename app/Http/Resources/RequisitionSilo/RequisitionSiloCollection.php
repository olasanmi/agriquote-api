<?php

namespace App\Http\Resources\RequisitionSilo;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\RequisitionSilo\RequisitionSilo;
use App\Http\Resources\RequisitionSiloComplement\RequisitionSiloComplementResource;

class RequisitionSiloCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'quantity' => $models->quantity,
                'product' => $models->product,
                'density' => $models->density,
                'voltage' => $models->voltage,
                'seismic_zone' => $models->seismic_zone,
                'type' => $models->type,
                'capacity' => $models->capacity,
                'model' => $models->model,
                'runway' => $models->runway,
                'aeration_system' => $models->aeration_system,
                'sweepers' => $models->sweepers,
                'capacity_sweepers' => $models->capacity_sweepers,
                'powersweep' => $models->powersweep,
                'powersweep_extension' => $models->powersweep_extension,
                'thermometry_system' => $models->thermometry_system,
                'thermometry_system_type' => $models->thermometry_system_type,
                'comment' => $models->comment,
                'silo_model_id' => $models->silo_model_id,
                'accesories' => $this->generateAccesories($models),
                'complement' => new RequisitionSiloComplementResource($models->Complements)
            ];
        });
    }

    public function generateAccesories(RequisitionSilo $requisitionSilo) {
        $arr = array();
        foreach ($requisitionSilo->Accesories as $key => $value) {
            array_push($arr, $value['name']);
        }
        return $arr;
    }
}
