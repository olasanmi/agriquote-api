<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'username' => $this->name,
            'email' => $this->email,
            'uuid' => $this->uuid,
            'token' => $this->token ? $this->token : '',
            'rol' => $this->UserType ? $this->UserType->name : '',
            'image' => $this->image,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name
        ];
    }
}
