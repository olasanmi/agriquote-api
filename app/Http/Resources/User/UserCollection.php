<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'email' => $models->email,
                'uuid' => $models->uuid,
                'rol' => $models->UserType ? $models->UserType->name : '',
                'image' => $models->image,
                'first_name' => $models->first_name,
                'last_name' => $models->last_name
            ];
        });
    }
}
