<?php

namespace App\Http\Resources\PurchaseOrder;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PurchaseOrderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'date' => $models->date,
                'reference' => $models->reference,
                'payment_conditions' => $models->payment_conditions,
                'shipping_conditions' => $models->shipping_conditions,
                'uuid' => $models->uuid,
                'quote' => $models->Quote ? $models->Quote->code : '',
                'customer_id' => $models->Quote ? $models->Quote->Client->id : '',
                'provider' => $models->Provider ? $models->Provider->name : '',
                'currency' => $models->currency,
                'approved' => $models->approved,
                'date_delivered' => $models->date_delivered,
                'pdf' => $models->pdf,
                'subtotal' => $models->subtotal,
                'total' => $models->total
            ];
        });
    }
}
