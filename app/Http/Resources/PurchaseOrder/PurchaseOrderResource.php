<?php

namespace App\Http\Resources\PurchaseOrder;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Quote\QuoteResource;
use App\Http\Resources\PurchaseOrderItem\PurchaseOrderItemCollection;

class PurchaseOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'reference' => $this->reference,
            'payment_conditions' => $this->payment_conditions,
            'shipping_conditions' => $this->shipping_conditions,
            'uuid' => $this->uuid,
            'provider' => $this->Provider,
            'description' => $this->description ? $this->description : '',
            'currency' => $this->currency,
            'items' => new PurchaseOrderItemCollection($this->PurchaseOrderItems),
            'quote' => new QuoteResource($this->Quote),
            'pdf' => $this->pdf,
            'subtotal' => $this->subtotal,
            'total' => $this->total
        ];
    }
}
