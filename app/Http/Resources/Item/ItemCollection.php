<?php

namespace App\Http\Resources\Item;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'reference' => $models->reference,
                'maker' => $models->Provider ? $models->Provider->name : '-',
                'quote_number' => $models->quote_number,
                'uuid' => $models->uuid,
                'description' => $models->description,
                'description_en' => $models->description_en,
                'image' => $models->image,
                'provider' => $models->Provider ? $models->Provider->name : ''
            ];
        });
    }
}
