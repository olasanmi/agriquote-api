<?php

namespace App\Http\Resources\Item;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Attribute\AttributeCollection;
use App\Http\Resources\SubItem\SubItemCollection;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'name_en' => $this->name_en ? $this->name_en : '',
            'name_br' => $this->name_br ? $this->name_br : '',
            'reference' => $this->reference,
            'maker' => $this->maker,
            'quote_number' => $this->quote_number,
            'uuid' => $this->uuid,
            'description' => $this->description,
            'description_en' => $this->description_en ? $this->description_en : '',
            'description_br' => $this->description_br ? $this->description_br : '',
            'image' => $this->image,
            'provider_id' => $this->provider_id,
            'attributes' => new AttributeCollection($this->Attributes),
            'subitems' => new SubItemCollection($this->Subitems),
            'description' => $this->description,
            'provider' => $this->Provider ? $this->Provider : ''
        ];
    }
}
