<?php

namespace App\Http\Resources\QuoteProvider;

use Illuminate\Http\Resources\Json\ResourceCollection;

class QuoteProvierCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'tCurrency' => $models->tCurrency,
                'provider' => $models->Provider->name,
                'container40' => $models->container40,
                'container20' => $models->container20,
                'price40' => $models->price40,
                'provide_id' => $models->provide_id,
                'price20' => $models->price20
            ];
        });
    }
}
