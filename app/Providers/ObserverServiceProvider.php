<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\User;
use App\Observers\User\UserObserver;
use App\Models\Client\Client;
use App\Observers\Client\ClientObserver;
use App\Models\Dealer\Dealer;
use App\Observers\Dealer\DealerObserver;
use App\Models\Item\Item;
use App\Observers\Item\ItemObserver;
use App\Models\Quote\Quote;
use App\Observers\Quote\QuoteObserver;
use App\Models\Provider\Provider;
use App\Observers\Provider\ProviderObserver;
use App\Models\PurchaseOrder\PurchaseOrder;
use App\Observers\PurchaseOrder\PurchaseObserver;
use App\Models\Requisition\Requisition;
use App\Observers\Requisition\RequisitionObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        User::observe(UserObserver::class);
        Client::observe(ClientObserver::class);
        Dealer::observe(DealerObserver::class);
        Item::observe(ItemObserver::class);
        Quote::observe(QuoteObserver::class);
        Provider::observe(ProviderObserver::class);
        PurchaseOrder::observe(PurchaseObserver::class);
        Requisition::observe(RequisitionObserver::class);
    }
}
