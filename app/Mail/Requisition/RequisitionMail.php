<?php

namespace App\Mail\Requisition;

use Illuminate\Bus\Queueable;
use App\Models\RequisitionPdf\RequisitionPdf;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Log;

class RequisitionMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->markdown('mails.requisition.requisition');
        $attachments = [];
        array_push($attachments, $this->data->pdf);
        foreach (json_decode($this->data->croquis, true) as $key => $croquis) {
            if ($croquis) {
                array_push($attachments, $croquis);
            }
        }
        foreach ($attachments as $filePath) {
            $email->attach($filePath);
        }
        $email->subject('New requisition');
        return $email;
    }
}
