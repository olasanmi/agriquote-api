<?php
namespace App\Services\Quote;
ini_set('max_execution_time', '5000000000000');

use App\Models\Quote\Quote;
use App\Models\User;
use App\Models\QuoteItem\QuoteItem;
use App\Models\QuoteProvider\QuoteProvider;
use App\Models\Revision\Revision;
use App\Models\ItemQuoteAttribute\ItemQuoteAttribute;
use App\Models\ItemQuoteSubitem\ItemQuoteSubitem;
// resources
use App\Http\Resources\Quote\QuoteResource;
// import services
use App\Services\Client\ClientServices;
use App\Services\PurchaseOrder\PurchaseOrderServices;
use App\Services\Reviews\ReviewsServices;
use Carbon\Carbon;
// email
use App\Mail\Quote\ValidationMail;
use App\Mail\Quote\QuoteMail;
use App\Mail\Quote\QuoteResponseMail;
use Illuminate\Support\Facades\Mail;
use Log;
// load domp pdf
use PDF;


class QuoteServices
{
	protected $clientService, $purchaseOrderService, $reviewsServices;
	function __construct()
	{
		$this->clientService = new ClientServices();
		$this->purchaseOrderService = new PurchaseOrderServices();
		$this->reviewsServices = new ReviewsServices();
	}

	public function generateReference($clientId) {
		$client =  $this->clientService->filterClient($clientId);
		return $client->country . $this->getYear() . '-' . $this->getMonth() . $this->getDay() . '-' . $this->getCountQuote($client->country) . '-' . '00';
	}

	public function getYear() {
		$currentYear = Carbon::now()->year;
		return substr($currentYear, 1);
	}

	public function getMonth() {
		$currentMont = Carbon::now()->format('m');
		return substr($currentMont, 0);
	}

	public function getDay() {
		$currentDay = Carbon::now()->format('d');
		return substr($currentDay, 0);
	}

	public function getCountQuote($country) {
		$now = Carbon::now()->format('Y-m-d') . ' 00:00:00';
		$quoteCount = Quote::where('country', $country)->where('date_creation', '>=', $now)
		->get()->count();
		$count = ($quoteCount + 1);
		return str_pad($count, 3, 0, STR_PAD_LEFT);
	}

	public function upRevisinNumber(Quote $quote) {
		$explode = explode('-', $quote->code);
		$sum = $explode[3] + 1;
		$newCode = $explode[0] . '-' . $explode[1] . '-' . $explode[2] . '-' . str_pad($sum, 2, 0, STR_PAD_LEFT);
		return $newCode;
	}

	public function createQuoteProviders(Quote $quote, $providers) {
		foreach ($providers as $key => $value) {
			$provider = QuoteProvider::where('provide_id', $value['id'])
			->where('quote_id', $quote->id)
			->first();
			if (!isset($provider->id)) {
				$quote->Providers()->create([
					'tCurrency' => $value['tCurrency'],
					'provide_id' => $value['id'],
					'container40' => $value['container40'],
					'container20' => $value['container20'],
					'price40' => $value['price40'],
					'price20' => $value['price20']
				]);
			} else {
				$provider->update([
					'tCurrency' => $value['tCurrency'],
					'container40' => $value['container40'],
					'container20' => $value['container20'],
					'price40' => $value['price40'],
					'price20' => $value['price20']
				]);
			}
		}
	}

	public function createItems(Quote $quote, $items) {
		foreach ($items as $key => $value) {
			$item = $quote->items()->create([
				'cost' => $value['cost'],
				'dealerProfit' => $value['dealerProfit'],
				'name' => $value['name'],
				'position' => $value['position'],
				'price' => $value['price'],
				'subitemsTotal' => $value['subitemsTotal'],
				'profit' => $value['profit'],
				'quantity' => $value['quantity'],
				'total' => $value['total'],
				'totalWeight' => $value['totalWeight'],
				'weight' => $value['weight'],
				'tCurrency' => $value['tCurrency'],
				'costWithTax' => $value['costWithTax'],
				'show_title' => $value['show_title'],
				'show_title2' => $value['show_title2'],
				'cost_type' => $value['cost_type'],
				'oldCost' => $value['oldCost']
			]);
			if (isset($value['item_id'])) {
				$item->update(['item_id' => $value['item_id']]);
			} else {
				$item->update(['item_id' => $value['id']]);
			}
			foreach ($value['attributes'] as $attr => $attribute) {
				$attributes = $item->Attributes()->create([
					'name' => $attribute['name'],
					'value' => $attribute['value'],
					'selected' => $attribute['selected']
				]);
				if (isset($attribute['attribute_id'])) {
					$attributes->update(['attribute_id' => $attribute['attribute_id']]);
				} else {
					$attributes->update(['attribute_id' => $attribute['id']]);
				}
			}
			foreach ($value['subitems'] as $sub => $subitem) {
				$subiten = $item->Subitems()->create([
					'name' => $subitem['name'],
					'price' => $subitem['price'],
					'quantity' => $subitem['quantity'],
					'costWithTax' => $subitem['costWithTax']
				]);
				if (isset($subitem['subitem_id'])) {
					$subiten->update(['subitem_id' => $subitem['subitem_id']]);
				} else {
					$subiten->update(['subitem_id' => $subitem['id']]);
				}
			}
		}
	}

	// update quote items relations
	public function updateQuoteItem(Quote $quote, $items) {
		foreach ($items as $key => $value) {
			if (isset($value['isset'])) {
				$item = QuoteItem::find($value['id']);
				$item->update([
					'cost' => $value['cost'],
					'dealerProfit' => $value['dealerProfit'],
					'name' => $value['name'],
					'position' => $value['position'],
					'price' => $value['price'],
					'subitemsTotal' => $value['subitemsTotal'],
					'profit' => $value['profit'],
					'quantity' => $value['quantity'],
					'total' => $value['total'],
					'totalWeight' => $value['totalWeight'],
					'weight' => $value['weight'],
					'tCurrency' => $value['tCurrency'],
					'costWithTax' => $value['costWithTax'],
					'show_title' => $value['show_title'],
					'show_title2' => $value['show_title2'],
					'cost_type' => $value['cost_type'],
					'oldCost' => $value['oldCost']
				]);
			} else {
				$item = $quote->items()->create([
					'cost' => $value['cost'],
					'dealerProfit' => $value['dealerProfit'],
					'name' => $value['name'],
					'position' => $value['position'],
					'price' => $value['price'],
					'subitemsTotal' => $value['subitemsTotal'],
					'profit' => $value['profit'],
					'quantity' => $value['quantity'],
					'total' => $value['total'],
					'totalWeight' => $value['totalWeight'],
					'weight' => $value['weight'],
					'tCurrency' => $value['tCurrency'],
					'item_id' => $value['id'],
					'costWithTax' => $value['costWithTax'],
					'show_title' => $value['show_title'],
					'show_title2' => $value['show_title2'],
					'cost_type' => $value['cost_type'],
					'oldCost' => $value['oldCost']
				]);
			}
			foreach ($value['attributes'] as $attr => $attribute) {
				if (isset($attribute['isset'])) {
					$itemAttr = ItemQuoteAttribute::find($attribute['idd']);
					$itemAttr->update([
						'name' => $attribute['name'],
						'value' => $attribute['value'],
						'selected' => $attribute['selected']
					]);
				} else {
					$item->Attributes()->create([
						'name' => $attribute['name'],
						'value' => $attribute['value'],
						'selected' => $attribute['selected'],
						'attribute_id' => $attribute['id']
					]);
				}
			}
			foreach ($value['subitems'] as $sub => $subitem) {
				if (isset($subitem['idd'])) {
					$itemSubitem = ItemQuoteSubitem::find($subitem['idd']);
					$itemSubitem->update([
						'name' => $subitem['name'],
						'price' => $subitem['price'],
						'quantity' => $subitem['quantity'],
						'costWithTax' => $subitem['costWithTax']
					]);
				} else {
					ItemQuoteSubitem::create([
						'name' => $subitem['name'],
						'price' => $subitem['price'],
						'quantity' => $subitem['quantity'],
						'subitem_id' => $subitem['id'],
						'quote_item_id' => $item->id,
						'costWithTax' => $subitem['costWithTax']
					]);
				}
			}
		}
	}

	public function changeStatus(Quote $quote, $request, $user) {
		$quote->update([
			'quote_statuse_id' => $request['quote_statuse_id'],
			'description' => $request['description'],
			'last_page' => $request['last_page'],
			'email_contact_2' => $request['email_contact_2'],
			'email_contact_3' => $request['email_contact_3']
		]);

		if ($request['quote_statuse_id'] === 2) {
			$this->sendValidationEmail($quote);
		}
		if ($request['quote_statuse_id'] === 3) {
			$quote->update([
				'confirm_date' => Carbon::now()->format('Y-m-d'),
				'user_confirm_id' => $user,
				'is_confirm' => true,
				'user_confirm_id' => $user
			]);
			$arr = array_chunk(json_decode($quote->Items, true), 20);
            $quote->newItems = $arr;
			$pdf = $this->generatePdf($quote);
			$response['pdf'] = $pdf;
			// generate opportunities
			$this->reviewsServices->generateReview(Quote::find($quote->id));
		}
		$response['quote'] = new QuoteResource($quote);
		return $response;
	}

	public function sendValidationEmail($quote) {
		$admins = User::Admins();
		foreach ($admins as $key => $value) {
			$quote->admin = $value['first_name'] . ' ' . $value['last_name'];
			Mail::to($value['email'])->send(new ValidationMail($quote));
		}
		Log::error('enviado los emails a los admin....');
	}

	public function sendNewEmailOportunities(Quote $quote, $request, Revision $revision) {
		// send email to main client
		$this->sendQuoteMail($quote, $quote->Client->email, $revision);
		Log::error("Se ha enviado el correo de la cotización por primer email código: " . $quote->code);
		// send dealer email
		if ($quote->Dealer and $quote->Dealer->id) {
			if ($request['dealer'])
				$this->sendQuoteMail($quote, $quote->Dealer->email, $revision);
		}
		// send email to quote contact 2
		if ($request['other']) {
			$quote->update([
				'email_contact_2' => $request['email_contact_2'],
				'email_contact_3' => $request['email_contact_3']
			]);
			if ($quote->email_contact_2)
				$this->sendQuoteMail($quote, $quote->email_contact_2, $revision);
			if ($quote->email_contact_3)
				$this->sendQuoteMail($quote, $quote->email_contact_3, $revision);
		}
		// send email to user create quote
		if ($request['quotedBy'])
			$this->sendQuoteMail($quote, $quote->User->email, $revision);
		// send email to quote user confirmator
		if ($request['approve'])
			$this->sendQuoteMail($quote, $quote->UserConfirm->email, $revision);
		// set first email send
		$quote->update(['first_email' => true]);
		if ($quote->email_count == 0)
			$this->setEmailCount($quote);
	}

	public function sendQuoteMail($quote, $email, Revision $revision) {
		Mail::to($email)->send(new QuoteMail($quote));
		$this->setNextEmailDate($quote, $revision);
		if ($quote->email_count >= 1) {
			$this->setEmailCount($quote);
		}
	}

	public function setEmailCount(Quote $quote) {
		$quote->update(['email_count' => $quote->email_count + 1]);
	}

	public function generatePdf(Quote $quote) {
		try {
            $pdf = PDF::loadView('pdf/quote', array('quote' => $quote))->setPaper('a4');
            $path = public_path('pdf/quote');
            $fileName =  $quote->code . '.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);
            $url = url('pdf/quote',  $fileName);
            $quote2 = Quote::find($quote->id);
            $quote2->update(['pdf' => $url]);
            return $url;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
	}

	public function setNextEmailDate(Quote $quote, Revision $revision) {
		$now = Carbon::now();
		if ($quote->user_status == 2) {
			$newDate = $now->addDays(20);
		} else {
			$newDate = $now->addDays($revision->email_interval);
		}
		$quote->update(['next_email' => $newDate->format('Y-m-d')]);
		$revision->update(['next_email' => $newDate->format('Y-m-d')]);
	}

	public function sendAdminResponseEmail(Quote $quote) {
		$admins = User::Admins();
		foreach ($admins as $key => $value) {
			Mail::to($value['email'])->send(new QuoteResponseMail($quote));
		}
		if ($quote->user_status == 2) {
			$this->setNextEmailDate($quote);
		}
	}

	public function setQuoteRevision(Quote $quote) {
		$revision = Revision::create([
			'quote_id' => $quote->id,
			'pdf' => $quote->pdf
		]);
	}

	public function generatePo(Quote $quote) {
		if ($quote->is_complete) {
			foreach ($quote->Providers as $pro => $provider) {
				$i = 0;
				$items = [];
				foreach ($quote->Items as $it => $item) {
					if ($item->Item->provider_id == $provider->provide_id) {
						array_push($items, $item);
						$i++;
					}
				}
				if ($i > 0) {
					$this->purchaseOrderService->createPo($quote, $provider->provide_id, $items);
				}
			}
		}
	}

	public function acceptRevisionTerms($id) {
		$revision = Revision::where('quote_id', '=', $id)->where('status', '=', true)->first();
		$revision->update(['accept_terms' => true]);
	}

	public function commentRevisionForUser(Quote $quote) {
		$revision = Revision::where('quote_id', '=', $quote->id)->where('status', '=', true)->first();
		$revision->update(['comment' => $quote->comment, 'user_status' => $quote->user_status]);
	}
}