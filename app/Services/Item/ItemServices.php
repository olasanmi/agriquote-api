<?php
namespace App\Services\Item;

use App\Models\Item\Item;

use Carbon\Carbon;

class ItemServices
{
	public $item;

	function __construct()
	{
		$this->item = null;
	}

	public function searchItem($query) {
		$this->item = Item::where('name', 'LIKE', '%'. $query . '%')
		->orWhere('reference', 'LIKE', '%' . $query . '%')
		->get();
		return $this->item;
	}
}
