<?php
namespace App\Services\Client;

use App\Models\Client\Client;

use Carbon\Carbon;

class ClientServices
{
	public $client;

	function __construct()
	{
		$this->client = null;
	}

	public function filterClient($id) {
		
		$this->client = Client::find($id);
		return $this->client;
	}

	public function searchClient($query) {
		$this->client = Client::where('fullname', 'LIKE', '%'. $query . '%')
		->orWhere('email', 'LIKE', '%' . $query . '%')
		->orWhere('phone', 'LIKE', '%' . $query . '%')
		->orWhere('cellphone', 'LIKE', '%' . $query . '%')
		->orWhere('brand', 'LIKE', '%' . $query . '%')
		->get();
		return $this->client;
	}
}