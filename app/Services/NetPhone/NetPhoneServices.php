<?php
namespace App\Services\NetPhone;
ini_set('max_execution_time', '5000000000000');

use App\Traits\ConsumeExternalService;

class NetPhoneServices
{
	use ConsumeExternalService;
	public $client_id, $client_secret, $url, $user, $password;

	function __construct()
	{
		$this->client_id = ENV('NETPHONE_CLIENT_ID');
		$this->client_secret = ENV('NETPHONE_CLIENT_SECRET');
		$this->url = ENV('NETPHONE_URL');
		$this->user = ENV('NETPHONE_USER');
		$this->password = ENV('NETPHONE_PASSWORD');
	}

	public function generateToken() {
		$rs = $this->performanceRequest('POST', 'oauth/token/', [
            'client_id' => '5688557594214400',
            'grant_type' => 'password',
            'username' => 'palaciosmunoz.carlos@gmail.com',
            'password' => 'Esc@2523'
        ], [
        	'Content-Type' => 'application/json;charset=utf-8'
        ]);
        
        return $rs;
	}
}