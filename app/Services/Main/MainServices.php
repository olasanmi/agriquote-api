<?php
namespace App\Services\Main;

use App\Models\Client\Client;
use App\Models\Dealer\Dealer;
use App\Models\Item\Item;
use App\Models\User;
use Carbon\Carbon;

class MainServices
{
	function __construct()
	{
	}

	public function loadMainContability() {
		$totalClient = Client::all()->count();
		$totalDealar = Dealer::all()->count();
		$totalUser = User::all()->count();
		$totalItem = Item::all()->count();
		return ['totalClient' => $totalClient, 'totalDealar' => $totalDealar, 'totalUser' => $totalUser, 'totalItem' => $totalItem];
	}
}