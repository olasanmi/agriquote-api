<?php
namespace App\Services\Reviews;

use Log;
// models
use App\Models\Revision\Revision;
use App\Models\Quote\Quote;


class ReviewsServices
{
	function __construct() {
	}

	public function generateReview(Quote $quote) {
		$disable = Revision::where('quote_id', '=', $quote->id)->where('status', '=', true)->first();
		if ($disable) {
			$disable->update(['status' => 0, 'email_pause' => true]);
		}
		$revision = Revision::create([
			'quote_id' => $quote->id,
			'pdf' => $quote->pdf,
			'code' => $quote->code,
			'total' => $quote->total,
			'is_complete' => false,
			'is_trash' => false
		]);
	}
}