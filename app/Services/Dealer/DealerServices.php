<?php
namespace App\Services\Dealer;

use App\Models\Dealer\Dealer;

use Carbon\Carbon;

class DealerServices
{
	public $dealer;

	function __construct()
	{
		$this->dealer = null;
	}

	public function filterDealer($id) {
		
		$this->dealer = Dealer::find($id);
		return $this->dealer;
	}

	public function searchDealer($query) {
		$this->dealer = Dealer::where('fullname', 'LIKE', '%'. $query . '%')
		->orWhere('email', 'LIKE', '%' . $query . '%')
		->orWhere('phone', 'LIKE', '%' . $query . '%')
		->orWhere('cellphone', 'LIKE', '%' . $query . '%')
		->orWhere('brand', 'LIKE', '%' . $query . '%')
		->get();
		return $this->dealer;
	}
}