<?php
namespace App\Services\Requisition;

use App\Models\Requisition\Requisition;
use App\Models\RequisitionProvider\RequisitionProvider;
use App\Models\RequisitionSiloAccesory\RequisitionSiloAccesory;
use App\Models\RequisitionSiloComplement\RequisitionSiloComplement;
use App\Models\RequisitionSilo\RequisitionSilo;
use App\Models\RequisitionPdf\RequisitionPdf;
use App\Models\Provider\Provider;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Mail\Requisition\RequisitionMail;
use Illuminate\Support\Facades\Mail;
use Log;
use PDF;

class RequisitionServices
{
	public $message, $providerIgr;
	function __construct()
	{
		$this->message = array();
		$this->providerIgr = [];
	}

	public function saveComplements(Requisition $requisition, $silos) {
		// save all silos in requisition
		foreach ($silos as $key => $value) {
			$silo = $requisition->Silos()->create($value);
			// save accesories
			foreach ($value['accesories'] as $acc => $accesory) {
				$silo->Accesories()->create([
					'name' => $accesory
				]);
			}
			//save complements
			if ($value['complement']) {
				$silo->Complements()->create($value['complement']);
			}
			// save providers
			$this->saveProviderRequisition($requisition, $silo->SiloModel->Provider->id);
		}
	}

	public function updateComplements(Requisition $requisition, $silos) {
		foreach ($silos as $key => $value) {
			if (isset($value['id'])) {
				$silo = RequisitionSilo::find($value['id']);
				$silo->update($value);
				if ($value['complement']) {
					if (isset($value['complement']['id'])) {
						$silo->Complements()->update($value['complement']);
					} else {
						$silo->Complements()->create($value['complement']);
					}
				}
				$silo->Accesories()->delete();
				foreach ($value['accesories'] as $acc => $accesory) {
					$silo->Accesories()->create([
						'name' => $accesory
					]);
				}
				$this->saveProviderRequisition($requisition, $silo->SiloModel->Provider->id);
			} else {
				$silo = $requisition->Silos()->create($value);
				foreach ($value['accesories'] as $acc => $accesory) {
					$silo->Accesories()->create([
						'name' => $accesory
					]);
				}
				if ($value['complement']) {
					$silo->Complements()->create($value['complement']);
				}
				$this->saveProviderRequisition($requisition, $silo->SiloModel->Provider->id);
			}
		}
	}

	public function sendRequisitionEmail(Requisition $requisition) {
		foreach ($requisition->Providers as $prov => $provider) {
			$data['provider'] = $provider;
			$data['silos'] = [];
			$data['croquis'] = [];
			$data['reference'] = Str::padLeft($requisition->id, 9, 0);
			$data['code'] = $requisition->reference;
			$data['country'] = $requisition->country;
			$data['seismic_zone'] = $requisition->seismic_zone;
			$data['comments'] = $requisition->comments;
			foreach ($requisition->Silos as $sil => $silo) {
				if ($silo->SiloModel->Provider->id == $provider->id) {
					$dataSilo['quantity'] = $silo->quantity;
					$dataSilo['product'] = $silo->product;
					$dataSilo['density'] = $silo->density;
					$dataSilo['voltage'] = $silo->voltage;
					$dataSilo['type'] = $silo->type;
					$dataSilo['capacity'] = $silo->capacity;
					$dataSilo['model'] = $silo->SiloModel->reference;
					$dataSilo['aeration_system'] = $silo->aeration_system;
					$dataSilo['sweepers'] = $silo->sweepers;
					$dataSilo['capacity_sweepers'] = $silo->capacity_sweepers;
					$dataSilo['powersweep'] = $silo->powersweep;
					$dataSilo['powersweep_extension'] = $silo->powersweep_extension;
					$dataSilo['thermometry_system'] = $silo->thermometry_system;
					$dataSilo['thermometry_system_type'] = $silo->thermometry_system_type;
					$dataSilo['runway'] = $silo->runway;
					$dataSilo['comment'] = $silo->comment;
					$dataSilo['complement'] = $silo->Complements;
					$dataSilo['accesories'] = $silo->Accesories;
					$dataSilo['provider'] = $silo->SiloModel->Provider->name;
					if (!is_null($silo->croquis)) {	
						array_push($data['croquis'], $silo->croquis);
					}
					array_push($data['silos'], $dataSilo);
				}
			}
			array_push($this->message, $data);
		}
		$this->generatePdf($requisition, $this->message);
		$this->generateSudengaPdf($requisition);
		$this->generateIgrainPdf($requisition);
		$requisition = Requisition::find($requisition->id);
		return ['success' => $requisition];
	}

	// save requisition provider
	public function saveProviderRequisition(Requisition $requisition, $provider_id) {
		$isset = RequisitionProvider::where('requisition_id', $requisition->id)->where('provider_id', $provider_id)->first();
		if (!$isset) {
			RequisitionProvider::create([
				'provider_id' => $provider_id,
				'requisition_id' => $requisition->id
			]);
		}
	}

	// generate pdf
	public function generatePdf(Requisition $requisition, $data) {
		if ($requisition->RequisitionPdf) {
	        // $requisition->RequisitionPdf()->delete();
	    }
		try {
            foreach ($data as $key => $value) {
            	$pdf = PDF::loadView('pdf/requisition', array('requisition' => $value))->setPaper('a4');
	            $path = public_path('pdf/requisition');
	            $fileName =  $value['code'] . '-' . $value['provider']['name'] . '.' . 'pdf' ;
	            $pdf->save($path . '/' . $fileName);
	            $url = url('pdf/requisition',  $fileName);
	            $croquis = !is_null($value['croquis']) ? json_encode($value['croquis'], true) : '';
	            $validate = RequisitionPdf::where('pdf', $url)
	            ->where('provider_id', $value['provider']['id'])
	            ->where('requisition_id', $requisition->id)
	            ->first();
	            if (!$validate) {
	            	RequisitionPdf::create([
		            	'pdf' => $url,
		            	'provider_id' => $value['provider']['id'],
		            	'croquis' => $croquis,
		            	'requisition_id' => $requisition->id
		            ]);
	            } else {
	            	$validate->update(['pdf' => $url]);
	            }
            }
            return ['success' => 'Pdf generation success'];
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
	}

	// generate sudenga pdf
	public function generateSudengaPdf (Requisition $requisition) {
		try {
			$sweeper['requisition'] = $requisition;
			$sweeper['items'] = [];
			$sweeper['croquis'] = [];
			foreach ($requisition->Silos as $siloKey => $silo) {
				if ($silo->SiloModel->Provider->id == $requisition->selected_provider) {
					if ($silo->sweepers == 'Si') {
						$silo->type = 1;
						$silo->modelSilo = $silo->SiloModel;
						if (!is_null($silo->croquis)) {	
							array_push($sweeper['croquis'], $silo->croquis);
						}
						array_push($sweeper['items'], $silo);
					}
				}
			}
			if (count($sweeper['items']) > 0) {
				$sweeper['providerComplement'] = Provider::find(20);
				$pdf = PDF::loadView('pdf/sudenga', array('requisition' => $sweeper))->setPaper('a4');
				$path = public_path('pdf/requisition');
				$fileName =  $requisition->reference . '-' . $sweeper['providerComplement']['name'] . '.' . 'pdf' ;
				$pdf->save($path . '/' . $fileName);
				$url = url('pdf/requisition',  $fileName);
				$croquis = !is_null($sweeper['croquis']) ? json_encode($sweeper['croquis'], true) : '';
				$validate = RequisitionPdf::where('pdf', $url)
	            ->where('provider_id', 20)
	            ->where('requisition_id', $requisition->id)
	            ->first();
	            if (!$validate) {
	            	RequisitionPdf::create([
						'pdf' => $url,
						'provider_id' => 20,
						'croquis' => $croquis,
						'requisition_id' => $requisition->id
					]);
	            } else {
	            	$validate->update(['pdf' => $url]);
	            }
			}
			return ['success' => 'Pdf generation success'];
		} catch (Exception $e) {
			return ['error' => $e->getMessage()];
		}
	}

	// generate igain pdf
	public function generateIgrainPdf (Requisition $requisition) {
		try {
			$sweeper['requisition'] = $requisition;
			$sweeper['items'] = [];
			$sweeper['croquis'] = [];
			foreach ($requisition->Silos as $siloKey => $silo) {
				if ($silo->SiloModel->Provider->id == $requisition->selected_provider) {
					if ($silo->thermometry_system == 'Si') {
						$silo->type = 2;
						$silo->modelSilo = $silo->SiloModel;
						if (!is_null($silo->croquis)) {	
							array_push($sweeper['croquis'], $silo->croquis);
						}
						array_push($sweeper['items'], $silo);
					}
				}
			}
			if (count($sweeper['items']) > 0) {
				$sweeper['providerComplement'] = Provider::find(24);
				$pdf = PDF::loadView('pdf/sudenga', array('requisition' => $sweeper))->setPaper('a4');
				$path = public_path('pdf/requisition');
				$fileName =  $requisition->reference . '-' . $sweeper['providerComplement']['name'] . '.' . 'pdf' ;
				$pdf->save($path . '/' . $fileName);
				$url = url('pdf/requisition',  $fileName);
				$croquis = !is_null($sweeper['croquis']) ? json_encode($sweeper['croquis'], true) : '';
				$validate = RequisitionPdf::where('pdf', $url)
	            ->where('provider_id', 24)
	            ->where('requisition_id', $requisition->id)
	            ->first();
	            if (!$validate) {
	            	RequisitionPdf::create([
						'pdf' => $url,
						'provider_id' => 24,
						'croquis' => $croquis,
						'requisition_id' => $requisition->id
					]);
	            } else {
	            	$validate->update(['pdf' => $url]);
	            }
			}
			return ['success' => 'Pdf generation success'];
		} catch (Exception $e) {
			return ['error' => $e->getMessage()];
		}
	}

	// generate unique reference for country
	public function generateReference($country) {
		return $country . $this->getYear() . '-' . $this->getMonth() . $this->getDay() . '-' . $this->getCountRequisition($country);
	}

	public function getYear() {
		$currentYear = Carbon::now()->year;
		return substr($currentYear, 1);
	}

	public function getMonth() {
		$currentMont = Carbon::now()->format('m');
		return substr($currentMont, 0);
	}

	public function getDay() {
		$currentDay = Carbon::now()->format('d');
		return substr($currentDay, 0);
	}

	public function getCountRequisition($country) {
		$now = Carbon::now()->format('Y-m-d') . ' 00:00:00';
		$rquisitionCount = Requisition::where('country', $country)->where('created_at', '>=', $now)
		->get()->count();
		$count = ($rquisitionCount + 1);
		return str_pad($count, 3, 0, STR_PAD_LEFT);
	}

	public function sendFirstEmail(RequisitionPdf $requisition) {
		Mail::to('maalhequi95@gmail.com')->send(new RequisitionMail($requisition));
		// Mail::to('csanchez@agri-techsolutions.com')->send(new RequisitionMail($requisition));
		// Mail::to('cpalacios@agri-techsolutions.com')->send(new RequisitionMail($requisition));
		// Mail::to('maalhequi95@gmail.com')->send(new RequisitionMail($requisition));
		$this->setNextEmailDate($requisition);
	}

	public function setNextEmailDate(RequisitionPdf $requisition) {
		$now = Carbon::now();
		$newDate = $now->addDays(3);
		$requisition->update(['next_email' => $newDate->format('Y-m-d')]);
	}
}