<?php
namespace App\Services\PurchaseOrder;

use App\Models\PurchaseOrder\PurchaseOrder;
use App\Models\PurchaseOrderItem\PurchaseOrderItem;
use App\Models\Quote\Quote;
use Carbon\Carbon;
use Log;
// load domp pdf
use PDF;
use App\Mail\PurchaseOrder\PurchaseOrderMail;
use Illuminate\Support\Facades\Mail;

class PurchaseOrderServices
{
	function __construct()
	{
	}

	// save purchase items
	public function saveItems(PurchaseOrder $purchaseOrder, $createMethod, $items) {
		if ($createMethod) {
			$this->itemDataSave($purchaseOrder, $items);
		} else {	
			foreach ($purchaseOrder->Quote->Providers as $prov => $provider) {
				Log::error($provider);
			}
		}
	}

	// generate item data
	public function itemDataSave(PurchaseOrder $purchaseOrder, $items) {
		foreach ($items as $key => $value) {
			$purchaseOrder->PurchaseOrderItems()->create([
				'quote_number' => isset($value['quote_number']) ? $value['quote_number'] : '',
				'discount' => isset($value['discount']) ? $value['discount'] : 0,
				'cost' => $value['cost'],
				'total' => $value['totalCost'],
				'quantity' => $value['quantity'],
				'quote_item_id' => $value['id'],
				'show' => $value['show']
			]);
		}
	}

	// update item data
	public function itemDataUpdate($items) {
		foreach ($items as $key => $value) {
			$item = PurchaseOrderItem::find($value['id']);
			$item->update([
				'quote_number' => isset($value['quote_number']) ? $value['quote_number'] : '',
				'show' => $value['show']
			]);
		}
	}

	// create po data
	public function createPo(Quote $quote, $provider, $items) {
		$po = $quote->PurchaseOrders()->create([
			'provider_id' => $provider,
			'date' => Carbon::now()->format('Y-m-d'),
			'payment_conditions' => '20% - 80%',
			'shipping_conditions' => 'ExWork',
		]);
		foreach ($items as $key => $value) {
			if ($value['cost'] == 0) {
				$cost = $value['subitemsTotal'];
			} else {
				if ($value['cost_type'] == false) {
					$cost = $value['cost'] + $cost = $value['subitemsTotal'];
				} else {
					$cost = $value['cost'];
				}
			}
			$po->update(['currency' => $value['tCurrency']]);
            $po->PurchaseOrderItems()->create([
				'quote_number' => '',
				'discount' => 0,
				'cost' => $cost,
				'total' => ($cost * $value['quantity']),
				'quantity' => $value['quantity'],
				'quote_item_id' => $value['id'],
				'show' => true
			]);
        }
	}

	// generate pdf
	public function generatePdf(PurchaseOrder $purchaseOrder) {
		try {
            $pdf = PDF::loadView('pdf/purchaseOrder', array('po' => $purchaseOrder))->setPaper('a4');
            $path = public_path('pdf/purchaseOrder');
            $fileName =  'PO_'.$purchaseOrder->reference . '.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);
            $url = url('pdf/purchaseOrder',  $fileName);
            $purchaseOrder->update(['pdf' => $url]);
            return $url;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
	}

	//send email
	public function sendEmailPdf(PurchaseOrder $purchaseOrder) {
		$this->generatePdf($purchaseOrder);
		Mail::to($purchaseOrder->Provider->email)->send(new PurchaseOrderMail($purchaseOrder));
	}
}