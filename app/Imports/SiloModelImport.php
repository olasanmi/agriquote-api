<?php

namespace App\Imports;

use App\Models\SiloModel\SiloModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Log;

class SiloModelImport implements ToCollection
{
    public $data;
    // constructor
    public function __construct() {
        $this->data = array();
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        Log::error(count($rows));
        foreach ($rows as $key => $value) {
            if ($key > 0) {
                $data['reference'] = $value[0];
                $data['diameter'] = $value[1];
                $data['cylinder_height'] = $value[3];
                $data['Total_height'] = $value[6];
                $data['provider_id'] = $value[11];
                $data['volumen'] = $value[8];
                $data['type'] = $value[12];
                array_push($this->data, $data);
            }
        }
        SiloModel::insert($this->data);
        $this->data = array();
        return true;
    }
}
