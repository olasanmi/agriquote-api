<?php

namespace App\Imports;

use App\Models\SiloModel\SiloModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Log;

class SukupImport implements ToCollection, WithCalculatedFormulas
{
    public $data;
    // constructor
    public function __construct() {
        $this->data = array();
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $key => $value) {
            if ($key >= 0) {
                if (!is_null( $value[0])) {
                    $data['reference'] = $value[0];
                    $data['diameter'] = $value[1];
                    $data['cylinder_height'] = $value[8];
                    $data['Total_height'] = $value[13];
                    $data['provider_id'] = 16;
                    $data['volumen'] = $value[12];
                    $data['type'] = 1;
                    array_push($this->data, $data);
                }
            }
        }
        SiloModel::insert($this->data);
        $this->data = array();
        return true;
    }
}
