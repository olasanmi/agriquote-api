<?php

namespace App\Imports;

use App\Models\SiloModel\SiloModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Log;

class SimagaImport implements ToCollection
{
    public $data;
    // constructor
    public function __construct() {
        $this->data = array();
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        //
        Log::error(count($rows));
        foreach ($rows as $key => $value) {
            if ($key > 0) {
                $data['reference'] = $value[0];
                $data['diameter'] = $value[1];
                $data['cylinder_height'] = $value[2];
                $data['Total_height'] = $value[3];
                $data['provider_id'] = $value[6];
                $data['volumen'] = $value[4];
                $data['type'] = $value[5];
                array_push($this->data, $data);
            }
        }
        SiloModel::insert($this->data);
        $this->data = array();
        return true;
    }
}
