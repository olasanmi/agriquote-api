<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

trait ConsumeExternalService
{

    public function performanceRequest($method, $requestUrl, $formParams = [], $headers = [])
    {
        $client = new Client();
        $url = $this->url . $requestUrl;
        $headers = array(
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );
        $response = $client->post($url, ['headers' =>$headers , 'body' => json_encode($formParams)]);
        $headers = $response->getHeaders();
        $body = $response->getBody();
        return $headers;
    }
}
