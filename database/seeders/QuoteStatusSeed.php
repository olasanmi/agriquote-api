<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class QuoteStatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('quote_statuses')->insert([
            'name' => 'to be confirmed'
        ]);
        DB::table('quote_statuses')->insert([
            'name' => 'In confirmation process'
        ]);
        DB::table('quote_statuses')->insert([
            'name' => 'confirmed'
        ]);
    }
}
