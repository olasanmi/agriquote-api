<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Uuid;

class UserTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin2021.'),
            'user_type_id' => 3,
            'uuid' => Uuid::generate()->string
        ]);
    }
}
