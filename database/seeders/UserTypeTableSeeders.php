<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UserTypeTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_type')->insert([
            'name' => 'support'
        ]);
        DB::table('user_type')->insert([
            'name' => 'standard'
        ]);
        DB::table('user_type')->insert([
            'name' => 'admin'
        ]);
    }
}
