<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemQuoteSubitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_quote_subitems', function (Blueprint $table) {
            $table->id();
            $table->string('name', 120)->nullable();
            $table->float('price', 50, 2)->nullable();
            $table->string('quantity', 120)->nullable();
            $table->unsignedBigInteger('quote_item_id')->nullable()->unsigned();
            $table->foreign('quote_item_id')->references('id')->on('quote_items')->onDelete('cascade');
            $table->unsignedBigInteger('subitem_id')->nullable()->unsigned();
            $table->foreign('subitem_id')->references('id')->on('subitems')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_quote_subitems');
    }
}
