<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemQuoteAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_quote_attributes', function (Blueprint $table) {
            $table->id();
            $table->string('name', 120)->nullable();
            $table->string('value', 250)->nullable();
            $table->boolean('selected', 250)->nullable()->default(true);
            $table->unsignedBigInteger('quote_item_id')->nullable()->unsigned();
            $table->foreign('quote_item_id')->references('id')->on('quote_items')->onDelete('cascade');
            $table->unsignedBigInteger('attribute_id')->nullable()->unsigned();
            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_quote_attributes');
    }
}
