<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequisitionSiloComplementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisition_silo_complements', function (Blueprint $table) {
            $table->id();
            $table->enum('tolva', [45, 60])->nullable();
            $table->enum('use', ['Humedo', 'Reposo', 'Expedición', 'Trabajo'])->nullable();
            $table->enum('manual_door', ['Si', 'No'])->nullable();
            $table->enum('electric_door', ['Si', 'No'])->nullable();
            $table->enum('aeration', ['Si', 'No'])->nullable();
            $table->float('conveyor_width')->nullable();
            $table->float('total_length')->nullable();
            $table->enum('cupula', ['Si', 'No'])->nullable();
            $table->enum('alero', ['Si', 'No'])->nullable();
            $table->string('croquis')->nullable();
            $table->unsignedBigInteger('requisition_silo_id')->nullable()->unsigned();
            $table->foreign('requisition_silo_id')->references('id')->on('requisition_silos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisition_silo_complements');
    }
}
