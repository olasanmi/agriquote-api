<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuoteProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_providers', function (Blueprint $table) {
            $table->id();
            $table->string('tCurrency')->nullable();
            $table->string('container40')->nullable();
            $table->string('container20')->nullable();
            $table->float('price40', 50, 2)->nullable();
            $table->float('price20', 50, 2)->nullable();
            $table->string('rate')->nullable();
            $table->unsignedBigInteger('provide_id')->nullable()->unsigned();
            $table->foreign('provide_id')->references('id')->on('providers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_providers');
    }
}
