<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRequisitionSilosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisition_silos', function (Blueprint $table) {
            $table->id();
            $table->integer('quantity')->nullable();
            $table->string('product')->nullable();
            $table->integer('density')->nullable();
            $table->float('voltage', 50, 2)->nullable();
            $table->float('seismic_zone', 50, 2)->nullable();
            $table->enum('type', ['Fondo conico', 'Fondo plano'])->nullable();
            $table->float('capacity', 50, 2)->nullable();
            $table->string('model')->nullable();
            $table->enum('aeration_system', ['Canales ariacion', 'Full floor'])->nullable();
            $table->enum('sweepers', ['Si', 'No'])->nullable();
            $table->float('capacity_sweepers', 50, 2)->nullable();
            $table->enum('powersweep', ['Si', 'No'])->nullable();
            $table->float('powersweep_extension', 50, 2)->nullable();
            $table->enum('thermometry_system', ['Si', 'No'])->nullable();
            $table->enum('thermometry_system_type', ['Portatil', 'Automatico'])->nullable();
            $table->unsignedBigInteger('requisition_id')->nullable()->unsigned();
            $table->foreign('requisition_id')->references('id')->on('requisitions')->onDelete('cascade');
            $table->unsignedBigInteger('silo_model_id')->nullable()->unsigned();
            $table->foreign('silo_model_id')->references('id')->on('silo_models')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisition_silos');
    }
}
