<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomerIdToPurchaseOrsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders', function (Blueprint $table) {
            //
            $table->string('customer_id', 24)->nullable();
            $table->enum('currency', ['usd', 'eur'])->nullable();
            $table->string('approved', 12)->nullable();
            $table->date('date_delivered')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orsers', function (Blueprint $table) {
            //
        });
    }
}
