<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNetVolToSiloModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('silo_models', function (Blueprint $table) {
            //
            $table->float('volumen', 50, 2)->nullable();
            $table->enum('type', [1, 2, 3])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('silo_models', function (Blueprint $table) {
            //
        });
    }
}
