<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddItemsToRequisitionPdfsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requisition_pdfs', function (Blueprint $table) {
            //
            $table->boolean('is_complete')->default(false);
            $table->boolean('is_pause')->default(false);
            $table->date('next_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requisition_pdfs', function (Blueprint $table) {
            //
        });
    }
}
