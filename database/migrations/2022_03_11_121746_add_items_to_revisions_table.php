<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddItemsToRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revisions', function (Blueprint $table) {
            //
            $table->float('total', 50, 2)->nullable();
            $table->date('next_email')->nullable();
            $table->bigInteger('email_interval')->default(10)->nullable();
            $table->boolean('is_complete')->nullable()->default(false);
            $table->boolean('email_pause')->default(false)->nullable();
            $table->boolean('is_trash')->default(false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revisions', function (Blueprint $table) {
            //
        });
    }
}
