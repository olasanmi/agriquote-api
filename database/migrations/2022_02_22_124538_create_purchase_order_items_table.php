<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_items', function (Blueprint $table) {
            $table->id();
            $table->string('quote_number', 32)->nullable();
            $table->float('discount', 50, 2)->nullable();
            $table->float('cost', 50, 2)->nullable();
            $table->float('total', 50, 2)->nullable();
            $table->unsignedBigInteger('quote_item_id')->nullable()->unsigned();
            $table->foreign('quote_item_id')->references('id')->on('quote_items')->onDelete('cascade');
            $table->unsignedBigInteger('purchase_order_id')->nullable()->unsigned();
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_items');
    }
}
