<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiloModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('silo_models', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->nullable();
            $table->string('diameter')->nullable();
            $table->string('cylinder_height')->nullable();
            $table->string('Total_height')->nullable();
            $table->unsignedBigInteger('provider_id')->nullable()->unsigned();
            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('silo_models');
    }
}
