<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunities', function (Blueprint $table) {
            $table->id();
            $table->string('quote_code', 50)->nullable();
            $table->date('next_email')->nullable();
            $table->bigInteger('email_interval')->default(10)->nullable();
            $table->boolean('status', 50)->nullable();
            $table->boolean('is_complete')->nullable()->default(false);
            $table->boolean('email_pause')->default(false)->nullable();
            $table->boolean('is_trash')->default(false)->nullable();
            $table->unsignedBigInteger('quote_id')->nullable()->unsigned();
            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunities');
    }
}
