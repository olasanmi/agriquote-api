<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuoteItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_items', function (Blueprint $table) {
            $table->id();
            $table->float('cost', 50, 2)->nullable();
            $table->float('profit', 50, 2)->nullable();
            $table->float('dealerProfit', 50, 2)->nullable();
            $table->integer('position')->nullable();
            $table->float('price', 50, 2)->nullable();
            $table->integer('quantity')->nullable();
            $table->float('total', 50, 2)->nullable();
            $table->float('weight', 50, 2)->nullable();
            $table->float('totalWeight', 50, 2)->nullable();
            $table->unsignedBigInteger('quote_id')->nullable()->unsigned();
            $table->foreign('quote_id')->references('id')->on('quotes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_items');
    }
}
